package co.thecomet.data.delivery;

import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

@NoArgsConstructor
@Entity(value = "network_users_delivery_stats", noClassnameStored = true)
public class UserDeliveryStats {
    @Id
    @Getter
    private ObjectId id;
    @Getter
    private String uuid;
    @Getter
    private Map<DeliveryType, Date> stats = Maps.newHashMap();

    public UserDeliveryStats(UUID uuid) {
        this.uuid = uuid.toString();
    }

    public UserDeliveryStats(String uuid) {
        this.uuid = uuid;
    }
}

package co.thecomet.data.delivery;

public enum DeliveryResetType {
    FIRST_OF_MONTH,
    ONE_MONTH_FROM_CLAIMING,
    ONE_DAY_FROM_CLAIMING,
    SINGLE_USE
}

package co.thecomet.data.delivery;

import co.thecomet.data.ranks.Rank;
import lombok.Getter;

public enum DeliveryType {
    VOTE_MSO(2, 5, "&6Vote For Us!", "&7Vote for us at\n&aMinecraftServers.org\n&7and receive &c500 points&7!",
            "&aGo to &7http://minecraftservers.org/server/250708 &aand follow the instructions to receive &6500 points&a!",
            "&7You already voted at\n&aMinecraftServers.org\n&7Try again tomorrow!", DeliveryResetType.ONE_DAY_FROM_CLAIMING, 0, 0, Rank.DEFAULT, false),
    VIP_BONUS(1, 4, "&aMonthly &6VIP &aBonus", "&7Click to claim your\n&c2500 &7monthly bonus coins!",
            "&6We've added &c2500 coins &6to your account!", "&7You have already claimed this bonus!", DeliveryResetType.FIRST_OF_MONTH, 2500, 0, Rank.VIP, true),
    VIP_PLUS_BONUS(1, 5, "&aMonthly &6VIP+ &aBonus", "&7Click to claim your\n&c5000 &7monthly bonus coins!",
            "&6We've added &c5000 coins &6to your account!", "&7You have already claimed this bonus!", DeliveryResetType.FIRST_OF_MONTH, 5000, 0, Rank.VIPPLUS, true),
    ULTRA_BONUS(1, 6, "&aMonthly &6Ultra &aBonus", "&7Click to claim your\n&c7500 &7monthly bonus coins!",
            "&6We've added &c7500 coins &6to your account!", "&7You have already claimed this bonus!", DeliveryResetType.FIRST_OF_MONTH, 7500, 0, Rank.ULTRA, true);

    @Getter
    private int row;
    @Getter
    private int column;
    @Getter
    private String deliveryDisplay;
    @Getter
    private String unclaimedLore;
    @Getter
    private String unclaimedMessage;
    @Getter
    private String claimedMessage;
    @Getter
    private DeliveryResetType resetType;
    @Getter
    private int coinReward;
    @Getter
    private int pointReward;
    @Getter
    private Rank requiredRank;
    @Getter
    private boolean shouldUpdateStats;

    DeliveryType(int row, int column, String deliveryDisplay, String unclaimedLore, String unclaimedMessage, String claimedMessage, DeliveryResetType resetType, int coinReward, int pointReward, Rank requiredRank, boolean shouldUpdateStats) {
        this.row = row;
        this.column = column;
        this.deliveryDisplay = deliveryDisplay;
        this.unclaimedLore = unclaimedLore;
        this.unclaimedMessage = unclaimedMessage;
        this.claimedMessage = claimedMessage;
        this.resetType = resetType;
        this.coinReward = coinReward;
        this.pointReward = pointReward;
        this.requiredRank = requiredRank;
        this.shouldUpdateStats = shouldUpdateStats;
    }
}


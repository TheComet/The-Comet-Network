package co.thecomet.data.ranks;

import co.thecomet.common.chat.FontColor;

public enum Rank {
    DEFAULT("", FontColor.GREY, FontColor.GREY, 0),
    VIP("VIP", FontColor.YELLOW, FontColor.WHITE, 5000),
    VIPPLUS("VIP+", FontColor.YELLOW, FontColor.WHITE, 10000),
    ULTRA("Ultra", FontColor.BLUE, FontColor.WHITE, 15000),
    YOUTUBER("Youtuber", FontColor.ORANGE, FontColor.WHITE, 0),
    FRIEND("Friend", FontColor.ORANGE, FontColor.WHITE, 0),
    BUILDER("Builder", FontColor.DARK_AQUA, FontColor.WHITE, 0),
    HELPER("Helper", FontColor.GREEN, FontColor.WHITE, 0),
    MOD("Mod", FontColor.DARK_GREEN, FontColor.WHITE, 0),
    ADMIN("Admin", FontColor.DARK_RED, FontColor.WHITE, 0),
    DEV("Developer", FontColor.PURPLE, FontColor.AQUA, 0),
    OWNER("Owner", FontColor.DARK_RED, FontColor.AQUA, 0);

    private String displayName;
    private FontColor rankColor;
    private FontColor chatColor;
    private int rankPurchaseCoins;

    Rank(String displayName, FontColor rankColor, FontColor chatColor, int rankPurchaseCoins) {
        this.displayName = displayName;
        this.rankColor = rankColor;
        this.chatColor = chatColor;
        this.rankPurchaseCoins = rankPurchaseCoins;
    }

    public String getDisplayName() {
        return displayName;
    }

    public FontColor getRankColor() {
        return rankColor;
    }

    public FontColor getChatColor() {
        return chatColor;
    }

    public int getRankPurchaseCoins() {
        return rankPurchaseCoins;
    }

    public static Rank getRankFromString(String r) {
        for (Rank rank : Rank.values()) {
            if (rank.name().equalsIgnoreCase(r)) {
                return rank;
            }
        }

        return DEFAULT;
    }

    public static boolean matchFound(String r) {
        for (Rank rank : Rank.values()) {
            if (rank.name().equalsIgnoreCase(r)) {
                return true;
            }
        }

        return false;
    }

}

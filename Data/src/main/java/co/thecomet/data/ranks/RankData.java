package co.thecomet.data.ranks;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor
@Entity(value = "network_rank_data", noClassnameStored = true)
public class RankData {
    @Getter
    @Id
    private ObjectId id;
    @Getter
    private Rank rank;
    @Getter
    private Map<String, Boolean> permissions = new HashMap<>();

    public RankData(Rank rank) {
        this.rank = rank;
    }
}

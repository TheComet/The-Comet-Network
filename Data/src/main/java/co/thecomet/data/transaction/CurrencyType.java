package co.thecomet.data.transaction;

import lombok.Getter;

public enum CurrencyType {
    POINTS("Points"),
    COINS("Coins");

    @Getter
    private String display;

    CurrencyType(String display) {
        this.display = display;
    }
}

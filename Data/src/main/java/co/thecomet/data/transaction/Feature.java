package co.thecomet.data.transaction;

import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Feature {
    @Getter
    private String display;
    @Getter
    private String id;
    @Getter
    private int points = 0;
    @Getter
    private boolean unique = true;
    @Getter
    private CurrencyType type = CurrencyType.POINTS;
    
    public Feature(String display, String id, int points, boolean unique, CurrencyType type) {
        this(display, id, points, type);
        this.unique = unique;
    }

    public Feature(String display, String id, int points, CurrencyType type) {
        this(display, id, points);
        this.type = type;
    }

    public Feature(String display, String id, int points) {
        this.display = display;
        this.id = id;
        this.points = points;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Feature feature = (Feature) o;

        if (id != null ? !id.equals(feature.id) : feature.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = display != null ? display.hashCode() : 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + points;
        result = 31 * result + (unique ? 1 : 0);
        return result;
    }
}

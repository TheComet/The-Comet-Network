package co.thecomet.data.transaction;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.util.UUID;

@NoArgsConstructor
@Entity(value = "network_transactions", noClassnameStored = true)
public class Transaction {
    @Getter
    @Id
    private ObjectId id;
    @Getter
    private Feature feature;
    @Getter
    private String playerUuid;
    @Getter @Setter
    private transient boolean approved = false;
    
    public Transaction(Feature feature, UUID uuid) {
        this.feature =  feature;
        this.playerUuid = uuid.toString();
    }
}

package co.thecomet.data;

import co.thecomet.common.async.Async;
import co.thecomet.common.chat.MessageType;
import co.thecomet.common.utils.Log;
import co.thecomet.data.delivery.DeliveryType;
import co.thecomet.data.delivery.UserDeliveryStats;
import co.thecomet.data.user.INetworkUser;
import co.thecomet.common.utils.UUIDFetcher;
import co.thecomet.data.moderation.ModerationAction;
import co.thecomet.data.moderation.ModerationEntry;
import co.thecomet.data.moderation.NetworkModerationData;
import co.thecomet.data.ranks.Rank;
import co.thecomet.data.ranks.RankData;
import co.thecomet.data.user.User;
import com.google.common.collect.Maps;
import org.bson.types.ObjectId;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.UpdateOperations;

import java.util.*;

public class DataAPI {
    /**
     * Initializes a network user instance for an online player.
     *
     * @param name the name of the user
     * @param uuid the uuid of the user
     * @param ip the ip of the user
     * @return Networkuser instance
     */
    public static User initUser(UUID uuid, String name, String ip) {
        User user = retrieveUser(uuid);

        if (user == null) {
            user = new User(uuid.toString());
        }

        updateUser(user, name, ip);

        return user;
    }

    /**
     * Updates the core data of a user.
     *
     * @param user
     * @param name
     * @param ip
     */
    public static void updateUser(User user, String name, String ip) {
        if (name == null && ip == null) {
            return;
        }

        boolean updated = false;
        if (name != null && !name.isEmpty()) {
            if (user.getName() == null || !(user.getName().equalsIgnoreCase(name)) || user.getNameLower() == null || !(user.getNameLower() == name.toLowerCase())) {
                if (user.getName() != null && !user.getNameHistory().contains(user.getName())) {
                    user.getNameHistory().add(user.getName());
                }

                user.setName(name);
                user.setNameLower(name.toLowerCase());
                updated = true;
            }
        }

        if (ip != null && !ip.isEmpty()) {
            if (user.getIp() == null || user.getIp().equalsIgnoreCase(ip) == false) {
                if (user.getIp() != null && !user.getIpHistory().contains(user.getIp())) {
                    user.getIpHistory().add(user.getIp());
                }

                user.setIp(ip);
                updated = true;
            }
        }

        if (updated) {
            Async.execute(() -> {
                BasicDAO<User, ObjectId> dao = DAOManager.getDAO(User.class);
                if (user.getId() == null) {
                    dao.save(user);
                } else {
                    UpdateOperations<User> ops = dao.createUpdateOperations();

                    if (name != null) {
                        ops.set("name", user.getName());
                        ops.set("nameHistory", user.getNameHistory());
                    }

                    if (ip != null) {
                        ops.set("ip", user.getIp());
                        ops.set("ipHistory", user.getIpHistory());
                    }

                    dao.update(dao.createQuery().field("uuid").equal(user.getUuid()), ops);
                }
            });
        }
    }

    /**
     * Retrieves a profile matching the specified uuid.
     *
     * @param uuid
     * @return
     */
    public static User retrieveUser(UUID uuid) {
        return DAOManager.getDAO(User.class).findOne("uuid", uuid.toString());
    }

    /**
     * Retrieves a profile matching the specified name.
     *
     * @param name
     * @return
     */
    public static User retrieveUserByName(String name) {
        return DAOManager.getDAO(User.class).findOne("nameLower", name.toLowerCase());
    }

    /**
     * Adds a moderation entry to a user's moderation history.
     *
     * @param entry
     */
    public static void addModerationEntry(ModerationEntry entry) {
        Async.execute(() -> {
            DAOManager.getDAO(ModerationEntry.class).save(entry);
            BasicDAO<User, ObjectId> dao = DAOManager.getDAO(User.class);

            UpdateOperations<User> ops = dao.createUpdateOperations();
            ops.add("moderationHistory.entries", entry);

            if (entry.getAction() == ModerationAction.BAN) {
                ops.set("moderationHistory.activeBan", entry);
            } else if (entry.getAction() == ModerationAction.MUTE) {
                ops.set("moderationHistory.activeMute", entry);
            }

            dao.update(dao.createQuery().field("uuid").equal(entry.getUuid()), ops);
        });
    }

    /**
     * Sets a user's play time and/or moment last online.
     *
     * @param uuid
     * @param playTime
     * @param lastOnline
     */
    public static void setTimeStats(UUID uuid, long playTime, long lastOnline) {
        Async.execute(() -> {
            BasicDAO<User, ObjectId> dao = DAOManager.getDAO(User.class);
            UpdateOperations<User> operations = dao.createUpdateOperations();
            operations.inc("playTime", playTime);
            operations.set("lastOnline", lastOnline);
            dao.update(dao.createQuery().field("uuid").equal(uuid.toString()), operations);
        });
    }

    /**
     * Sets the user's last server they were seen on.
     *
     * @param uuid
     * @param name
     */
    public static void setLastServer(UUID uuid, String name) {
        Async.execute(() -> {
            BasicDAO<User, ObjectId> dao = DAOManager.getDAO(User.class);
            UpdateOperations<User> operations = dao.createUpdateOperations();
            operations.set("lastServer", name);
            dao.update(dao.createQuery().field("uuid").equal(uuid.toString()), operations);
        });
    }

    /**
     * Initializes RankData for all rank enums and
     * stores them in the permissions manager.
     */
    public static List<RankData> initRankData() {
        Log.debug("Initializing rank data!");
        BasicDAO<RankData, ObjectId> dao = DAOManager.getDAO(RankData.class);
        Map<Rank, RankData> mappedData = Maps.newHashMap();

        for (RankData data : dao.find().asList()) {
            mappedData.put(data.getRank(), data);
        }

        for (Rank rank : Rank.values()) {
            if (!mappedData.containsKey(rank)) {
                Log.debug("Creating data for rank " + rank.name() );
                RankData data = new RankData(rank);
                Log.debug("Saving data for rank " + rank.name());
                dao.save(data);
                mappedData.put(rank, data);
            }
        }

        return new ArrayList<>(mappedData.values());
    }

    public static NetworkModerationData initGlobalModerationData() {
        BasicDAO<NetworkModerationData, ObjectId> dao = DAOManager.getDAO(NetworkModerationData.class);
        NetworkModerationData data = dao.find().get();

        if (data == null) {
            data = new NetworkModerationData();
            dao.save(data);
        }

        return data;
    }

    public static void banIps(List<String> ips) {
        Async.execute(() -> {
            BasicDAO<NetworkModerationData, ObjectId> dao = DAOManager.getDAO(NetworkModerationData.class);
            UpdateOperations<NetworkModerationData> ops = dao.createUpdateOperations();
            ops.addAll("ipBans", ips, false);
            dao.update(dao.createQuery(), ops);
        });
    }

    public static void unbanIps(List<String> ips) {
        Async.execute(() -> {
            BasicDAO<NetworkModerationData, ObjectId> dao = DAOManager.getDAO(NetworkModerationData.class);
            UpdateOperations<NetworkModerationData> ops = dao.createUpdateOperations();
            ops.removeAll("ipBans", ips);
            dao.update(dao.createQuery(), ops);
        });
    }

    public static void addPoints(int points, UUID uuid, boolean inform) {
        addPointsInternal(points, uuid, inform, true);
    }

    public static void addPoints(int points, String name, boolean inform) {
        INetworkUser user = SessionAPI.getUser(name);
        if (user != null) {
            addPoints(points, user.getUuid(), inform);
        } else {
            Async.execute(() -> {
                try {
                    UUID uuid = UUIDFetcher.getUUIDOf(name);
                    if (uuid != null) {
                        addPointsInternal(points, uuid, inform);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
    }

    private static void addPointsInternal(int points, UUID uuid, boolean inform, boolean async) {
        if (async) {
            Async.execute(() -> {
                addPointsInternal(points, uuid, inform);
            });
        } else {
            addPointsInternal(points, uuid, inform);
        }
    }

    private static void addPointsInternal(int points, UUID uuid, boolean inform) {
        BasicDAO<User, ObjectId> dao = DAOManager.getDAO(User.class);
        UpdateOperations<User> ops = dao.createUpdateOperations();
        ops.inc("points", points);
        dao.update(dao.createQuery().field("uuid").equal(uuid.toString()), ops);

        INetworkUser user = SessionAPI.getUser(uuid);
        if (user != null) {
            user.incrementPoints(points);
            if (inform) {
                user.sendMessages(MessageType.INFO, "You have been given " + points + " points!", "Your balance is now " + user.getPoints() + "!");
            }
        }
    }

    /**
     * Adds points to a user profile.
     *
     * @param points
     * @param user
     * @param inform
     */
    public static void addPoints(int points, User user, boolean inform) {
        if (user == null) {
            return;
        }

        addPoints(points, UUID.fromString(user.getUuid()), inform);
    }

    public static void addCoins(int coins, UUID uuid, boolean inform) {
        addCoinsInternal(coins, uuid, inform, true);
    }

    public static void addCoins(int coins, String name, boolean inform) {
        INetworkUser user = SessionAPI.getUser(name);
        if (user != null) {
            addCoins(coins, user.getUuid(), inform);
        } else {
            Async.execute(() -> {
                try {
                    UUID uuid = UUIDFetcher.getUUIDOf(name);
                    if (uuid != null) {
                        addCoinsInternal(coins, uuid, inform);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
    }

    private static void addCoinsInternal(int coins, UUID uuid, boolean inform, boolean async) {
        if (async) {
            Async.execute(() -> {
                addCoinsInternal(coins, uuid, inform);
            });
        } else {
            addCoinsInternal(coins, uuid, inform);
        }
    }

    private static void addCoinsInternal(int coins, UUID uuid, boolean inform) {
        BasicDAO<User, ObjectId> dao = DAOManager.getDAO(User.class);
        UpdateOperations<User> ops = dao.createUpdateOperations();
        ops.inc("coins", coins);
        dao.update(dao.createQuery().field("uuid").equal(uuid.toString()), ops);

        INetworkUser user = SessionAPI.getUser(uuid);
        if (user != null) {
            user.incrementCoins(coins);
            if (inform) {
                user.sendMessages(MessageType.INFO, "You have been given " + coins + " coins!", "Your balance is now " + user.getCoins() + "!");
            }
        }
    }

    /**
     * Adds coins to a user profile.
     *
     * @param coins
     * @param user
     * @param inform
     */
    public static void addCoins(int coins, User user, boolean inform) {
        if (user == null) {
            return;
        }

        addCoins(coins, UUID.fromString(user.getUuid()), inform);
    }

    public static void updateRankCoinsReceived(UUID uuid, Rank rank) {
        Async.execute(() -> {
            BasicDAO<User, ObjectId> dao = DAOManager.getDAO(User.class);
            UpdateOperations<User> ops = dao.createUpdateOperations();
            ops.add("rankCoinsReceived", rank);
            dao.update(dao.createQuery().field("uuid").equal(uuid.toString()), ops);
        });
    }

    public static UserDeliveryStats getDeliveryStats(UUID uuid) {
        BasicDAO<UserDeliveryStats, ObjectId> dao = DAOManager.getDAO(UserDeliveryStats.class);
        UserDeliveryStats stats = dao.findOne("uuid", uuid.toString());

        if (stats == null) {
            stats = new UserDeliveryStats(uuid);
            dao.save(stats);
        }

        INetworkUser player = SessionAPI.getUser(uuid);
        if (player != null) {
            player.setData(UserDeliveryStats.class, stats);
        }

        return stats;
    }

    public static void updateUserDeliveries(UUID uuid, DeliveryType type, Date date) {
        BasicDAO<UserDeliveryStats, ObjectId> dao = DAOManager.getDAO(UserDeliveryStats.class);
        UserDeliveryStats stats = getDeliveryStats(uuid);
        stats.getStats().put(type, date);

        UpdateOperations<UserDeliveryStats> ops = dao.createUpdateOperations();
        ops.set("stats", stats.getStats());
        dao.update(dao.createQuery().field("uuid").equal(uuid.toString()), ops);
    }
}

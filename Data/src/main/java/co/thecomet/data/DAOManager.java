package co.thecomet.data;

import co.thecomet.data.delivery.UserDeliveryStats;
import co.thecomet.data.hub.HubData;
import co.thecomet.data.moderation.ModerationEntry;
import co.thecomet.data.moderation.NetworkModerationData;
import co.thecomet.data.proxy.ProxySettings;
import co.thecomet.data.ranks.RankData;
import co.thecomet.data.transaction.Transaction;
import co.thecomet.data.user.User;
import co.thecomet.data.user.UserHubData;
import co.thecomet.database.mongodb.ResourceManager;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.dao.BasicDAO;

import java.util.List;
import java.util.Map;

public class DAOManager {
    private static Map<Class, BasicDAO<?, ObjectId>> daoStore = Maps.newHashMap();
    private static List<Class> entities = Lists.newArrayList();
    private static Datastore datastore = null;

    static {
        entities.add(HubData.class);
        entities.add(ModerationEntry.class);
        entities.add(NetworkModerationData.class);
        entities.add(ProxySettings.class);
        entities.add(RankData.class);
        entities.add(Transaction.class);
        entities.add(User.class);
        entities.add(UserDeliveryStats.class);
        entities.add(UserHubData.class);
    }

    public static <T> BasicDAO<T, ObjectId> getDAO(Class<T> clazz) {
        if (datastore == null) {
            Morphia morphia = ResourceManager.getInstance().getMorphia();
            entities.stream().filter(entity -> !morphia.isMapped(entity)).forEach(morphia::map);

            Datastore datastore = ResourceManager.getInstance().getDatastore();
            datastore.ensureIndexes();
            datastore.ensureCaps();
            DAOManager.datastore = datastore;
        }

        BasicDAO<T, ObjectId> dao = daoStore.containsKey(clazz) ? (BasicDAO<T, ObjectId>) daoStore.get(clazz) : prepareDAO(clazz);
        return dao;
    }

    protected static <T> BasicDAO<T, ObjectId> prepareDAO(Class<T> clazz) {
        BasicDAO<T, ObjectId> dao = new BasicDAO<>(clazz, datastore);
        daoStore.put(clazz, dao);
        return dao;
    }
}

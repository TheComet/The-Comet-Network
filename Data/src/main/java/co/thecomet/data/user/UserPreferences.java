package co.thecomet.data.user;

public class UserPreferences {
    public boolean playersVisible = true;
    public boolean chatEnabled = true;
    public boolean acceptingPrivateMessages = true;

    public boolean isPlayersVisible() {
        return playersVisible;
    }

    public void setPlayersVisible(boolean playersVisible) {
        this.playersVisible = playersVisible;
    }

    public boolean isChatEnabled() {
        return chatEnabled;
    }

    public void setChatEnabled(boolean chatEnabled) {
        this.chatEnabled = chatEnabled;
    }

    public boolean isAcceptingPrivateMessages() {
        return acceptingPrivateMessages;
    }

    public void setAcceptingPrivateMessages(boolean acceptingPrivateMessages) {
        this.acceptingPrivateMessages = acceptingPrivateMessages;
    }
}

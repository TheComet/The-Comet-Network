package co.thecomet.data.user;

import co.thecomet.common.serialization.JsonLocation;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;

import java.util.UUID;

@Entity(value = "network_player_hub_data", noClassnameStored = true)
public class UserHubData {
    @Id
    public ObjectId id;
    @Indexed(unique = true)
    public String uuid;
    public JsonLocation checkpoint;
    public UserPreferences preferences = new UserPreferences();
    
    public UserHubData() {}

    public UserHubData(UUID uuid) {
        this.uuid = uuid.toString();
    }
}

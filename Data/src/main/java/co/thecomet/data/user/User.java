package co.thecomet.data.user;

import co.thecomet.data.moderation.ModerationHistory;
import co.thecomet.data.ranks.Rank;
import co.thecomet.data.transaction.Transaction;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;

import java.util.ArrayList;
import java.util.List;

@ToString
@NoArgsConstructor
@Entity(value = "network_users", noClassnameStored = true)
public class User {
    @Getter
    @Id
    private ObjectId id;
    @Getter
    @Indexed(unique = true)
    private String uuid;
    @Getter @Setter
    @Indexed
    private String name;
    @Getter @Setter
    @Indexed
    private String nameLower;
    @Getter @Setter
    private Rank rank = Rank.DEFAULT;
    @Getter
    private List<String> nameHistory = new ArrayList<>();
    @Getter @Setter
    private String ip;
    @Getter
    private List<String> ipHistory = new ArrayList<>();
    @Getter @Setter
    private long playTime = 0;
    @Getter @Setter
    private long lastOnline = 0;
    @Getter @Setter
    private String lastServer = "";
    @Getter
    private ModerationHistory moderationHistory = new ModerationHistory();
    @Getter @Setter
    private int points = 0;
    @Getter @Setter
    private int coins = 0;
    @Getter
    private List<Transaction> transactions = new ArrayList<>();
    @Getter
    private List<Rank> rankCoinsReceived = Lists.newArrayList();

    public User(String uuid) {
        this.uuid = uuid;
    }
}

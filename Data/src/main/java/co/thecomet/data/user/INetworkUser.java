package co.thecomet.data.user;

import co.thecomet.common.chat.MessageType;
import co.thecomet.data.moderation.ModerationHistory;
import co.thecomet.data.ranks.Rank;
import co.thecomet.data.transaction.Transaction;

import java.util.List;
import java.util.UUID;

public interface INetworkUser {
    public UUID getUuid();

    public String getName();

    public void setName(String name);

    public String getNameLower();

    public void setNameLower(String name);

    public List<String> getNameHistory();

    public String getIp();

    public void setIp(String ip);

    public List<String> getIpHistory();

    public long getPlayTime();

    public void setPlayTime(long time);

    public void incrementPlayTime(long time);

    public long getLastOnline();

    public void setLastOnline(long time);

    public String getLastServer();

    public void setLastServer(String server);

    public Rank getRank();

    public void setRank(Rank rank);

    public ModerationHistory getModerationHistory();

    public int getPoints();

    public void setPoints(int points);

    public void incrementPoints(int points);

    public int getCoins();

    public void setCoins(int coins);

    public void incrementCoins(int coins);

    public List<Transaction> getTransactions();

    public void setTransactions(List<Transaction> transactions);

    public List<Rank> getRankCoinsReceived();

    public void sendMessage(MessageType type, String message);

    public void sendMessages(MessageType type, String ... messages);

    public User getProfile();

    public <T> T getData(Class<T> clazz);

    public <T> void setData(Class<T> clazz, T data);
}

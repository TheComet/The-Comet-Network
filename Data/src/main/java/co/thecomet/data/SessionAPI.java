package co.thecomet.data;

import co.thecomet.common.utils.Log;
import co.thecomet.data.user.INetworkUser;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class SessionAPI {
    private static Cache<UUID, INetworkUser> users = CacheBuilder.newBuilder().build();

    public static List<INetworkUser> getUsers() {
        return new ArrayList<>(users.asMap().values());
}

    public static INetworkUser addUser(INetworkUser user) {
        Log.debug("Adding user with uuid " + user.getUuid());
        users.put(user.getUuid(), user);
        return user;
    }

    public static INetworkUser getUser(UUID uuid) {
        return users.asMap().get(uuid);
    }

    public static INetworkUser getUser(String name) {
        for (INetworkUser user : getUsers()) {
            if (user.getNameLower().equalsIgnoreCase(name)) {
                return user;
            }
        }

        return null;
    }

    public static INetworkUser removeUser(UUID uuid) {
        INetworkUser user = getUser(uuid);
        users.invalidate(uuid);
        return user;
    }

    public static boolean containsUser(UUID uuid) {
        return users.asMap().containsKey(uuid);
    }
}

package co.thecomet.data.moderation;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Reference;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Embedded
public class ModerationHistory {
    @Getter
    @Reference
    private List<ModerationEntry> entries = new ArrayList<>();
    @Getter @Setter
    @Reference
    private ModerationEntry activeBan;
    @Getter @Setter
    @Reference
    private ModerationEntry activeMute;
}

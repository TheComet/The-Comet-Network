package co.thecomet.data.moderation;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.util.ArrayList;
import java.util.List;

@Entity(value = "network_moderation_data", noClassnameStored = true)
public class NetworkModerationData {
    @Id
    public ObjectId id;
    public List<String> ipBans = new ArrayList<>();
}

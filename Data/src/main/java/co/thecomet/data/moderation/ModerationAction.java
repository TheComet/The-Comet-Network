package co.thecomet.data.moderation;

public enum ModerationAction {
    BAN,
    IPBAN,
    KICK,
    MUTE;
}

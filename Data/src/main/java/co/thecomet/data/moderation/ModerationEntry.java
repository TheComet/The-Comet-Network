package co.thecomet.data.moderation;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;

import java.util.Date;

@NoArgsConstructor
@Entity(value = "network_users_moderation_history_entries", noClassnameStored = true)
public class ModerationEntry {
    @Getter
    @Id
    private ObjectId id;
    @Getter
    @Indexed
    private String uuid;
    @Getter
    @Indexed
    private String staffUuid;
    @Getter
    @Indexed
    private ModerationAction action;
    @Getter
    private String reason;
    @Getter
    private Date createdDate;
    @Getter
    private Date expiration;

    public ModerationEntry(String uuid, String staffUuid, ModerationAction action, String reason, long expiration) {
        this.uuid = uuid;
        this.staffUuid = staffUuid;
        this.action = action;
        this.reason = reason;
        this.createdDate = new Date(System.currentTimeMillis());
        this.expiration = expiration > 0 ? new Date(System.currentTimeMillis() + expiration) : null;
    }

    public ModerationEntry(String uuid, String staffUuid, ModerationAction action, String reason) {
        this(uuid, staffUuid, action, reason, -1);
    }
}

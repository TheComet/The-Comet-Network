package co.thecomet.data.proxy;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@NoArgsConstructor
@Entity(value = "network_proxy_settings", noClassnameStored = true)
public class ProxySettings {
    @Getter
    @Id
    private ObjectId id;
    @Getter @Setter
    private String motd = "";
    @Getter @Setter
    private int maxPlayers = 1000;
}

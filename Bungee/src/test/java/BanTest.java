import co.thecomet.bungee.utils.TimeFormatUtils;
import co.thecomet.data.moderation.ModerationAction;
import co.thecomet.data.moderation.ModerationEntry;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.UUID;

public class BanTest {
    @Test
    public void tempBan() {
        Date current = new Date(System.currentTimeMillis());

        ModerationEntry entry = new ModerationEntry(UUID.randomUUID().toString(), UUID.randomUUID().toString(), ModerationAction.BAN, "Griefing", TimeFormatUtils.getTime("1d"));
        Date target = entry.getExpiration();
        target.setTime(target.getTime() + 1000 * 60 * 5);
        Assert.assertTrue("Current is not before target.", current.before(target));

        System.out.println("Remaining Ban Time: " + TimeFormatUtils.convertToFormattedTime(target.getTime() - current.getTime(), true));
    }
}

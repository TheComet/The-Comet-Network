package co.thecomet.bungee.maintenance;

import co.thecomet.bungee.CometBungee;
import co.thecomet.data.DAOManager;
import co.thecomet.data.ranks.Rank;
import co.thecomet.data.user.User;
import com.archeinteractive.rc.RedisConnect;
import com.archeinteractive.rc.redis.pubsub.NetTaskSubscribe;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

import java.util.HashMap;

public class MaintenanceReceiver implements Listener {
    public MaintenanceReceiver() {
        RedisConnect.getRedis().registerChannel("maintenance");
        RedisConnect.getRedis().registerTask(this);
        ProxyServer.getInstance().getPluginManager().registerListener(CometBungee.getInstance(), this);
    }

    @NetTaskSubscribe(args = {"maintenance"}, name = "maintenance")
    public void onMaintenance(HashMap<String, Object> args) {
        Object m = args.get("maintenance");

        if (m == null) {
            return;
        }

        if (!(m instanceof Boolean)) {
            return;
        }

        boolean maintenance = ((Boolean)m).booleanValue();
        CometBungee.setMaintenance(maintenance);

        if (CometBungee.isMaintenance()) {
            for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
                ProxyServer.getInstance().getScheduler().runAsync(CometBungee.getInstance(), () -> {
                    User user = DAOManager.getDAO(User.class).findOne("uuid", player.getUniqueId().toString());
                    if (user == null || user.getRank().ordinal() < Rank.HELPER.ordinal()) {
                        player.disconnect("Currently under maintenance. We will be back very soon.");
                    }
                });
            }
        }
    }
    
    @EventHandler(priority = EventPriority.LOW)
    public void onLogin(LoginEvent event) {
        if (event.getConnection() == null) {
            ProxyServer.getInstance().getLogger().info("Connection Null");
            return;
        }

        if (event.getConnection().getUniqueId() == null) {
            ProxyServer.getInstance().getLogger().info("UUID Null");
            return;
        }



        if (CometBungee.isMaintenance()) {
            event.registerIntent(CometBungee.getInstance());
            ProxyServer.getInstance().getScheduler().runAsync(CometBungee.getInstance(), () -> {
                User user = DAOManager.getDAO(User.class).findOne("uuid", event.getConnection().getUniqueId().toString());
                if (user == null || user.getRank() == null || user.getRank().ordinal() < Rank.HELPER.ordinal()) {
                    event.setCancelReason("Currently under maintenance. We will be back very soon.");
                    event.setCancelled(true);
                }
                event.completeIntent(CometBungee.getInstance());
            });
        }
    }
}

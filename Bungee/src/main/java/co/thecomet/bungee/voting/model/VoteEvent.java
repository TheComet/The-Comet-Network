package co.thecomet.bungee.voting.model;

import net.md_5.bungee.api.plugin.Event;

/**
 * {@code VotifierEvent} is a custom Bukkit event class that is sent
 * synchronously to CraftBukkit's main thread allowing other plugins to listener
 * for votes.
 * <p/>
 * NOW SENT TO BUNGEECORDS MAIN THREAD ;P
 */
public class VoteEvent extends Event {
    /**
     * Encapsulated vote record.
     */
    private Vote vote;

    /**
     * Constructs a vote event that encapsulated the given vote record.
     *
     * @param vote vote record
     */
    public VoteEvent(final Vote vote) {
        this.vote = vote;
    }

    /**
     * Return the encapsulated vote record.
     *
     * @return vote record
     */
    public Vote getVote() {
        return vote;
    }
}
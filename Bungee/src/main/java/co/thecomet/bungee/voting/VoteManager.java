package co.thecomet.bungee.voting;

import co.thecomet.bungee.CometBungee;
import co.thecomet.bungee.voting.crypto.RSAIO;
import co.thecomet.bungee.voting.crypto.RSAKeygen;
import co.thecomet.bungee.voting.nio.VoteReceiver;
import lombok.Getter;
import net.md_5.bungee.api.scheduler.ScheduledTask;

import java.io.File;
import java.security.KeyPair;
import java.util.logging.Logger;

public class VoteManager {
    @Getter
    private CometBungee plugin;
    @Getter
    private Logger logger;
    @Getter
    private String version = "1.9";
    @Getter
    private KeyPair keyPair;
    @Getter
    private VoteReceiver voteReceiver;
    private ScheduledTask voteReceiverTask;

    public VoteManager(CometBungee plugin) {
        this.plugin = plugin;
        this.logger = plugin.getLogger();
        loadKey();
        startReceiver();
    }

    public void loadKey() {
        try {
            File dataFolder = plugin.getDataFolder();
            if(!dataFolder.exists()) dataFolder.mkdir();
            File rsaFolder = new File(dataFolder, "rsa");
            if(!rsaFolder.exists()){
                rsaFolder.mkdir();
                keyPair = RSAKeygen.generate(2048);
                RSAIO.save(rsaFolder, keyPair);
            }else{
                keyPair = RSAIO.load(rsaFolder);
                plugin.getLogger().info("Loaded RSA files.");
            }
        } catch (Exception e) {
            gracefulExit();
            e.printStackTrace();
            return;
        }
    }

    public void startReceiver() {
        // Initialize the receiver.
        try {
            voteReceiver = new VoteReceiver(this, "0.0.0.0", 8192);
            voteReceiverTask = plugin.getProxy().getScheduler().runAsync(plugin, voteReceiver);

            getLogger().info("Voting enabled.");
        } catch (Exception ex) {
            gracefulExit();
            return;
        }
    }

    private void gracefulExit() {
        getLogger().severe("Voting did not initialize properly!");

        if (voteReceiver != null) {
            voteReceiver.shutdown();
        }

        if (voteReceiverTask != null) {
            voteReceiverTask.cancel();
        }
    }
}

package co.thecomet.bungee.listeners;

import co.thecomet.bungee.CometBungee;
import co.thecomet.bungee.db.DeliveryDataAPI;
import co.thecomet.bungee.voting.model.Vote;
import co.thecomet.bungee.voting.model.VoteEvent;
import com.archeinteractive.rc.RedisConnect;
import com.archeinteractive.rc.redis.pubsub.NetTask;
import com.archeinteractive.rc.redis.pubsub.NetTaskSubscribe;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class VoteListener implements Listener {
    public VoteListener() {
        init();
    }

    private void init() {
        RedisConnect.getRedis().registerChannel("proxy-voting");
        RedisConnect.getRedis().registerTask(this);
    }

    @EventHandler
    public void onVote(VoteEvent event) {
        ProxyServer.getInstance().getScheduler().runAsync(CometBungee.getInstance(), () -> {
            boolean success = DeliveryDataAPI.addVote(event.getVote().getUsername());

            if (success) {
                ProxyServer.getInstance().getScheduler().schedule(CometBungee.getInstance(), () -> voteReceived(event.getVote()), 5, TimeUnit.SECONDS);
            }
        });
    }

    @NetTaskSubscribe(name = "voteReceived", args = {"serviceName", "username", "address", "timestamp"})
    public void voteReceived(HashMap<String, Object> args) {
        String serviceName = (String) args.get("serviceName");
        String username = (String) args.get("username");
        String address = (String) args.get("address");
        String timestamp = (String) args.get("timestamp");

        new ArrayList<>(ProxyServer.getInstance().getPlayers()).stream().forEach(player -> {
            if (player.getName().equalsIgnoreCase(username)) {
                forwardVote(serviceName, username, address, timestamp);
                return;
            }
        });
    }

    private void voteReceived(Vote vote) {
        NetTask.withName("voteReceived")
                .withArg("serviceName", vote.getServiceName())
                .withArg("username", vote.getUsername())
                .withArg("address", vote.getAddress())
                .withArg("timestamp", vote.getTimeStamp())
                .send("proxy-voting");
    }

    private void forwardVote(String serviceName, String username, String address, String timestamp) {
        NetTask.withName("forwardVote")
                .withArg("serviceName", serviceName)
                .withArg("username", username)
                .withArg("address", address)
                .withArg("timestamp", timestamp)
                .send("bukkit-voting");
    }
}

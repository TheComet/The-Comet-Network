package co.thecomet.bungee;

import co.thecomet.common.utils.Log;
import co.thecomet.data.DAOManager;
import co.thecomet.bungee.listeners.BanListener;
import co.thecomet.bungee.listeners.VoteListener;
import co.thecomet.bungee.maintenance.MaintenanceReceiver;
import co.thecomet.bungee.settings.ProxySettingsManager;
import co.thecomet.bungee.voting.VoteManager;
import co.thecomet.common.chat.FontColor;
import com.archeinteractive.rc.RedisConnect;
import com.archeinteractive.rc.redis.pubsub.NetTaskSubscribe;
import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.HashMap;

public class CometBungee extends Plugin {
    @Getter
    private static CometBungee instance;
    @Getter
    private static DAOManager daoManager;
    @Getter
    private static RedisHandler handler;
    @Getter
    private static VoteManager voteManager;
    @Getter @Setter
    private static boolean maintenance = false;
    
    public void onEnable() {
        Log.setLogger(getLogger());
        
        instance = this;
        daoManager = new DAOManager();

        ProxyServer.getInstance().getPluginManager().registerListener(this, new BanListener());
        ProxyServer.getInstance().setReconnectHandler(new HubBalancer());
        
        handler = new RedisHandler();
        voteManager = new VoteManager(this);

        getProxy().getPluginManager().registerListener(this, new VoteListener());
    }

    public class RedisHandler {
        public RedisHandler() {
            init();
        }
        
        private void init() {
            RedisConnect.getRedis().registerChannel("galert");
            RedisConnect.getRedis().registerTask(this);
            new MaintenanceReceiver();
            new ProxySettingsManager();
        }
        
        @NetTaskSubscribe(name = "galert", args = {"message"})
        public void galert(HashMap<String, Object> args) {
            for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
                player.sendMessage(ChatMessageType.CHAT, new TextComponent(FontColor.translateString(String.format(getFormat(), String.valueOf(args.get("message"))))));
            }
        }
        
        private String getFormat() {
            return "&8[&c&lALERT&r&8] &r%s";
        }
    }
}

package co.thecomet.bungee;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ReconnectHandler;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class HubBalancer implements ReconnectHandler {
    @Override
    public ServerInfo getServer(ProxiedPlayer proxiedPlayer) {
        return getAvailableHub();
    }

    @Override
    public void setServer(ProxiedPlayer proxiedPlayer) {
        //
    }

    @Override
    public void save() {
        //
    }

    @Override
    public void close() {
        //
    }
    
    private ServerInfo getAvailableHub() {
        ServerInfo destination = null;
        for (ServerInfo info : ProxyServer.getInstance().getServers().values()) {
            if (info.getName().toUpperCase().startsWith("HUB")) {
                // TODO: Update VIP checking to be independent of redis plugin
                if (destination == null || info.getPlayers().size() < destination.getPlayers().size()) {
                    destination = info;
                }
            }
        }
        
        return destination;
    }
}

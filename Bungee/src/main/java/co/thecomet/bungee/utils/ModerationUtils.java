package co.thecomet.bungee.utils;

import co.thecomet.data.moderation.ModerationEntry;
import co.thecomet.data.moderation.ModerationHistory;

import java.util.Date;

public class ModerationUtils {
    public static boolean isBanned(ModerationHistory history) {
        ModerationEntry ban = history.getActiveBan();
        if (ban != null) {
            if (ban.getExpiration() != null) {
                if (new Date(System.currentTimeMillis()).before(ban.getExpiration())) {
                    return true;
                }
            } else {
                return true;
            }
        }
        return false;
    }
}

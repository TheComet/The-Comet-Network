package co.thecomet.bungee.settings;

import co.thecomet.bungee.CometBungee;
import co.thecomet.data.proxy.ProxySettings;
import co.thecomet.data.DAOManager;
import co.thecomet.common.chat.FontColor;
import co.thecomet.data.ranks.Rank;
import co.thecomet.data.user.User;
import com.archeinteractive.rc.RedisConnect;
import com.archeinteractive.rc.bungee.server.ServerHeartbeatHandler;
import com.archeinteractive.rc.redis.pubsub.NetTask;
import com.archeinteractive.rc.redis.pubsub.NetTaskSubscribe;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import org.apache.commons.lang.StringUtils;
import org.mongodb.morphia.query.UpdateOperations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ProxySettingsManager extends Command implements Listener {
    private ProxySettings settings = null;
    private List<String> subcommands = new ArrayList<String>(){{
        add("setmotd");
        add("setmaxplayers");
    }};
    
    public ProxySettingsManager() {
        super("proxy");

        ProxyServer.getInstance().getPluginManager().registerCommand(CometBungee.getInstance(), this);
        ProxyServer.getInstance().getPluginManager().registerListener(CometBungee.getInstance(), this);
        RedisConnect.getRedis().registerChannel("proxysettings");
        RedisConnect.getRedis().registerTask(this);

        settings = loadProxySettings();
    }
    
    @EventHandler
    public void onPing(ProxyPingEvent event) {
        String motd1 = "";
        String motd2 = "";
        if (settings != null) {
            if (settings.getMotd() != null) {
                String[] lines = settings.getMotd().split("%n");
                if (lines.length >= 1) {
                    motd1 = lines[0];

                    if (lines.length >= 2) {
                        motd2 = lines[1];
                    }
                }
            }
        }
        
        event.getResponse().getPlayers().setOnline(ServerHeartbeatHandler.getPlayersOnline());
        event.getResponse().getPlayers().setMax(settings.getMaxPlayers());
        event.setResponse(new ServerPing(event.getResponse().getVersion(),
                event.getResponse().getPlayers(),
                FontColor.translateString(motd1, true) + "\n" + FontColor.translateString(motd2),
                event.getResponse().getFaviconObject()));
    }
    
    @EventHandler
    public void onPreLogin(PreLoginEvent event) {
        if (ProxyServer.getInstance().getOnlineCount() >= settings.getMaxPlayers()) {
            event.setCancelReason("The network is full. Please try again soon!");
            event.setCancelled(true);
        }
    }
    
    @NetTaskSubscribe(name = "setmotd", args = {"motd"})
    public void motd(HashMap<String, Object> data) {
        if (data.get("motd") == null || data.get("motd").equals("")) {
            settings.setMotd(null);
        } else {
            settings.setMotd(String.class.cast(data.get("motd")));
        }
    }
    
    @NetTaskSubscribe(name = "setmaxplayers", args = {"maxPlayers"})
    public void maxPlayers(HashMap<String, Object> data) {
        int maxPlayers = 1000;
        try {
            maxPlayers = Number.class.cast(data.get("maxPlayers")).intValue();
        } catch (NumberFormatException e) {}
        settings.setMaxPlayers(maxPlayers);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxyServer.getInstance().getScheduler().runAsync(CometBungee.getInstance(), () -> {
            User user = sender instanceof ProxiedPlayer ? DAOManager.getDAO(User.class).findOne("uuid", ((ProxiedPlayer) sender).getUniqueId().toString()) : null;
            if (user == null || user.getRank().ordinal() < Rank.ADMIN.ordinal()) {
                sender.sendMessage(FontColor.translateString("&c&l»&4&l» &4&lERROR&7: &cPermission Denied"));
                return;
            }

            if (args.length < 1) {
                usage(sender);
                return;
            }

            if (subcommands.contains(args[0].toLowerCase()) == false) {
                usage(sender);
                return;
            }

            switch (args[0].toLowerCase()) {
                case "setmotd":
                    String motd = args.length == 1 ? "" : getMotd(Arrays.copyOfRange(args, 1, args.length));
                    NetTask.withName("setmotd")
                            .withArg("motd", motd)
                            .send("proxysettings");
                    updateMotd(motd);
                    sender.sendMessage(FontColor.translateString("&a&l»&2&l» &a&lSUCCESS&7: &aThe motd has been updated!"));
                    break;
                case "setmaxplayers":
                    int maxPlayers;
                    try {
                        maxPlayers = args.length == 1 ? 1000 : Integer.parseInt(args[1]);
                        NetTask.withName("setmaxplayers")
                                .withArg("maxPlayers", maxPlayers)
                                .send("proxysettings");
                    } catch (NumberFormatException e) {
                        usage(sender);
                        return;
                    }
                    updateMaxPlayers(maxPlayers);
                    sender.sendMessage(FontColor.translateString("&a&l»&2&l» &a&lSUCCESS&7: &aThe max players has been set!"));
                    break;
            }
        });
    }
    
    public void usage(CommandSender sender) {
        sender.sendMessage(FontColor.translateString("&e&l»&6&l» &4&lUSAGE&7: &6/proxy setmotd <motd>"));
        sender.sendMessage(FontColor.translateString("&e&l»&6&l» &4&lUSAGE&7: &6/proxy setmaxplayers <maxPlayers>"));
    }
    
    public void updateMotd(String motd) {
        UpdateOperations<ProxySettings> ops = DAOManager.getDAO(ProxySettings.class).createUpdateOperations();
        ops.set("motd", motd);
        DAOManager.getDAO(ProxySettings.class).update(DAOManager.getDAO(ProxySettings.class).createQuery(), ops);
    }

    public void updateMaxPlayers(int maxPlayers) {
        UpdateOperations<ProxySettings> ops = DAOManager.getDAO(ProxySettings.class).createUpdateOperations();
        ops.set("maxPlayers", maxPlayers);
        DAOManager.getDAO(ProxySettings.class).update(DAOManager.getDAO(ProxySettings.class).createQuery(), ops);
    }
    
    public String getMotd(String[] args) {
        return StringUtils.join(args, " ");
    }

    private ProxySettings loadProxySettings() {
        ProxySettings settings = DAOManager.getDAO(ProxySettings.class).find().get();
        if (settings == null) {
            settings = new ProxySettings();
            DAOManager.getDAO(ProxySettings.class).save(settings);
        }
        return settings;
    }
}

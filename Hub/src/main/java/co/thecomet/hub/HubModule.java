package co.thecomet.hub;

import co.thecomet.core.serialization.BukkitJsonLocation;
import co.thecomet.data.SessionAPI;
import co.thecomet.common.chat.FontColor;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.command.CommandRegistry;
import co.thecomet.common.serialization.JsonLocation;
import co.thecomet.core.moderation.commands.TeleportCommands;
import co.thecomet.core.module.Module;
import co.thecomet.core.module.ModuleInfo;
import co.thecomet.core.module.ModuleManager;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.data.ranks.Rank;
import co.thecomet.data.user.INetworkUser;
import co.thecomet.hub.commands.DonorCommands;
import co.thecomet.hub.db.HubDataAPI;
import co.thecomet.data.hub.HubData;
import co.thecomet.data.user.UserHubData;
import co.thecomet.hub.listeners.ConnectionListener;
import co.thecomet.hub.listeners.HubListener;
import co.thecomet.hub.modules.delivery.DeliveryModule;
import co.thecomet.hub.voting.VoteHandler;
import co.thecomet.hub.modules.parkour.ParkourModule;
import co.thecomet.hub.modules.scoreboard.ScoreboardModule;
import co.thecomet.hub.modules.toolbar.ToolbarModule;
import co.thecomet.hub.modules.cosmetic.CosmeticsModule;
import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.google.common.collect.Lists;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@ModuleInfo(name = "hub")
public class HubModule extends Module {
    @Getter
    private static HubModule instance;
    @Getter
    private static HubData hubData;
    @Getter
    private static List<JsonLocation> loadedDeliveryBoxes = Lists.newArrayList();

    @Override
    public void onEnable() {
        instance = this;
        prepareWorlds();
        loadHubData();

        CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "setspawn", Rank.ADMIN, HubModule::configureSpawn);
        CommandRegistry.registerUniversalCommand(CoreAPI.getPlugin(), "update", Rank.ADMIN, HubModule::update);
        CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "spawn", HubModule::spawn);
        new DonorCommands();

        ModuleManager.registerModule(new ScoreboardModule());
        ModuleManager.registerModule(new ToolbarModule());
        ModuleManager.registerModule(new ParkourModule());
        ModuleManager.registerModule(new CosmeticsModule());
        ModuleManager.registerModule(new DeliveryModule());

        new VoteHandler();
    }

    @Override
    public void onDisable() {
        CommandRegistry.unregisterCommand("setspawn");
        CommandRegistry.unregisterCommand("update");
    }

    @Override
    public List<Listener> registerListeners() {
        return new ArrayList<Listener>(){{
            add(new ConnectionListener());
            add(new HubListener());
            add(new TeleportCommands(true, Rank.FRIEND, true, Rank.FRIEND, true, Rank.DEFAULT, true, Rank.DEFAULT, true, Rank.ADMIN));
        }};
    }
    
    public void prepareWorlds() {
        for (World world : Bukkit.getWorlds()) {
            world.setAutoSave(false);
            world.setGameRuleValue("doMobSpawning", String.valueOf(false));
        }
    }

    public void loadHubData() {
        hubData = HubDataAPI.initHubData();

        for (JsonLocation location : hubData.deliveryBoxes) {
            BukkitJsonLocation loc = new BukkitJsonLocation(location);
            Block block = loc.getBlock();
            if (block != null && block.getType() == Material.ENDER_CHEST) {
                boolean loaded = false;
                for (JsonLocation location1 : loadedDeliveryBoxes) {
                    if (location.equals(location1)) {
                        loaded = true;
                    }
                }

                if (!loaded) {
                    Hologram hologram = HologramsAPI.createHologram(CoreAPI.getPlugin(), loc.getLocation().add(0.5, 1.5, 0.5));
                    hologram.appendTextLine(FontColor.translateString("&cDelivery Box"));
                }
            }
        }
    }

    public UserHubData getPlayerHubData(UUID uuid) {
        INetworkUser user = SessionAPI.getUser(uuid);
        UserHubData data = null;

        if (user == null) {
            return null;
        }

        if ((data = user.getData(UserHubData.class)) == null) {
            data = Hub.getPlayerHubDataDAO().findOne(Hub.getPlayerHubDataDAO().createQuery().field("uuid").equal(uuid.toString()));

            if (data == null) {
                data = new UserHubData(uuid);
                final UserHubData finalData = data;
                CoreAPI.async(() -> Hub.getPlayerHubDataDAO().save(finalData));
            }

            user.setData(UserHubData.class, data);
        }

        return data;
    }

    public static void configureSpawn(Player sender, String[] args) {
        hubData.spawn = new BukkitJsonLocation(sender.getLocation()).getSerializedLocation();
        sender.getLocation().getWorld().setSpawnLocation(sender.getLocation().getBlockX(), sender.getLocation().getBlockY(), sender.getLocation().getBlockZ());
        Hub.getHubDataDAO().save(hubData);
        MessageFormatter.sendSuccessMessage(sender, "The lobby spawn has been set to your position.");
    }

    public static void update(CommandSender sender, String[] args) {
        HubModule.getInstance().loadHubData();
        MessageFormatter.sendSuccessMessage(sender, "Lobby data has been reloaded.");
    }
    
    public static void spawn(Player sender, String[] args) {
        if (args.length > 0) {
            MessageFormatter.sendUsageMessage(sender, "/spawn");
            return;
        }
        
        sender.teleport(new BukkitJsonLocation(hubData.spawn).getLocation());
    }
}

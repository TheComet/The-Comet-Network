package co.thecomet.hub.db;

import co.thecomet.core.CoreAPI;
import co.thecomet.common.serialization.JsonLocation;
import co.thecomet.core.serialization.BukkitJsonLocation;
import co.thecomet.hub.Hub;
import co.thecomet.data.hub.HubData;
import co.thecomet.data.user.UserHubData;
import co.thecomet.data.user.UserPreferences;
import org.bukkit.Bukkit;
import org.mongodb.morphia.query.UpdateOperations;

import java.util.UUID;

public class HubDataAPI {
    public static UserHubData getPlayerHubData(UUID uuid) {
        return Hub.getPlayerHubDataDAO().findOne("uuid", uuid.toString());
    }

    public static void updateCheckpoint(UUID uuid, UserHubData data) {
        CoreAPI.async(() -> {
            UpdateOperations<UserHubData> ops = Hub.getPlayerHubDataDAO().createUpdateOperations();
            ops.set("checkpoint", data.checkpoint);
            Hub.getPlayerHubDataDAO().update(Hub.getPlayerHubDataDAO().createQuery().field("uuid").equal(uuid.toString()), ops);
        });
    }

    public static HubData initHubData() {
        HubData data = Hub.getInstance().getHubDataDAO().find().get();
        if (data == null) {
            data = new HubData();
            data.spawn = new BukkitJsonLocation(Bukkit.getWorlds().get(0).getSpawnLocation()).getSerializedLocation();
            Hub.getInstance().getHubDataDAO().save(data);
        }
        return data;
    }

    public static void updatePreferences(UUID uuid, UserPreferences preferences) {
        CoreAPI.async(() -> {
            UpdateOperations<UserHubData> ops = Hub.getPlayerHubDataDAO().createUpdateOperations();
            ops.set("preferences", preferences);
            Hub.getPlayerHubDataDAO().update(Hub.getPlayerHubDataDAO().createQuery().field("uuid").equal(uuid.toString()), ops);
        });
    }

    public static void addDeliveryBox(JsonLocation location) {
        UpdateOperations<HubData> ops = Hub.getHubDataDAO().createUpdateOperations();
        ops.add("deliveryBoxes", location);
        Hub.getHubDataDAO().update(Hub.getHubDataDAO().createQuery(), ops);
    }
}

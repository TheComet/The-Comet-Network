package co.thecomet.hub.listeners;

import co.thecomet.data.SessionAPI;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.events.LoginSuccessEvent;
import co.thecomet.data.ranks.Rank;
import co.thecomet.data.user.INetworkUser;
import co.thecomet.hub.Hub;
import co.thecomet.hub.db.HubDataAPI;
import co.thecomet.data.user.UserHubData;
import co.thecomet.hub.modules.preferences.PreferencesMenu;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffect;

public class ConnectionListener implements Listener {
    @EventHandler
    public void onLoginSuccess(LoginSuccessEvent event) {
        INetworkUser player = event.getProfile();
        UserHubData data = HubDataAPI.getPlayerHubData(player.getUuid());

        if (data == null) {
            data = new UserHubData(player.getUuid());

            final UserHubData finalData = data;
            CoreAPI.async(() -> Hub.getPlayerHubDataDAO().save(finalData));
        }

        player.setData(UserHubData.class, data);
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerJoin(PlayerJoinEvent event) {
        for (PotionEffect effect : event.getPlayer().getActivePotionEffects()) {
            event.getPlayer().removePotionEffect(effect.getType());
        }

        INetworkUser player = SessionAPI.getUser(event.getPlayer().getUniqueId());
        if (player != null && player.getRank().ordinal() >= Rank.ULTRA.ordinal()) {
            if (!event.getPlayer().getAllowFlight()) {
                event.getPlayer().setAllowFlight(true);
            }
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        PreferencesMenu.cleanup(event.getPlayer());
    }
}

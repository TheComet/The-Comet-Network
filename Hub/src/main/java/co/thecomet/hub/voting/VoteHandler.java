package co.thecomet.hub.voting;

import co.thecomet.data.SessionAPI;
import co.thecomet.common.chat.MessageType;
import co.thecomet.data.user.INetworkUser;
import com.archeinteractive.rc.RedisConnect;
import com.archeinteractive.rc.redis.pubsub.NetTaskSubscribe;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.HashMap;

public class VoteHandler {
    public VoteHandler() {
        RedisConnect.getRedis().registerChannel("bukkit-voting");
        RedisConnect.getRedis().registerTask(this);
    }

    @NetTaskSubscribe(name = "forwardVote", args = {"serviceName", "username", "address", "timestamp"})
    public void forwardVote(HashMap<String, Object> args) {
        String serviceName = (String) args.get("serviceName");
        String username = (String) args.get("username");
        String address = (String) args.get("address");
        String timestamp = (String) args.get("timestamp");

        new ArrayList<>(Bukkit.getOnlinePlayers()).stream().forEach(player -> {
            if (player.getName().equalsIgnoreCase(username)) {
                INetworkUser user = SessionAPI.getUser(player.getUniqueId());

                if (user != null) {
                    user.setPoints(user.getPoints() + 500);
                }

                user.sendMessage(MessageType.INFO, "You have received 500 points for voting!");
            }
        });
    }
}

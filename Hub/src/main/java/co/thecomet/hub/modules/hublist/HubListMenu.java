package co.thecomet.hub.modules.hublist;

import co.thecomet.data.SessionAPI;
import co.thecomet.common.chat.FontColor;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.ui.Menu;
import co.thecomet.core.ui.MenuItem;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.core.utils.ProxyUtil;
import co.thecomet.data.ranks.Rank;
import co.thecomet.data.user.INetworkUser;
import co.thecomet.hub.Hub;
import co.thecomet.hub.status.ServerInfo;
import co.thecomet.hub.status.ServerType;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HubListMenu extends Menu implements Runnable {
    private boolean cancelled = false;
    private int id;

    public HubListMenu() {
        super(FontColor.translateString(ServerType.HUB.getInventoryDisplay()), 1);

        id = Bukkit.getScheduler().runTaskTimer(CoreAPI.getPlugin(), this, 20, 20).getTaskId();
    }

    @Override
    public void run() {
        if (cancelled) {
            return;
        }

        update();
    }

    private void update() {
        List<ServerInfo> infos = new ArrayList<>(retrieveHubs());

        Collections.sort(infos, (ServerInfo i1, ServerInfo i2) -> {
            int id1 = 0, id2 = 0;
            try {
                id1 = Integer.parseInt(i1.getId().replace(ServerType.HUB.name(), ""));
                id2 = Integer.parseInt(i2.getId().replace(ServerType.HUB.name(), ""));
            } catch (Exception e) {}
            return Integer.compare(id1, id2);
        });

        process(infos);
        reload();
    }

    private List<ServerInfo> retrieveHubs() {
        return Hub.getInstance().getServerInfoListener().get(ServerType.HUB);
    }

    public void process(List<ServerInfo> infos) {
        int index = 0;

        for (int i = 0; i < items.length; i++) {
            if (i < getInventory().getSize()) {
                removeMenuItem(i);
            }
        }
        
        for (ServerInfo info : infos) {
            if (info == null) {
                continue;
            }

            MenuItem item = create(info);
            item.setQuantity(info.getPlayersOnline());
            item.setDescriptions(new ArrayList<String>() {{
                add(FontColor.BOLD.toString() + FontColor.ORANGE + "Players: " + FontColor.RED + info.getPlayersOnline() + org.bukkit.ChatColor.GOLD + " / " + org.bukkit.ChatColor.RED + info.getMaxPlayers());
            }});

            if (index >= getInventory().getSize()) {
                return;
            }

            put(item, index);
            index += 1;
        }
    }

    public MenuItem create(ServerInfo info) {
        return new MenuItem(getDisplay(info), new MaterialData(Material.STAINED_CLAY, getData(info))) {
            @Override
            public void onClick(Player player) {
                if (info.getPlayersOnline() < info.getMaxPlayers()) {
                    if (info.isVipOnly()) {
                        INetworkUser user = SessionAPI.getUser(player.getUniqueId());
                        if (user.getRank() == Rank.DEFAULT) {
                            MessageFormatter.sendErrorMessage(player, "You must have purchase a rank to join this hub.", "You can buy a rank at store.thecomet.co");
                            return;
                        }
                    }

                    ProxyUtil.send(player, info.getId());
                } else {
                    MessageFormatter.sendErrorMessage(player, "The server is currently full.");
                }
            }
        };
    }

    public byte getData(ServerInfo info) {
        if (info.getId().equalsIgnoreCase(CoreAPI.getPlugin().getId())) {
            return DyeColor.MAGENTA.getData();
        }

        if (info.getPlayersOnline() < info.getMaxPlayers()) {
            return info.isVipOnly() ? DyeColor.LIGHT_BLUE.getData() : DyeColor.WHITE.getData();
        }

        return DyeColor.RED.getData();
    }

    public String getDisplay(ServerInfo info) {
        return (info.isVipOnly() ? ServerType.HUB.getVipDisplay() : ServerType.HUB.getDisplay()) + " " + info.getId().replace(ServerType.HUB.name(), "");
    }

    public void put(MenuItem item, int index) {
        this.addMenuItem(item, index);
    }

    public void reload() {
        updateMenu();
        updateInventory();
    }

    public void cancel() {
        this.cancelled = true;
        Bukkit.getScheduler().cancelTask(id);
    }
}

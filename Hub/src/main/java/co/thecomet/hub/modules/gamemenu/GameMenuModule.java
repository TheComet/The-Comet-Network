package co.thecomet.hub.modules.gamemenu;

import co.thecomet.common.chat.FontColor;
import co.thecomet.core.module.Module;
import co.thecomet.core.module.ModuleInfo;
import co.thecomet.core.ui.MenuItem;
import co.thecomet.hub.modules.toolbar.ToolbarModule;
import co.thecomet.hub.modules.toolbar.Toolbar;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

@ModuleInfo(name = "gamelist", depends = {ToolbarModule.class})
public class GameMenuModule extends Module {
    private GameMenu menu;
    private MenuItem toolbarItem;

    @Override
    public void onEnable() {
        menu = new GameMenu();
        addToolbarItem();
    }

    @Override
    public void onDisable() {
        Toolbar.unregister(toolbarItem);
        menu.disable();
    }

    public void addToolbarItem() {
        toolbarItem = new MenuItem(FontColor.translateString("&2&lG&aame &2&lM&aenu "), new MaterialData(Material.COMPASS)) {
            @Override
            public void onClick(Player player) {
                if (menu.getInventory().getViewers().contains(player) == false) {
                    menu.openMenu(player);
                }
            }
        };
        toolbarItem.setSlot(0);

        Toolbar.register(this, toolbarItem);
    }
}

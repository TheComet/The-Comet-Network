package co.thecomet.hub.modules.scoreboard;

import co.thecomet.data.SessionAPI;
import co.thecomet.common.chat.FontColor;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.utils.scoreboard.SimpleScoreboard;
import co.thecomet.data.user.INetworkUser;
import co.thecomet.hub.status.ServerInfoListener;
import co.thecomet.hub.status.ServerType;
import com.google.common.collect.Maps;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Map;
import java.util.UUID;

public class ScoreboardManager implements Runnable, Listener {
    private static Map<UUID, SimpleScoreboard> scoreboards = Maps.newHashMap();

    @Override
    public void run() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            update(player);
        }
    }

    public void update(Player player) {
        if (player == null) {
            return;
        }
        
        INetworkUser user = SessionAPI.getUser(player.getUniqueId());
        if (user == null) {
            return;
        }

        SimpleScoreboard scoreboard = null;
        if (scoreboards.containsKey(user.getUuid())) {
            scoreboard = scoreboards.get(user.getUuid());
        } else {
            scoreboard = new SimpleScoreboard(FontColor.ORANGE.toString() + FontColor.BOLD.toString() + "The Comet");
            scoreboards.put(user.getUuid(), scoreboard);

            scoreboard.add(FontColor.YELLOW.toString() + "Players Online", 14);
            scoreboard.add(String.valueOf(ServerInfoListener.getOnlinePlayers()) + FontColor.DARK_GREY, 13);
            scoreboard.add(FontColor.translateString("&a"), 12);
            scoreboard.add(FontColor.GREEN.toString() + "Connected To", 11);
            scoreboard.add("Hub " + CoreAPI.getPlugin().getId().replace(ServerType.HUB.name(), ""), 10);
            scoreboard.add(FontColor.translateString("&b"), 9);
            scoreboard.add(FontColor.DARK_AQUA.toString() + "Coins", 8);
            scoreboard.add(String.valueOf(user.getCoins()) + FontColor.GREY, 7);
            scoreboard.add(FontColor.translateString("&c"), 6);
            scoreboard.add(FontColor.ORANGE.toString() + "Points", 5);
            scoreboard.add(String.valueOf(user.getPoints()), 4);
            scoreboard.add(FontColor.translateString("&d"), 3);
            scoreboard.add(FontColor.RED.toString() + "Website", 2);
            scoreboard.add("Coming Soon!", 1);
        }

        if (!scoreboard.containsScore(13)) {
            scoreboard.add(String.valueOf(ServerInfoListener.getOnlinePlayers()) + FontColor.DARK_GREY, 13);
        } else if (!scoreboard.get(13, null).equals(String.valueOf(ServerInfoListener.getOnlinePlayers()) + FontColor.DARK_GREY)) {
            scoreboard.remove(13, scoreboard.get(13, null));
            scoreboard.add(String.valueOf(ServerInfoListener.getOnlinePlayers()) + FontColor.DARK_GREY, 13);
        }

        if (!scoreboard.containsScore(7)) {
            scoreboard.add(String.valueOf(user.getCoins()) + FontColor.GREY, 7);
        } else if (!scoreboard.get(7, null).equals(String.valueOf(user.getCoins()) + FontColor.GREY)) {
            scoreboard.remove(7, scoreboard.get(7, null));
            scoreboard.add(String.valueOf(user.getCoins()) + FontColor.GREY, 7);
        }

        if (!scoreboard.containsScore(4)) {
            scoreboard.add(String.valueOf(user.getPoints()), 4);
        } else if (!scoreboard.get(4, null).equals(String.valueOf(user.getPoints()))) {
            scoreboard.remove(4, scoreboard.get(4, null));
            scoreboard.add(String.valueOf(user.getPoints()), 4);
        }

        scoreboard.update();
        scoreboard.send(player);
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        update(event.getPlayer());
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        scoreboards.remove(event.getPlayer().getUniqueId());
    }
}

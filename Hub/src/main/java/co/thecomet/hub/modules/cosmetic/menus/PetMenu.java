package co.thecomet.hub.modules.cosmetic.menus;

import co.thecomet.common.chat.FontColor;
import co.thecomet.core.ui.MenuItem;
import co.thecomet.data.ranks.Rank;
import co.thecomet.data.transaction.CurrencyType;
import co.thecomet.data.transaction.Feature;
import co.thecomet.hub.modules.cosmetic.Unlockable;
import co.thecomet.hub.modules.cosmetic.UnlockableMenu;
import co.thecomet.hub.modules.cosmetic.unlockables.pets.PetUnlockable;
import com.dsh105.echopet.compat.api.entity.PetType;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

public class PetMenu extends UnlockableMenu {
    protected CosmeticMenu mainMenu;
    protected final MenuItem item = new MenuItem(FontColor.translateString("&6Pets"), new MaterialData(Material.MONSTER_EGG, (byte) EntityType.OCELOT.getTypeId())) {
        @Override
        public void onClick(Player player) {
            if (getInventory().getViewers().contains(player) == false) {
                openMenu(player);
            }
        }
    };

    public PetMenu(CosmeticMenu mainMenu) {
        super(FontColor.translateString("&6Pets"), 6);
        this.mainMenu = mainMenu;
        this.item.setSlot(7);

        init();
    }

    private void init() {
        registerUnlockable(this, new PetUnlockable(FontColor.translateString("&cChicken"), PetType.CHICKEN, Material.MONSTER_EGG, (byte) EntityType.CHICKEN.getTypeId())
                .setAccessWithoutFeature(Rank.VIP)
                .setPoints(1500)
                .setFeature(new Feature("Chicken", "cosmetic-pet-chicken", 1500, true, CurrencyType.COINS))
                .setMenuSlot(2, 2));
        registerUnlockable(this, new PetUnlockable(FontColor.translateString("&cCow"), PetType.COW, Material.MONSTER_EGG, (byte) EntityType.COW.getTypeId())
                .setAccessWithoutFeature(Rank.VIP)
                .setPoints(1500)
                .setFeature(new Feature("Cow", "cosmetic-pet-cow", 1500, true, CurrencyType.COINS))
                .setMenuSlot(2, 3));
        registerUnlockable(this, new PetUnlockable(FontColor.translateString("&cPig"), PetType.PIG, Material.MONSTER_EGG, (byte) EntityType.PIG.getTypeId())
                .setAccessWithoutFeature(Rank.VIP)
                .setPoints(1500)
                .setFeature(new Feature("Pig", "cosmetic-pet-pig", 1500, true, CurrencyType.COINS))
                .setMenuSlot(2, 4));
        registerUnlockable(this, new PetUnlockable(FontColor.translateString("&cSheep"), PetType.SHEEP, Material.MONSTER_EGG, (byte) EntityType.SHEEP.getTypeId())
                .setAccessWithoutFeature(Rank.VIP)
                .setPoints(1500)
                .setFeature(new Feature("Sheep", "cosmetic-pet-sheep", 1500, true, CurrencyType.COINS))
                .setMenuSlot(2, 5));
        registerUnlockable(this, new PetUnlockable(FontColor.translateString("&cWolf"), PetType.WOLF, Material.MONSTER_EGG, (byte) EntityType.WOLF.getTypeId())
                .setAccessWithoutFeature(Rank.VIP)
                .setPoints(1500)
                .setFeature(new Feature("Wolf", "cosmetic-pet-wolf", 1500, true, CurrencyType.COINS))
                .setMenuSlot(2, 6));
        registerUnlockable(this, new PetUnlockable(FontColor.translateString("&cOcelot"), PetType.OCELOT, Material.MONSTER_EGG, (byte) EntityType.OCELOT.getTypeId())
                .setAccessWithoutFeature(Rank.VIP)
                .setPoints(1500)
                .setFeature(new Feature("Ocelot", "cosmetic-pet-ocelot", 1500, true, CurrencyType.COINS))
                        .setMenuSlot(2, 7));
        registerUnlockable(this, new PetUnlockable(FontColor.translateString("&cEnder Dragon"), PetType.ENDERDRAGON, Material.DRAGON_EGG)
                .setNameable(false)
                .setRequiredRank(Rank.ULTRA)
                .setMenuSlot(3, 5));

        for (Unlockable unlockable : UnlockableMenu.getUnlockables(this)) {
            if (unlockable.getMenuSlot() < 0 || unlockable.getMenuSlot() > 53) {
                continue;
            }

            this.addMenuItem(unlockable, unlockable.getMenuSlot());
        }
    }

    public MenuItem getItem() {
        return item;
    }
}

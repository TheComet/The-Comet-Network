package co.thecomet.hub.modules.preferences;

import co.thecomet.data.SessionAPI;
import co.thecomet.common.chat.FontColor;
import co.thecomet.core.module.Module;
import co.thecomet.core.module.ModuleInfo;
import co.thecomet.core.ui.MenuItem;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.data.user.INetworkUser;
import co.thecomet.data.user.UserHubData;
import co.thecomet.data.user.UserPreferences;
import co.thecomet.hub.modules.toolbar.Toolbar;
import co.thecomet.hub.modules.toolbar.ToolbarModule;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.material.MaterialData;

import java.util.*;

@ModuleInfo(name = "preferences", depends = {ToolbarModule.class})
public class PreferencesModule extends Module implements Listener {
    private Map<UUID, PreferencesMenu> preferences = new HashMap<>();
    private MenuItem toolbarItem;

    @Override
    public void onEnable() {
        addToolbarItem();
    }

    @Override
    public void onDisable() {
        Toolbar.unregister(toolbarItem);
    }

    @Override
    public List<Listener> registerListeners() {
        return new ArrayList<Listener>() {{
            add(PreferencesModule.this);
        }};
    }

    public void addToolbarItem() {
        toolbarItem = new MenuItem(FontColor.translateString("&4&lP&creferences &8"), new MaterialData(Material.REDSTONE_COMPARATOR)) {
            @Override
            public void onClick(Player player) {
                PreferencesMenu.getUserPreferences(player).openMenu(player);
            }
        };
        toolbarItem.setSlot(8);

        Toolbar.register(this, toolbarItem);
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        for (INetworkUser player : SessionAPI.getUsers()) {
            UserPreferences preferences = player.getData(UserHubData.class).preferences;
            if (!preferences.isChatEnabled()) {
                Player p = Bukkit.getPlayer(player.getUuid());
                if (event.getPlayer() == p) {
                    event.setCancelled(true);
                    MessageFormatter.sendErrorMessage(p, "You have disabled chat. Please enable chat to participate.");
                } else {
                    event.getRecipients().remove(Bukkit.getPlayer(player.getUuid()));
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent event) {
        for (INetworkUser player : SessionAPI.getUsers()) {
            UserHubData data = player.getData(UserHubData.class);

            if (data == null) {
                data = new UserHubData(player.getUuid());
                player.setData(UserHubData.class, data);
            }

            UserPreferences preferences = data.preferences;

            if (preferences == null) {
                preferences = new UserPreferences();
                data.preferences = preferences;
            }

            Player p = Bukkit.getPlayer(player.getUuid());
            if (!preferences.isPlayersVisible()) {
                if (event.getPlayer() == p) {
                    Bukkit.getOnlinePlayers().stream().filter(op -> op != p).forEach(op -> p.hidePlayer(op));
                } else {
                    p.hidePlayer(event.getPlayer());
                }
            }
        }
    }

    public static void updateHiddenPlayers(Player player) {
        INetworkUser user = SessionAPI.getUser(player.getUniqueId());
        UserPreferences preferences = user.getData(UserHubData.class).preferences;
        if (preferences.isPlayersVisible()) {
            Bukkit.getOnlinePlayers().stream().filter(op -> op != player).forEach(op -> player.showPlayer(op));
        } else {
            Bukkit.getOnlinePlayers().stream().filter(op -> op != player).forEach(op -> player.hidePlayer(op));
        }
    }
}

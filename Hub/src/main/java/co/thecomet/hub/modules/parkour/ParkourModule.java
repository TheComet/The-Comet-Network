package co.thecomet.hub.modules.parkour;

import co.thecomet.core.serialization.BukkitJsonLocation;
import co.thecomet.data.SessionAPI;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.command.CommandRegistry;
import co.thecomet.core.module.Module;
import co.thecomet.core.module.ModuleInfo;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.hub.HubModule;
import co.thecomet.data.user.UserHubData;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.material.Sign;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ModuleInfo(name = "parkour", depends = {HubModule.class})
public class ParkourModule extends Module {
    private static ParkourModule instance;
    private Map<Location, Checkpoint> checkpoints = new HashMap<>();

    public void onEnable() {
        instance = this;
        CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "cp", ParkourModule::cpCommand);
    }

    public void onDisable() {
        CommandRegistry.unregisterCommand("cp");
    }

    @Override
    public List<Listener> registerListeners() {
        return new ArrayList<Listener>() {{
            add(new CheckpointListener());
        }};
    }

    public boolean containsCheckpoints(Location location) {
        return checkpoints.containsKey(location);
    }

    public void addCheckpoint(Location location, Checkpoint checkpoint) {
        checkpoints.put(location, checkpoint);
    }

    public void removeCheckpoint(Location location) {
        checkpoints.remove(location);
    }

    public Checkpoint getCheckpoint(Location location) {
        return checkpoints.get(location);
    }

    public static ParkourModule getInstance() {
        return instance;
    }

    public static void cpCommand(Player player, String[] args) {
        UserHubData data = SessionAPI.getUser(player.getUniqueId()).getData(UserHubData.class);

        if (data == null || data.checkpoint == null || !getInstance().containsCheckpoints(new BukkitJsonLocation(data.checkpoint).getLocation())) {
            MessageFormatter.sendErrorMessage(player, "You have not set your checkpoint!");
            return;
        }

        Location location = new BukkitJsonLocation(data.checkpoint).getLocation();
        Block block = location.getBlock();

        if (block == null || (block.getState() instanceof org.bukkit.block.Sign) == false) {
            MessageFormatter.sendErrorMessage(player, "We were unable to teleport you to your checkpoint!");
            return;
        }

        Sign sign = new Sign(block.getType(), block.getData());
        Location newLocation = adjustLocation(location, sign);
        player.teleport(newLocation);
    }
    
    public static Location adjustLocation(Location location, Sign sign) {
        switch (sign.getAttachedFace()) {
            case NORTH:
                location.setYaw(180.0f);
                return location.add(0.5, 0.0, 1.5);
            case EAST:
                location.setYaw(-90.0f);
                return location.add(-0.5, 0.0, 0.5);
            case SOUTH:
                return location.add(0.5, 0.0, -0.5);
            case WEST:
                location.setYaw(90.0f);
                return location.add(1.5, 0.0, 0.5);
            default:
                break;
        }
        
        return location;
    }
}

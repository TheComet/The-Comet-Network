package co.thecomet.hub.modules.parkour;

import co.thecomet.core.serialization.BukkitJsonLocation;
import co.thecomet.data.SessionAPI;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.data.ranks.Rank;
import co.thecomet.data.user.INetworkUser;
import co.thecomet.hub.HubModule;
import co.thecomet.hub.db.HubDataAPI;
import co.thecomet.data.user.UserHubData;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class CheckpointListener implements Listener {
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getClickedBlock() == null) {
            return;
        }

        Block b = event.getClickedBlock();
        if(b.getType() == Material.WALL_SIGN || b.getType() == Material.SIGN_POST) {
            Sign sign = (Sign) b.getState();
            if (validate(sign.getLines(), event.getClickedBlock().getLocation())) {
                if (sign.getLine(0).equals("[CHECKPOINT]") == false) {
                    sign.setLine(0, "[CHECKPOINT]");
                    sign.update();
                }

                if (SessionAPI.containsUser(event.getPlayer().getUniqueId())) {
                    Checkpoint checkpoint = ParkourModule.getInstance().getCheckpoint(sign.getLocation());
                    UserHubData data = HubModule.getInstance().getPlayerHubData(event.getPlayer().getUniqueId());

                    if (data.checkpoint == null || data.checkpoint.equals(checkpoint.getSignLocation()) == false) {
                        data.checkpoint = checkpoint.getSignLocation();
                        HubDataAPI.updateCheckpoint(event.getPlayer().getUniqueId(), data);
                        MessageFormatter.sendSuccessMessage(event.getPlayer(), "You have set your check point.");
                    }
                }
            }
        }
    }

    @EventHandler
    public void onSignChange(SignChangeEvent event) {
        INetworkUser player;
        if ((player = SessionAPI.getUser(event.getPlayer().getUniqueId())) != null && player.getRank().ordinal() >= Rank.ADMIN.ordinal()) {
            validate(event.getLines(), event.getBlock().getLocation());
        } else {
            MessageFormatter.sendErrorMessage(event.getPlayer(), "You cannot create checkpoint signs.");
            event.getBlock().setType(Material.AIR);
        }
    }

    private boolean validate(String[] lines, Location location) {
        if (lines[0].equalsIgnoreCase("checkpoint") || lines[0].equalsIgnoreCase("[CHECKPOINT]")) {
            if (ParkourModule.getInstance().containsCheckpoints(location) == false) {
                ParkourModule.getInstance().addCheckpoint(location, new Checkpoint(new BukkitJsonLocation(location).getSerializedLocation()));
            }

            return true;
        }
        
        return false;
    }
}

package co.thecomet.hub.modules.preferences;

import co.thecomet.data.SessionAPI;
import co.thecomet.common.chat.FontColor;
import co.thecomet.core.ui.Menu;
import co.thecomet.core.ui.MenuItem;
import co.thecomet.data.user.INetworkUser;
import co.thecomet.hub.db.HubDataAPI;
import co.thecomet.data.user.UserHubData;
import co.thecomet.data.user.UserPreferences;
import com.google.common.collect.Maps;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

import java.util.Map;
import java.util.UUID;

public class PreferencesMenu extends Menu {
    private static Map<UUID, PreferencesMenu> users = Maps.newHashMap();
    private UserPreferences preferences;

    private MenuItem vis;
    private MenuItem chat;
    private MenuItem priv;
    private MenuItem filter;

    public PreferencesMenu(INetworkUser data) {
        super(FontColor.translateString("&4&lP&creferences &8"), 6);
        this.preferences = data.getData(UserHubData.class).preferences;
        users.put(data.getUuid(), this);
        init();
    }

    public void init() {
        DyeColor visDye = preferences.isPlayersVisible() ? DyeColor.LIME : DyeColor.GRAY;
        vis = new MenuItem(ChatColor.GOLD + PreferenceItemEnum.TOGGLE_PLAYER_VISIBILITY.getSetting(), new MaterialData(Material.INK_SACK), visDye.getDyeData()) {
            @Override
            public void onClick(Player player) {
                if (preferences.isPlayersVisible()) {
                    vis.setData(DyeColor.GRAY.getDyeData());
                    preferences.setPlayersVisible(false);
                } else {
                    vis.setData(DyeColor.LIME.getDyeData());
                    preferences.setPlayersVisible(true);
                }

                PreferencesMenu.this.updateInventory();
                PreferencesModule.updateHiddenPlayers(player);
                HubDataAPI.updatePreferences(player.getUniqueId(), preferences);
                PreferencesMenu.this.closeMenu(player);
            }
        };

        DyeColor chatDye = preferences.isChatEnabled() ? DyeColor.LIME : DyeColor.GRAY;
        chat = new MenuItem(ChatColor.GOLD + PreferenceItemEnum.TOGGLE_CHAT.getSetting(), new MaterialData(Material.INK_SACK), chatDye.getDyeData()) {
            @Override
            public void onClick(Player player) {
                if (preferences.isChatEnabled()) {
                    chat.setData(DyeColor.GRAY.getDyeData());
                    preferences.setChatEnabled(false);
                } else {
                    chat.setData(DyeColor.LIME.getDyeData());
                    preferences.setChatEnabled(true);
                }

                PreferencesMenu.this.updateInventory();
                HubDataAPI.updatePreferences(player.getUniqueId(), preferences);
                PreferencesMenu.this.closeMenu(player);
            }
        };

        DyeColor privDye = preferences.isAcceptingPrivateMessages() ? DyeColor.LIME : DyeColor.GRAY;
        priv = new MenuItem(ChatColor.GOLD + PreferenceItemEnum.TOGGLE_PRIVATE_MESSAGING.getSetting(), new MaterialData(Material.INK_SACK), privDye.getDyeData()) {
            @Override
            public void onClick(Player player) {
                if (preferences.isAcceptingPrivateMessages()) {
                    priv.setData(DyeColor.GRAY.getDyeData());
                    preferences.setAcceptingPrivateMessages(false);
                } else {
                    priv.setData(DyeColor.LIME.getDyeData());
                    preferences.setAcceptingPrivateMessages(true);
                }

                PreferencesMenu.this.updateInventory();
                HubDataAPI.updatePreferences(player.getUniqueId(), preferences);
                PreferencesMenu.this.closeMenu(player);
            }
        };

        this.addMenuItem(PreferenceItemEnum.TOGGLE_PLAYER_VISIBILITY.getMenuItem(), PreferenceItemEnum.TOGGLE_PLAYER_VISIBILITY.getIndex());
        this.addMenuItem(PreferenceItemEnum.TOGGLE_CHAT.getMenuItem(), PreferenceItemEnum.TOGGLE_CHAT.getIndex());
        this.addMenuItem(PreferenceItemEnum.TOGGLE_PRIVATE_MESSAGING.getMenuItem(), PreferenceItemEnum.TOGGLE_PRIVATE_MESSAGING.getIndex());

        this.addMenuItem(vis, PreferenceItemEnum.TOGGLE_PLAYER_VISIBILITY.getIndex() + 9);
        this.addMenuItem(chat, PreferenceItemEnum.TOGGLE_CHAT.getIndex() + 9);
        this.addMenuItem(priv, PreferenceItemEnum.TOGGLE_PRIVATE_MESSAGING.getIndex() + 9);

        updateInventory();
    }

    public static PreferencesMenu getUserPreferences(Player player) {
        PreferencesMenu menu = users.containsKey(player.getUniqueId()) ? users.get(player.getUniqueId()) : new PreferencesMenu(SessionAPI.getUser(player.getUniqueId()));
        return menu;
    }

    public static void cleanup(Player player) {
        users.remove(player.getUniqueId());
    }
}

enum PreferenceItemEnum {
    TOGGLE_PLAYER_VISIBILITY("Player Visibility", Material.EYE_OF_ENDER, 10),
    TOGGLE_CHAT("Player Chat", Material.BOOK, 13),
    TOGGLE_PRIVATE_MESSAGING("Private Messaging", Material.BOOK_AND_QUILL, 16);

    private String setting;
    private Material item;
    private int index;

    PreferenceItemEnum(String setting, Material item, int index) {
        this.setting = setting;
        this.item = item;
        this.index = index;
    }

    public String getSetting() {
        return setting;
    }

    public Material getItem() {
        return item;
    }

    public int getIndex() {
        return index;
    }

    public MenuItem getMenuItem() {
        return new MenuItem(ChatColor.GOLD + setting, new MaterialData(item)) {
            @Override
            public void onClick(Player player) {
                //
            }
        };
    }
}

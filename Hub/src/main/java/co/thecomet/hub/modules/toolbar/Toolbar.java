package co.thecomet.hub.modules.toolbar;

import co.thecomet.common.utils.Log;
import co.thecomet.data.SessionAPI;
import co.thecomet.core.module.Module;
import co.thecomet.core.ui.MenuItem;
import co.thecomet.data.user.INetworkUser;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class Toolbar implements Listener {
    private static MenuItem[] items = new MenuItem[9];
    private static Multimap<Module, MenuItem> moduleMapping = HashMultimap.create();

    public static void register(Module module, MenuItem item) {
        if (item.getSlot() > 8) {
            return;
        }

        put(module, item);
        update();
    }

    public static void unregister(MenuItem item) {
        moduleMapping.entries().forEach((entry) -> {
            if (entry.getValue().getSlot() == item.getSlot()) {
                moduleMapping.remove(entry.getKey(), entry.getValue());
            }
        });

        items[item.getSlot()] = null;
    }

    public static void put(Module module, MenuItem item) {
        if (items[item.getSlot()] != null) {
            unregister(items[item.getSlot()]);
        }

        items[item.getSlot()] = item;
        moduleMapping.put(module, item);
    }

    public static void update() {
        SessionAPI.getUsers().forEach((user) -> {
            repopulate(user.getUuid());
        });
    }

    public static void repopulate(UUID uuid) {
        Player player = Bukkit.getPlayer(uuid);
        if (player == null || player.getInventory() == null) {
            return;
        }
        
        player.getInventory().clear();
        for (int i = 0; i < items.length; i++) {
            MenuItem item = items[i];
            if (item != null) {
                player.getInventory().setItem(i, item.getItemStack());
            } else {
                player.getInventory().clear(i);
            }

            player.updateInventory();
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerJoin(PlayerJoinEvent event) {
        if (event.getPlayer().getGameMode() == GameMode.SURVIVAL || event.getPlayer().getGameMode() == GameMode.ADVENTURE) {
            Toolbar.repopulate(event.getPlayer().getUniqueId());
        } if (event.getPlayer().getGameMode() == GameMode.CREATIVE) {
            event.getPlayer().getInventory().clear();
            event.getPlayer().updateInventory();
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onInventoryClick(InventoryClickEvent event) {
        if(event.getWhoClicked().getGameMode() == GameMode.SURVIVAL || event.getWhoClicked().getGameMode() == GameMode.ADVENTURE) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerDropItem(PlayerDropItemEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        event.setCancelled(triggerMenuItem(event.getPlayer(), event.getItem()));
    }

    @EventHandler
    public void onPlayerGameModeChange(PlayerGameModeChangeEvent event) {
        if (event.getNewGameMode() == GameMode.CREATIVE) {
            event.getPlayer().getInventory().clear();
            event.getPlayer().updateInventory();
        }

        if (event.getNewGameMode() == GameMode.SURVIVAL || event.getNewGameMode() == GameMode.ADVENTURE) {
            Toolbar.repopulate(event.getPlayer().getUniqueId());
        }
    }

    public static boolean triggerMenuItem(Player player, ItemStack stack) {
        if (stack == null || stack.getData().getItemType() == Material.AIR) {
            return false;
        }

        Log.debug("Toolbar item has been triggered.");
        INetworkUser user = SessionAPI.getUser(player.getUniqueId());
        if (user == null) {
            Log.debug("We cannot trigger toolbar item because the user is null.");
            return true;
        }

        for (MenuItem item : items) {
            if (item == null) {
                Log.debug("The toolbar menu item is null.");
                continue;
            }

            if (item.getItemStack().equals(stack)) {
                Log.debug("Processing toolbar item interaction");
                item.onClick(Bukkit.getPlayer(player.getUniqueId()));
                return true;
            }
        }

        return false;
    }
}

package co.thecomet.hub.modules.cosmetic.unlockables.particles;

import co.thecomet.common.chat.FontColor;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.effects.particle.ParticleEffect;
import co.thecomet.data.ranks.Rank;
import co.thecomet.data.transaction.CurrencyType;
import co.thecomet.data.transaction.Feature;
import co.thecomet.hub.modules.cosmetic.Unlockable;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

import java.util.ArrayList;
import java.util.UUID;

public class BloodHelix extends Unlockable {
    private static final Multimap<UUID, Integer> tasks = HashMultimap.create();

    public BloodHelix() {
        super(FontColor.translateString("&c&lBlood Helix"), new MaterialData(Material.REDSTONE));
        this.setAccessWithoutFeature(Rank.VIP);
        this.points = 3000;
        this.feature = new Feature("Blood Helix", "cosmetic-blood-helix", this.points, true, CurrencyType.COINS);
        this.setMenuSlot(1, 6);
        this.setDescriptions(new ArrayList<String>(){{
            add(FontColor.translateString(String.valueOf(points) + " Comet Coins", true));
            add("");
            add(FontColor.translateString("&7There are rumors that some people"));
            add(FontColor.translateString("&7are capable of commanding their"));
            add(FontColor.translateString("&7blood of their own will."));
            add("");
            add(FontColor.translateString("&6&lUnlocked at VIP rank"));
        }});
    }

    @Override
    protected void onActivate(Player player) {
        int taskId = Bukkit.getScheduler().runTaskTimer(CoreAPI.getPlugin(), new EffectRunnable(player), 0L, 1L).getTaskId();
        tasks.put(player.getUniqueId(), taskId);
        taskId = Bukkit.getScheduler().runTaskTimer(CoreAPI.getPlugin(), new EffectRunnable(player), 25L, 1L).getTaskId();
        tasks.put(player.getUniqueId(), taskId);
    }

    @Override
    protected void onDeactivate(Player player) {
        for (int taskId : tasks.removeAll(player.getUniqueId())) {
            Bukkit.getScheduler().cancelTask(taskId);
        }
    }

    public class EffectRunnable implements Runnable {
        private final int ITERATIONS = 60;
        private final double RADIAN_ANGLE = (Math.PI * 0.75F) / (ITERATIONS / 2.0F);
        private final float START_RADIUS = 1.5F;
        private final float RADIAL_DECREMENT = START_RADIUS / ITERATIONS;
        private final float HEIGHT = 3.5F;
        private final float HEIGHT_INCREMENT = HEIGHT / ITERATIONS;
        private final UUID UUID;
        private int i = 0;

        public EffectRunnable(Player player) {
            UUID = player.getUniqueId();
        }

        @Override
        public void run() {
            if (i > ITERATIONS) {
                i = 0;
            }

            Player player = Bukkit.getPlayer(UUID);
            if (player == null) {
                return;
            }

            Location location = player.getLocation().clone().add(
                    Math.cos(RADIAN_ANGLE * i) * (START_RADIUS - (i * RADIAL_DECREMENT)),
                    i * HEIGHT_INCREMENT,
                    Math.sin(RADIAN_ANGLE * i) * (START_RADIUS - (i * RADIAL_DECREMENT)));
            Location location2 = player.getLocation().clone().add(
                    -Math.cos(RADIAN_ANGLE * i) * (START_RADIUS - (i * RADIAL_DECREMENT)),
                    i * HEIGHT_INCREMENT,
                    -Math.sin(RADIAN_ANGLE * i) * (START_RADIUS - (i * RADIAL_DECREMENT)));

            ParticleEffect.REDSTONE.display(0.02F, 0.02F, 0.02F, 0.0F, 5, location, new ArrayList<Player>(Bukkit.getOnlinePlayers()));
            ParticleEffect.REDSTONE.display(0.02F, 0.02F, 0.02F, 0.0F, 5, location2, new ArrayList<Player>(Bukkit.getOnlinePlayers()));
            i++;
        }
    }
}

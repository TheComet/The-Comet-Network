package co.thecomet.hub.modules.cosmetic.unlockables.particles;

import co.thecomet.common.chat.FontColor;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.effects.particle.ParticleEffect;
import co.thecomet.data.ranks.Rank;
import co.thecomet.data.transaction.CurrencyType;
import co.thecomet.data.transaction.Feature;
import co.thecomet.hub.modules.cosmetic.Unlockable;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class CupidsHalo extends Unlockable {
    private static final Map<UUID, Integer> tasks = new HashMap<>();

    public CupidsHalo() {
        super(FontColor.translateString("&c&lCupid's Halo"), new MaterialData(Material.RED_ROSE));
        this.setAccessWithoutFeature(Rank.VIP);
        this.points = 1500;
        this.feature = new Feature("Cupid's Halo", "cosmetic-cupids-halo", this.points, true, CurrencyType.COINS);
        this.setMenuSlot(1, 3);
        this.setDescriptions(new ArrayList<String>(){{
            add(FontColor.translateString(String.valueOf(points) + " Comet Coins", true));
            add("");
            add(FontColor.translateString("&7A crown of hearts suitable"));
            add(FontColor.translateString("&7for cupid himself."));
            add("");
            add(FontColor.translateString("&6&lUnlocked at VIP rank"));
        }});
    }

    @Override
    protected void onActivate(Player player) {
        int taskId = Bukkit.getScheduler().runTaskTimer(CoreAPI.getPlugin(), new EffectRunnable(player), 1L, 1L).getTaskId();

        tasks.put(player.getUniqueId(), taskId);
    }

    @Override
    protected void onDeactivate(Player player) {
        Bukkit.getScheduler().cancelTask(tasks.remove(player.getUniqueId()));
    }

    public class EffectRunnable implements Runnable {
        private final float RADIUS = 0.5F;
        private final double RADIAN_ANGLE = (2.0 * Math.PI) / 20.0;
        private final UUID UUID;
        private int i = 0;

        public EffectRunnable(Player player) {
            UUID = player.getUniqueId();
        }

        @Override
        public void run() {
            if (i > 40) {
                i = 0;
            }

            Player player = Bukkit.getPlayer(UUID);
            if (player == null) {
                return;
            }

            Location location = player.getLocation().clone().add(
                    Math.cos(RADIAN_ANGLE * ((float) i) * RADIUS),
                    2.0,
                    Math.sin(RADIAN_ANGLE * ((float) i) * RADIUS));

            ParticleEffect.HEART.display(0.0F, 0.0F, 0.0F, 1.0F, 1, location, new ArrayList<Player>(Bukkit.getOnlinePlayers()));
            i++;
        }
    }
}

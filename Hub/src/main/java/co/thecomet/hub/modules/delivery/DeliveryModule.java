package co.thecomet.hub.modules.delivery;

import co.thecomet.core.CoreAPI;
import co.thecomet.core.command.CommandRegistry;
import co.thecomet.common.serialization.JsonLocation;
import co.thecomet.core.module.Module;
import co.thecomet.core.module.ModuleInfo;
import co.thecomet.core.serialization.BukkitJsonLocation;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.data.DataAPI;
import co.thecomet.data.delivery.UserDeliveryStats;
import co.thecomet.data.ranks.Rank;
import co.thecomet.hub.HubModule;
import co.thecomet.hub.db.HubDataAPI;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Lists;
import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@ModuleInfo(name = "delivery")
public class DeliveryModule extends Module implements Listener {
    @Getter
    private static Cache<UUID, Long> cooldown = CacheBuilder.newBuilder().expireAfterWrite(5, TimeUnit.SECONDS).build();

    public void onEnable() {
        CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "adddeliverybox", Rank.ADMIN, DeliveryModule::addDeliveryChest);
    }

    @Override
    public List<Listener> registerListeners() {
        ArrayList<Listener> listeners = Lists.newArrayList();
        listeners.add(this);
        return listeners;
    }

    public static void openDeliveryMenu(Player player) {
        CoreAPI.async(() -> {
            UserDeliveryStats stats = DataAPI.getDeliveryStats(player.getUniqueId());

            if (player != null && player.isOnline()) {
                DeliveryMenu menu = new DeliveryMenu(stats);
                menu.openMenu(player);
            }
        });
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getClickedBlock() == null) {
            return;
        }

        if (event.getClickedBlock().getType() == Material.ENDER_CHEST) {
            event.setCancelled(true);

            if (cooldown.asMap().containsKey(event.getPlayer().getUniqueId())) {
                MessageFormatter.sendInfoMessage(event.getPlayer(), "Please try again in a few seconds.");
                return;
            }

            cooldown.put(event.getPlayer().getUniqueId(), System.currentTimeMillis());
            openDeliveryMenu(event.getPlayer());
        }
    }

    public static void addDeliveryChest(Player player, String[] args) {
        Block block = player.getTargetBlock((Set) null, 10);
        if (block != null) {
            if (block.getType() == Material.ENDER_CHEST) {
                JsonLocation location = new BukkitJsonLocation(block.getLocation()).getSerializedLocation();
                MessageFormatter.sendSuccessMessage(player, "The ender chest you are looking at is now a delivery box!");
                CoreAPI.async(() -> HubDataAPI.addDeliveryBox(location));
                HubModule.getHubData().deliveryBoxes.add(location);
            } else {
                MessageFormatter.sendErrorMessage(player, "You must be looking at an ender chest!");
            }
        } else {
            MessageFormatter.sendErrorMessage(player, "You are not looking at a block!");
        }
    }
}

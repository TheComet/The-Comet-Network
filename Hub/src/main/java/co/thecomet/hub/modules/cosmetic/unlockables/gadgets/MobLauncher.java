package co.thecomet.hub.modules.cosmetic.unlockables.gadgets;

import co.thecomet.common.chat.FontColor;
import co.thecomet.common.utils.RandomUtil;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.effects.particle.ParticleEffect;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.core.utils.items.ItemBuilder;
import co.thecomet.data.ranks.Rank;
import co.thecomet.data.transaction.CurrencyType;
import co.thecomet.data.transaction.Feature;
import co.thecomet.hub.modules.cosmetic.Unlockable;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.bukkit.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.material.MaterialData;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class MobLauncher extends Unlockable implements Listener {
    private static final int COOLDOWN_TIME = 10;
    private static final Cache<UUID, Long> COOLDOWN = CacheBuilder.newBuilder().expireAfterWrite(COOLDOWN_TIME, TimeUnit.SECONDS).build();
    private static final List<EntityType> allowedTypes = new ArrayList<>();
    private static final String ITEM_PREFIX = FontColor.translateString("&cMob Launcher &7- &r");
    private static final ItemStack TEMPLATE = ItemBuilder.build(Material.BLAZE_ROD).name(ITEM_PREFIX).build();

    static {
        allowedTypes.add(EntityType.SHEEP);
        allowedTypes.add(EntityType.COW);
        allowedTypes.add(EntityType.HORSE);
        allowedTypes.add(EntityType.CHICKEN);
        allowedTypes.add(EntityType.RABBIT);
        allowedTypes.add(EntityType.WOLF);
        allowedTypes.add(EntityType.PIG);
        allowedTypes.add(EntityType.OCELOT);
        allowedTypes.add(EntityType.IRON_GOLEM);
        allowedTypes.add(EntityType.SLIME);
        allowedTypes.add(EntityType.MAGMA_CUBE);
        allowedTypes.add(EntityType.VILLAGER);
        allowedTypes.add(EntityType.WITCH);
        allowedTypes.add(EntityType.CREEPER);
        allowedTypes.add(EntityType.ZOMBIE);
        allowedTypes.add(EntityType.SKELETON);
        allowedTypes.add(EntityType.PIG_ZOMBIE);
        allowedTypes.add(EntityType.SPIDER);
        allowedTypes.add(EntityType.MUSHROOM_COW);
        allowedTypes.add(EntityType.SQUID);
        allowedTypes.add(EntityType.CAVE_SPIDER);
        allowedTypes.add(EntityType.BLAZE);
        allowedTypes.add(EntityType.ENDERMITE);
        allowedTypes.add(EntityType.ENDERMAN);
        allowedTypes.add(EntityType.SILVERFISH);
    }

    public MobLauncher() {
        super(FontColor.translateString("&c&lMob Launcher"), new MaterialData(Material.BLAZE_ROD));
        this.setAccessWithoutFeature(Rank.ULTRA);
        this.setPoints(4000);
        this.setFeature(new Feature("Mob Launcher", "cosmetic-mob-launcher", this.points, true, CurrencyType.COINS));
        this.setMenuSlot(1, 4);
        this.setDescriptions(new ArrayList<String>(){{
            add(FontColor.translateString(String.valueOf(points) + " Comet Coins", true));
            add("");
            add(FontColor.translateString("&7Launch mobs into the sky until"));
            add(FontColor.translateString("&7they explode with sparkly goodness!"));
            add("");
            add(FontColor.translateString("&6&lUnlocked at Ultra rank"));
        }});
    }

    @Override
    protected void onActivate(Player player) {
        EntityType type = allowedTypes.get(RandomUtil.getBetween(0, allowedTypes.size()));
        setItem(player, type);
    }

    @Override
    protected void onDeactivate(Player player) {
        player.getInventory().setItem(5, null);
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (event.getItem() == null || event.getItem().getItemMeta() == null || event.getItem().getItemMeta().getDisplayName() == null) {
            return;
        }

        if (!event.getItem().getItemMeta().getDisplayName().startsWith(ITEM_PREFIX)) {
            return;
        }

        event.setCancelled(true);
        if (event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK) {
            if (COOLDOWN.asMap().containsKey(event.getPlayer().getUniqueId())) {
                MessageFormatter.sendInfoMessage(event.getPlayer(), "You can use the Mob Launcher in " + ((COOLDOWN.asMap().get(event.getPlayer().getUniqueId()) + (COOLDOWN_TIME * 1000) - System.currentTimeMillis()) / 1000) + " seconds!");
                return;
            }
        }

        if (run(event.getPlayer(), event.getAction())) {
            COOLDOWN.put(event.getPlayer().getUniqueId(), System.currentTimeMillis());
        }
    }

    public boolean run(Player player, Action action) {
        EntityType type = getTypeFromGadget(player);
        if (action == Action.LEFT_CLICK_AIR || action == Action.LEFT_CLICK_BLOCK) {
            final Vector dir = player.getLocation().getDirection().multiply(0.3F);
            final Entity entity = player.getWorld().spawnEntity(player.getLocation(), type);
            entity.setVelocity(dir);

            final int task = Bukkit.getScheduler().runTaskTimer(CoreAPI.getPlugin(), () -> {
                entity.setVelocity(dir);
                ParticleEffect.REDSTONE.display(0.0F, 0.0F, 0.0F, 10.0F, 10, entity.getLocation(), 15.0D);
            }, 2L, 2L).getTaskId();

            Bukkit.getScheduler().runTaskLater(CoreAPI.getPlugin(), () -> {
                Bukkit.getScheduler().cancelTask(task);
                launchFirework(entity.getLocation());
                entity.remove();
            }, 60L);
            return true;
        }

        if (allowedTypes.indexOf(type) == allowedTypes.size() - 1) {
            type = allowedTypes.get(0);
        } else {
            type = allowedTypes.get(allowedTypes.indexOf(type) + 1);
        }

        setItem(player, type);
        return false;
    }

    public static ItemStack buildItem(EntityType type) {
        return new ItemBuilder(TEMPLATE.clone()).appendNameEnd(type.name()).build();
    }

    public static void setItem(Player player, EntityType type) {
        player.getInventory().setItem(5, buildItem(type));
    }

    public static EntityType getTypeFromGadget(Player player) {
        ItemStack is = player.getInventory().getItem(5);
        if (is !=  null && is.hasItemMeta() && is.getItemMeta().hasDisplayName() && is.getItemMeta().getDisplayName().startsWith(ITEM_PREFIX)) {
            String name = is.getItemMeta().getDisplayName().replace(ITEM_PREFIX, "");
            for (EntityType type : EntityType.values()) {
                if (type.name().equalsIgnoreCase(name)) {
                    return type;
                }
            }
        }

        return null;
    }

    public static void launchFirework(Location location) {
        FireworkEffect.Builder builder = FireworkEffect.builder();

        if (RandomUtil.RANDOM.nextBoolean()) {
            builder.withTrail();
        }

        if (RandomUtil.RANDOM.nextBoolean()) {
            builder.withFlicker();
        }

        builder.with(FireworkEffect.Type.values()[RandomUtil.getBetween(0, FireworkEffect.Type.values().length)]);

        int count = 20;
        do {
            builder.withColor(Color.fromRGB(RandomUtil.RANDOM.nextInt(255), RandomUtil.RANDOM.nextInt(255), RandomUtil.RANDOM.nextInt(255)));
            count--;
        } while (count >= 0);

        Firework firework = location.getWorld().spawn(location, Firework.class);
        FireworkMeta data = firework.getFireworkMeta();
        data.addEffects(new FireworkEffect[]{builder.build()});
        data.setPower(RandomUtil.RANDOM.nextInt(3));
        firework.setFireworkMeta(data);
        Bukkit.getScheduler().runTaskLater(CoreAPI.getPlugin(), () -> firework.detonate(), 1L);
    }
}

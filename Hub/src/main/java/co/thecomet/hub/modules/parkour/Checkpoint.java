package co.thecomet.hub.modules.parkour;

import co.thecomet.common.serialization.JsonLocation;

public class Checkpoint {
    private JsonLocation signLocation;

    public Checkpoint(JsonLocation signLocation) {
        this.signLocation = signLocation;
    }

    public JsonLocation getSignLocation() {
        return signLocation;
    }
}

package co.thecomet.hub.modules.cosmetic.unlockables.particles;

import co.thecomet.common.chat.FontColor;
import co.thecomet.common.utils.RandomUtil;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.effects.particle.ParticleEffect;
import co.thecomet.data.ranks.Rank;
import co.thecomet.data.transaction.CurrencyType;
import co.thecomet.data.transaction.Feature;
import co.thecomet.hub.modules.cosmetic.Unlockable;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class RainCloud extends Unlockable {
    private static final Map<UUID, Integer> tasks = new HashMap<>();

    public RainCloud() {
        super(FontColor.translateString("&c&lRain Cloud"), new MaterialData(Material.INK_SACK, (byte) 4));
        this.setAccessWithoutFeature(Rank.VIP);
        this.points = 1500;
        this.feature = new Feature("Rain Cloud", "cosmetic-rain-cloud", this.points, true, CurrencyType.COINS);
        this.setMenuSlot(1, 5);
        this.setDescriptions(new ArrayList<String>(){{
            add(FontColor.translateString(String.valueOf(points) + " Comet Coins", true));
            add("");
            add(FontColor.translateString("&7It's raining, it's pouring..."));
            add("");
            add(FontColor.translateString("&6&lUnlocked at VIP rank"));
        }});
    }

    @Override
    protected void onActivate(Player player) {
        int taskId = Bukkit.getScheduler().runTaskTimer(CoreAPI.getPlugin(), new EffectRunnable(player), 1L, 1L).getTaskId();

        tasks.put(player.getUniqueId(), taskId);
    }

    @Override
    protected void onDeactivate(Player player) {
        Bukkit.getScheduler().cancelTask(tasks.remove(player.getUniqueId()));
    }

    public class EffectRunnable implements Runnable {
        private final UUID UUID;
        private int i = 0;

        public EffectRunnable(Player player) {
            UUID = player.getUniqueId();
        }

        @Override
        public void run() {
            if (i > 40) {
                i = 0;
            }

            Player player = Bukkit.getPlayer(UUID);
            if (player == null) {
                return;
            }

            float xRand = RandomUtil.RANDOM.nextBoolean() ? RandomUtil.RANDOM.nextFloat() : -RandomUtil.RANDOM.nextFloat();
            float yRand = RandomUtil.RANDOM.nextBoolean() ? RandomUtil.RANDOM.nextFloat() : -RandomUtil.RANDOM.nextFloat();
            float zRand = RandomUtil.RANDOM.nextBoolean() ? RandomUtil.RANDOM.nextFloat() : -RandomUtil.RANDOM.nextFloat();
            Location location = player.getLocation().clone().add(xRand, 3.0F + (yRand * 0.2F), zRand);

            ParticleEffect.CLOUD.display(0.15F, 0.05F, 0.15F, 0.0F, 5, location, new ArrayList<Player>(Bukkit.getOnlinePlayers()));
            if (i % 3 == 0) {
                location = player.getLocation().clone().add(xRand * 0.75F, 3.0F + (yRand * 0.1F), zRand * 0.75F);
                ParticleEffect.DRIP_WATER.display(0.0F, 0.0F, 0.0F, 1.0F, 1, location, new ArrayList<Player>(Bukkit.getOnlinePlayers()));
            }
            i++;
        }
    }
}

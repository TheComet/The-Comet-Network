package co.thecomet.hub.modules.delivery;

import co.thecomet.data.SessionAPI;
import co.thecomet.common.chat.FontColor;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.ui.Menu;
import co.thecomet.core.ui.MenuItem;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.data.DataAPI;
import co.thecomet.data.delivery.DeliveryResetType;
import co.thecomet.data.delivery.DeliveryType;
import co.thecomet.data.delivery.UserDeliveryStats;
import co.thecomet.data.user.INetworkUser;
import com.google.common.collect.Lists;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Months;

import java.util.Calendar;
import java.util.Date;

public class DeliveryMenu extends Menu {
    private UserDeliveryStats stats;

    public DeliveryMenu(UserDeliveryStats stats) {
        super(FontColor.DARK_RED + "D" + FontColor.RED + "elivery", 2);
        this.stats = stats;

        init();
    }

    private void init() {
        for (DeliveryType type : DeliveryType.values()) {
            createDeliveryItem(type, stats.getStats().get(type));
        }
    }

    private void createDeliveryItem(DeliveryType type, Date date) {
        MenuItem item;
        if (date == null) {
            item = new MenuItem(FontColor.translateString(type.getDeliveryDisplay()), new MaterialData(Material.STORAGE_MINECART), 1) {
                @Override
                public void onClick(Player player) {
                    final INetworkUser user = SessionAPI.getUser(player.getUniqueId());

                    if (user == null) {
                        return;
                    } else if (user.getRank().ordinal() < type.getRequiredRank().ordinal()) {
                        MessageFormatter.sendInfoMessage(player, "You must have " + type.getRequiredRank().getDisplayName() + " to claim this delivery!");
                        return;
                    }

                    if (type.getCoinReward() > 0) {
                        DataAPI.addCoins(type.getCoinReward(), player.getUniqueId(), false);
                    }

                    if (type.getPointReward() > 0) {
                        DataAPI.addPoints(type.getPointReward(), player.getUniqueId(), false);
                    }

                    if (type.isShouldUpdateStats()) {
                        DeliveryModule.getCooldown().put(player.getUniqueId(), System.currentTimeMillis());
                        CoreAPI.async(() -> DataAPI.updateUserDeliveries(player.getUniqueId(), type, new Date(System.currentTimeMillis())));
                    }

                    player.sendMessage(FontColor.translateString(type.getUnclaimedMessage()));
                    closeMenu(player);
                }
            };
            item.setDescriptions(Lists.newArrayList(FontColor.translateString(type.getUnclaimedLore()).split("\n")));
        } else {
            if (type.getResetType() != DeliveryResetType.SINGLE_USE) {
                if (type.getResetType() == DeliveryResetType.ONE_DAY_FROM_CLAIMING) {
                    Days days = Days.daysBetween(new DateTime(date), new DateTime(System.currentTimeMillis()));
                    if (days.getDays() > 0) {
                        stats.getStats().remove(type);
                        createDeliveryItem(type, null);
                        return;
                    }
                } else if (type.getResetType() == DeliveryResetType.ONE_MONTH_FROM_CLAIMING) {
                    Months months = Months.monthsBetween(new DateTime(date), new DateTime(new Date(System.currentTimeMillis())));
                    if (months.getMonths() > 0) {
                        stats.getStats().remove(type);
                        createDeliveryItem(type, null);
                        return;
                    }
                } else if (type.getResetType() == DeliveryResetType.FIRST_OF_MONTH) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);

                    int yearClaimed = calendar.get(Calendar.YEAR);
                    int monthClaimed = calendar.get(Calendar.MONTH);

                    calendar.setTime(new Date(System.currentTimeMillis()));

                    if (calendar.get(Calendar.YEAR) > yearClaimed || (calendar.get(Calendar.YEAR) == yearClaimed && calendar.get(Calendar.MONTH) > monthClaimed)) {
                        stats.getStats().remove(type);
                        createDeliveryItem(type, null);
                        return;
                    }
                }
            }

            item = new MenuItem(FontColor.translateString(type.getDeliveryDisplay()), new MaterialData(Material.MINECART), 1) {
                @Override
                public void onClick(Player player) {
                    player.sendMessage(FontColor.translateString(type.getClaimedMessage()));
                    closeMenu(player);
                }
            };
            item.setDescriptions(Lists.newArrayList(FontColor.translateString(type.getClaimedMessage()).split("\n")));
        }

        item.setSlot(type.getRow() * 9 + type.getColumn() - 10);
        this.addMenuItem(item, item.getSlot());
        this.updateMenu();
        this.updateInventory();
    }
}

package co.thecomet.hub.modules.hublist;

import co.thecomet.common.chat.FontColor;
import co.thecomet.core.module.Module;
import co.thecomet.core.module.ModuleInfo;
import co.thecomet.core.ui.MenuItem;
import co.thecomet.hub.modules.toolbar.ToolbarModule;
import co.thecomet.hub.modules.toolbar.Toolbar;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

@ModuleInfo(name = "hublist", depends = {ToolbarModule.class})
public class HubListModule extends Module {
    private HubListMenu menu;
    private MenuItem toolbarItem;

    @Override
    public void onEnable() {
        menu = new HubListMenu();
        addToolbarItem();
    }

    @Override
    public void onDisable() {
        Toolbar.unregister(toolbarItem);
        menu.cancel();
    }

    public void addToolbarItem() {
        toolbarItem = new MenuItem(FontColor.translateString("&4&lL&cobby &4&lM&cenu"), new MaterialData(Material.WATCH)) {
            @Override
            public void onClick(Player player) {
                if (menu.getInventory().getViewers().contains(player) == false) {
                    menu.openMenu(player);
                }
            }
        };
        toolbarItem.setSlot(1);

        Toolbar.register(this, toolbarItem);
    }
}

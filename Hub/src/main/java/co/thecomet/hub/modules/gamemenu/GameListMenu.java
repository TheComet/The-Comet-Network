package co.thecomet.hub.modules.gamemenu;

import co.thecomet.data.SessionAPI;
import co.thecomet.common.chat.FontColor;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.ui.Menu;
import co.thecomet.core.ui.MenuItem;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.core.utils.ProxyUtil;
import co.thecomet.data.ranks.Rank;
import co.thecomet.data.user.INetworkUser;
import co.thecomet.hub.Hub;
import co.thecomet.hub.status.ServerInfo;
import co.thecomet.hub.status.ServerType;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GameListMenu extends Menu implements Runnable {
    private ServerType type;
    private boolean cancelled = false;
    private int id;

    public GameListMenu(ServerType type) {
        super(ChatColor.DARK_AQUA + FontColor.translateString(type.getInventoryDisplay()), 6);
        this.type = type;

        id = Bukkit.getScheduler().runTaskTimer(CoreAPI.getPlugin(), this, 20, 20).getTaskId();
    }

    @Override
    public void run() {
        if (cancelled) {
            return;
        }
        
        update();
    }

    private void update() {
        List<ServerInfo> infos = retrieveGames();
        Collections.sort(infos, (ServerInfo i1, ServerInfo i2) -> i2.getPlayersOnline() - i1.getPlayersOnline());
        process(infos);
        reload();
    }

    private List<ServerInfo> retrieveGames() {
        return Hub.getInstance().getServerInfoListener().get(type);
    }

    public void process(List<ServerInfo> infos) {
        int indexFloor = 0;
        int indexCeiling = 53;

        for (int i = 0; i < items.length; i++) {
            removeMenuItem(i);
        }
        
        for (ServerInfo info : infos) {
            if (info == null) {
                continue;
            }

            MenuItem item = create(info);
            item.setQuantity(info.getPlayersOnline());
            item.setDescriptions(new ArrayList<String>() {{
                add(FontColor.BOLD.toString() + FontColor.ORANGE + "Players: " + FontColor.RED + info.getPlayersOnline() + org.bukkit.ChatColor.GOLD + " / " + org.bukkit.ChatColor.RED + info.getMaxPlayers());
            }});
            if (indexFloor == indexCeiling) {
                return;
            }

            switch (item.getIcon().getData()) {
                case 0:
                    put(item, indexFloor);
                    indexFloor += 1;
                    break;
                case 14:
                    put(item, indexCeiling);
                    indexCeiling -= 1;
                    break;
                default:
                    break;
            }
        }
    }

    public MenuItem create(ServerInfo info) {
        return new MenuItem(FontColor.translateString(getDisplay(info)), new MaterialData(Material.STAINED_CLAY, getData(info))) {
            @Override
            public void onClick(Player player) {
                if (info.getPlayersOnline() < info.getMaxPlayers()) {
                    if (info.isJoinable()) {
                        ProxyUtil.send(player, info.getId());
                        return;
                    } else {
                        if (info.isSpectaorJoinInProgress()) {
                            ProxyUtil.send(player, info.getId());
                            return;
                        }
                    }
                } else {
                    INetworkUser user = SessionAPI.getUser(player.getUniqueId());
                    if (user != null) {
                        if (user.getRank() != Rank.DEFAULT && info.isJoinable()) {
                            ProxyUtil.send(player, info.getId());
                            return;
                        }
                    }
                }

                MessageFormatter.sendErrorMessage(player, "You cannot join this server at this time.");
            }
        };
        
    }

    public byte getData(ServerInfo info) {
        if (info.getStatus().equalsIgnoreCase("PLAYING") == false) {
            return DyeColor.WHITE.getData();
        }

        return DyeColor.RED.getData();
    }

    public String getDisplay(ServerInfo info) {
        return type.getDisplay() + " " + info.getId().toLowerCase().replace(type.name().toLowerCase(), "");
    }

    public void put(MenuItem item, int index) {
        this.addMenuItem(item, index);
    }

    public void reload() {
        updateMenu();
        updateInventory();
    }

    public void cancel() {
        this.cancelled = true;
        Bukkit.getScheduler().cancelTask(id);
    }
}

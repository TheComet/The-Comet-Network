package co.thecomet.hub.modules.cosmetic.unlockables.gadgets;

import co.thecomet.data.SessionAPI;
import co.thecomet.common.chat.FontColor;
import co.thecomet.common.utils.RandomUtil;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.core.utils.items.ItemBuilder;
import co.thecomet.data.ranks.Rank;
import co.thecomet.data.transaction.CurrencyType;
import co.thecomet.data.transaction.Feature;
import co.thecomet.data.user.INetworkUser;
import co.thecomet.hub.modules.cosmetic.Unlockable;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Lists;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class Rainbow extends Unlockable implements Listener {
    private static final int COOLDOWN_TIME = 90;
    private static final int WAIT_TIME = 20 * 20;
    private static final Cache<UUID, Long> COOLDOWN = CacheBuilder.newBuilder().expireAfterWrite(COOLDOWN_TIME, TimeUnit.SECONDS).build();
    private static final String ITEM_PREFIX = FontColor.translateString("&cRainbow");
    private static List<UUID> active = Lists.newArrayList();

    private static Map<Integer, Short> rainbowWool = new HashMap<>();
    private static final float HEIGHT = 5.0f;
    private static final float RADIUS = 8.0f;
    private static final float MODIFIER = (float) (HEIGHT / Math.pow(RADIUS, 2));
    private static final float SPACE = 0.25f;
    private static final int POINTS = 30;
    private static final int LEFT_RIGHT = 0;

    static {
        rainbowWool.put(0, (short) 2);
        rainbowWool.put(1, (short) 11);
        rainbowWool.put(2, (short) 3);
        rainbowWool.put(3, (short) 5);
        rainbowWool.put(4, (short) 4);
        rainbowWool.put(5, (short) 1);
        rainbowWool.put(6, (short) 14);
    }

    public Rainbow() {
        super(FontColor.translateString("&c&lRainbow"), new MaterialData(Material.RAW_FISH), (short) 2);
        this.setAccessWithoutFeature(Rank.YOUTUBER);
        this.setPoints(15000);
        this.setFeature(new Feature("Rainbow", "cosmetic-rainbow", this.points, true, CurrencyType.COINS));
        this.setMenuSlot(1, 3);
        this.setDescriptions(new ArrayList<String>() {{
            add(FontColor.translateString(String.valueOf(points) + " Comet Coins", true));
            add("");
            add(FontColor.translateString("&7A beautiful rainbow appears in the sky!"));
        }});
    }

    @Override
    protected void onActivate(Player player) {
        player.getInventory().setItem(5, ItemBuilder.build(Material.RAW_FISH).durability(2).name(ITEM_PREFIX).build());
    }

    @Override
    protected void onDeactivate(Player player) {
        player.getInventory().setItem(5, null);
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (event.getItem() == null || event.getItem().getItemMeta() == null || event.getItem().getItemMeta().getDisplayName() == null) {
            return;
        }

        if (!event.getItem().getItemMeta().getDisplayName().startsWith(ITEM_PREFIX)) {
            return;
        }

        INetworkUser user = SessionAPI.getUser(event.getPlayer().getUniqueId());
        event.setCancelled(true);
        if (user.getRank().ordinal() < Rank.ADMIN.ordinal() && event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK) {
            if (COOLDOWN.asMap().containsKey(event.getPlayer().getUniqueId())) {
                MessageFormatter.sendInfoMessage(event.getPlayer(), "You can use the Rainbow in " + ((COOLDOWN.asMap().get(event.getPlayer().getUniqueId()) + (COOLDOWN_TIME * 1000) - System.currentTimeMillis()) / 1000) + " seconds!");
                return;
            }
        }

        if (active.contains(user.getUuid())) {
            MessageFormatter.sendErrorMessage(event.getPlayer(), "You already have an activated rainbow.");
            return;
        }

        if (active.size() >= 2) {
            MessageFormatter.sendErrorMessage(event.getPlayer(), "Only two rainbows can be active on a hub simultaneously.");
            return;
        }

        if (canRun(event.getPlayer(), event.getAction())) {
            final RainbowWrapper wrapper = new RainbowWrapper();
            run(event.getPlayer(), wrapper);
            COOLDOWN.put(event.getPlayer().getUniqueId(), System.currentTimeMillis());
        }
    }

    public void run(Player player, RainbowWrapper wrapper) {
        final Location location = player.getLocation();
        for (int i = -POINTS; i <= POINTS; i++) {
            final int iteration = i;
            Bukkit.getScheduler().runTaskLater(CoreAPI.getPlugin(), () -> {
                List<UUID> group = Lists.newArrayList();
                for (int j = 0; j < rainbowWool.size(); j++) {
                    float addY = (float) (-Math.pow(MODIFIER * iteration, 2.0) + HEIGHT + j * SPACE);
                    Location base = getSpawnLocation(location, (RADIUS / POINTS) * iteration, LEFT_RIGHT, addY);
                    ArmorStand stand = (ArmorStand) player.getWorld().spawnEntity(base, EntityType.ARMOR_STAND);
                    stand.setGravity(false);
                    stand.setVisible(false);
                    ItemStack is = new ItemStack(Material.WOOL, 1, rainbowWool.get(j));
                    ItemMeta meta = is.getItemMeta();
                    meta.setDisplayName("rainbow-" + player.getName() + "-" + iteration + "-" + j);
                    is.setItemMeta(meta);
                    Item item = player.getWorld().dropItem(stand.getLocation(), is);
                    stand.setPassenger(item);
                    group.add(stand.getUniqueId());
                    group.add(item.getUniqueId());
                }
                wrapper.groups.add(group);
            }, iteration);
        }

        active.add(player.getUniqueId());
        new BukkitRunnable() {
            @Override
            public void run() {
                if (wrapper.groups.size() == 0) {
                    active.remove(player.getUniqueId());
                    this.cancel();
                } else {
                    List<UUID> group = wrapper.groups.get(RandomUtil.getBetween(0, wrapper.getGroups().size()));
                    for (UUID uuid : group) {
                        Bukkit.getWorlds().get(0).getEntitiesByClasses(Item.class, ArmorStand.class).stream()
                                .filter(entity -> entity.getUniqueId().equals(uuid))
                                .forEach(org.bukkit.entity.Entity::remove);
                    }
                    wrapper.groups.remove(group);
                }
            }
        }.runTaskTimer(CoreAPI.getPlugin(), WAIT_TIME, 3);
    }

    public Location getSpawnLocation(Location location, double amt, int direction, float addY) {
        final float newX = (float)(location.getX() + ( amt * Math.cos(Math.toRadians(location.getYaw() + 90 * direction))));
        final float newZ = (float)(location.getZ() + ( amt * Math.sin(Math.toRadians(location.getYaw() + 90 * direction))));

        Location loc = location.clone();
        loc.setX(newX);
        loc.setZ(newZ);
        return loc.add(0.0, addY, 0.0);
    }

    public boolean canRun(Player player, Action action) {
        if (action == Action.LEFT_CLICK_AIR || action == Action.LEFT_CLICK_BLOCK) {
            return true;
        }

        return false;
    }

    @EventHandler
    public void onPlayerPickupItem(PlayerPickupItemEvent event) {
        if (event.getItem() == null) {
            return;
        }

        if (event.getItem().getItemStack() == null) {
            return;
        }

        if (event.getItem().getItemStack().getItemMeta() == null) {
            return;
        }

        ItemMeta meta = event.getItem().getItemStack().getItemMeta();
        if (meta.getDisplayName() == null || meta.getDisplayName().isEmpty()) {
            return;
        } else if (meta.getDisplayName().startsWith("rainbow")) {
            event.setCancelled(true);
        }
    }

    public static class RainbowWrapper {
        @Getter
        private List<List<UUID>> groups = Lists.newArrayList();
        @Getter
        private long created = System.currentTimeMillis();
    }
}

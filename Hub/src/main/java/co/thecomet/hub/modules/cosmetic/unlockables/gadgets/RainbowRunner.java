package co.thecomet.hub.modules.cosmetic.unlockables.gadgets;

import co.thecomet.common.chat.FontColor;
import co.thecomet.common.utils.RandomUtil;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.effects.particle.ParticleEffect;
import co.thecomet.core.utils.blocks.Regeneration;
import co.thecomet.data.ranks.Rank;
import co.thecomet.hub.modules.cosmetic.Unlockable;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.material.MaterialData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;

public class RainbowRunner extends Unlockable implements Listener {
    private Regeneration regeneration = new Regeneration(CoreAPI.getPlugin());
    
    public RainbowRunner() {
        super(FontColor.translateString("&c&lRainbow Runner"), new MaterialData(Material.IRON_BOOTS));
        this.setRequiredRank(Rank.VIP);
        this.setMenuSlot(1, 6);
        this.setDescriptions(new ArrayList<String>(){{
            add("");
            add(FontColor.translateString("&7Run around in style. The rainbow way!"));
            add("");
            add(FontColor.translateString("&6&lUnlocked at VIP rank"));
        }});
    }

    @Override
    protected void onActivate(Player player) {
        player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 2));
    }

    @Override
    protected void onDeactivate(Player player) {
        player.removePotionEffect(PotionEffectType.SPEED);
    }
    
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerMove(PlayerMoveEvent event) {
        if (isActive(event.getPlayer()) && activeUnlockable.get(event.getPlayer().getUniqueId()) == this) {
            Block block = event.getFrom().subtract(0, 1, 0).getBlock();

            switch (block.getType()) {
                case AIR:
                case SIGN:
                case SIGN_POST:
                case WALL_SIGN:
                case VINE:
                case WATER:
                case STATIONARY_WATER:
                case LAVA:
                case STATIONARY_LAVA:
                case TORCH:
                case GRASS:
                case WOOL:
                case STAINED_CLAY:
                    return;
                default:
                    break;
            }
            
            if (regeneration.request(block, 20 * 2)) {
                DyeColor color = DyeColor.values()[RandomUtil.RANDOM.nextInt(DyeColor.values().length)];
                block.setType(Material.STAINED_CLAY);
                block.setData(color.getData(), false);
            }

            particle(event.getPlayer());
        }
    }
    
    public void particle(Player player) {
        ParticleEffect.CRIT_MAGIC.display(0.4f, 0.4f, 0.4f, 5.0f, 10, player.getLocation(), Bukkit.getOnlinePlayers().toArray(new Player[Bukkit.getOnlinePlayers().size()]));
    }
}

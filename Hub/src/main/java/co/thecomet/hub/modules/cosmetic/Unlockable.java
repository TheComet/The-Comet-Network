package co.thecomet.hub.modules.cosmetic;

import co.thecomet.data.SessionAPI;
import co.thecomet.common.chat.FontColor;
import co.thecomet.core.transaction.TransactionManager;
import co.thecomet.core.ui.MenuItem;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.data.ranks.Rank;
import co.thecomet.data.transaction.Feature;
import co.thecomet.data.transaction.Transaction;
import co.thecomet.data.user.INetworkUser;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

import java.util.*;

public abstract class Unlockable extends MenuItem {
    protected static final Map<UUID, Unlockable> activeUnlockable = new HashMap<>();
    protected Rank requiredRank = Rank.DEFAULT;
    protected int points = 0;
    protected Feature feature = null;
    protected Rank accessWithoutFeature = Rank.FRIEND;
    protected int menuSlot = -1;
    
    public Unlockable(String text) {
        super(text);
    }

    public Unlockable(String text, MaterialData icon) {
        super(text, icon);
    }

    public Unlockable(String text, MaterialData icon, int quantity) {
        super(text, icon, quantity);
    }

    public Unlockable(String text, MaterialData icon, short data) {
        super(text, icon, data);
    }

    public Unlockable(String text, MaterialData icon, int quantity, short data) {
        super(text, icon, quantity, data);
    }

    protected abstract void onActivate(Player player);

    public void activate(Player player) {
        activeUnlockable.put(player.getUniqueId(), this);
        onActivate(player);
    }
    
    protected abstract void onDeactivate(Player player);

    public void deactivate(Player player) {
        onDeactivate(player);
        activeUnlockable.remove(player.getUniqueId());
    }

    public Rank getRequiredRank() {
        return requiredRank;
    }
    
    public Unlockable setRequiredRank(Rank rank) {
        this.requiredRank = rank;
        return this;
    }

    public int getMenuSlot() {
        return menuSlot;
    }

    public Unlockable setMenuSlot(int menuSlot) {
        this.menuSlot = menuSlot;
        return this;
    }
    
    public Unlockable setMenuSlot(int x, int y) {
        menuSlot = x * 9 + y - 10;
        return this;
    }

    public int getPoints() {
        return points;
    }

    public Unlockable setPoints(int points) {
        this.points = points;
        return this;
    }

    public Feature getFeature() {
        return feature;
    }

    public Unlockable setFeature(Feature feature) {
        this.feature = feature;
        return this;
    }

    public Rank getAccessWithoutFeature() {
        return accessWithoutFeature;
    }

    public Unlockable setAccessWithoutFeature(Rank accessWithoutFeature) {
        this.accessWithoutFeature = accessWithoutFeature;
        return this;
    }

    @Override
    public void onClick(Player player) {
        INetworkUser user = SessionAPI.getUser(player.getUniqueId());
        if (user == null) {
            return;
        }

        if (!hasPrivileges(user)) {
            if (feature == null) {
                insufficientPrivileges(player);
            } else {
                TransactionManager.submit(player, feature);
            }

            return;
        }

        if (activeUnlockable.containsKey(player.getUniqueId()) && activeUnlockable.get(player.getUniqueId()) == this) {
            onDeactivate(player);
            activeUnlockable.remove(player.getUniqueId(), this);
        } else {
            if (isActive(player)) {
                Unlockable unlockable = activeUnlockable.get(player.getUniqueId());
                unlockable.deactivate(player);
            }

            onActivate(player);
            activeUnlockable.put(player.getUniqueId(), this);
        }
    }

    public static boolean isActive(Player player) {
        return activeUnlockable.containsKey(player.getUniqueId());
    }

    protected void insufficientPrivileges(Player player) {
        MessageFormatter.sendErrorMessage(player, "Insufficient Privileges - " + requiredRank.getRankColor() + requiredRank.getDisplayName() + FontColor.RED + " rank is required to use this perk");
    }

    protected boolean hasPrivileges(INetworkUser player) {
        if (player.getRank().ordinal() < requiredRank.ordinal()) {
            return false;
        }

        if (feature != null && points > 0) {
            if (player.getRank().ordinal() < accessWithoutFeature.ordinal()) {
                boolean purchased = false;
                for (Transaction transaction : new ArrayList<>(player.getTransactions())) {
                    if (transaction.getFeature().equals(feature)) {
                        purchased = true;
                        break;
                    }
                }

                if (!purchased) {
                    return false;
                }
            }
        }

        return true;
    }
}

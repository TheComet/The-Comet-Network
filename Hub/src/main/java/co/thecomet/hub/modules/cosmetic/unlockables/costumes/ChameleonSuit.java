package co.thecomet.hub.modules.cosmetic.unlockables.costumes;

import co.thecomet.common.chat.FontColor;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.utils.ColorUtil;
import co.thecomet.core.utils.items.ItemBuilder;
import co.thecomet.core.utils.items.ItemHelper;
import co.thecomet.data.ranks.Rank;
import co.thecomet.hub.modules.cosmetic.Unlockable;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ChameleonSuit extends Unlockable implements Listener {
    private static Map<UUID, BukkitTask> tasks = new HashMap<>();
    public ChameleonSuit() {
        super(FontColor.translateString("&c&lChameleon Suit"), new MaterialData(Material.LEATHER_CHESTPLATE));
        this.setRequiredRank(Rank.ULTRA);
        this.setMenuSlot(1, 5);
        this.setDescriptions(new ArrayList<String>(){{
            add("");
            add(FontColor.translateString("&7Be awesome... like a chameleon!"));
            add("");
            add(FontColor.translateString("&6&lUnlocked at Ultra rank"));
        }});
    }

    @Override
    protected void onActivate(Player player) {
        tasks.put(player.getUniqueId(), new BodilSuitRunnable(player.getUniqueId()).runTaskTimer(CoreAPI.getPlugin(), 0, 1));
    }

    @Override
    protected void onDeactivate(Player player) {
        clean(player);
    }
    
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        clean(event.getPlayer());
    }
    
    private void clean(Player player) {
        if (tasks.containsKey(player.getUniqueId())) {
            BukkitTask task = tasks.get(player.getUniqueId());
            task.cancel();
            tasks.remove(player.getUniqueId());
        }

        player.getEquipment().setArmorContents(null);
    }

    public class BodilSuitRunnable extends BukkitRunnable {
        private float hue = 0.0f;
        private UUID uuid;
        
        public BodilSuitRunnable(UUID uuid) {
            this.uuid = uuid;
        }
        
        @Override
        public void run() {
            Player player = Bukkit.getPlayer(uuid);
            if (player == null) {
                cancel();
                return;
            }
            
            Color color = getNextColor();
            updateSuit(player, color);
        }
        
        private Color getNextColor() {
            hue += 0.01f;

            if (hue >= 1.0) {
                hue = 0.0f;
            }

            float saturation = 0.8f;
            float brightness = 0.8f;

            return ColorUtil.getColorFromHSB(hue, saturation, brightness);
        }
        
        private void updateSuit(Player player, Color color) {
            EntityEquipment equipment = player.getEquipment();
            
            if (equipment.getHelmet() == null) {
                ItemStack helmet = ItemBuilder.build(Material.LEATHER_HELMET)
                        .color(color)
                        .build();
                ItemHelper.equipArmor(player, helmet);
            } else {
                ItemStack helmet = equipment.getHelmet();
                ItemHelper.setColor(helmet, color);
            }

            if (equipment.getChestplate() == null) {
                ItemStack chestplate = ItemBuilder.build(Material.LEATHER_CHESTPLATE)
                        .color(color)
                        .build();
                ItemHelper.equipArmor(player, chestplate);
            } else {
                ItemStack chestplate = equipment.getChestplate();
                ItemHelper.setColor(chestplate, color);
            }

            if (equipment.getLeggings() == null) {
                ItemStack leggings = ItemBuilder.build(Material.LEATHER_LEGGINGS)
                        .color(color)
                        .build();
                ItemHelper.equipArmor(player, leggings);
            } else {
                ItemStack leggings = equipment.getLeggings();
                ItemHelper.setColor(leggings, color);
            }

            if (equipment.getBoots() == null) {
                ItemStack boots = ItemBuilder.build(Material.LEATHER_BOOTS)
                        .color(color)
                        .build();
                ItemHelper.equipArmor(player, boots);
            } else {
                ItemStack boots = equipment.getBoots();
                ItemHelper.setColor(boots, color);
            }

            player.updateInventory();
        }
    }
}

package co.thecomet.hub.modules.cosmetic.unlockables.gadgets;

import co.thecomet.common.chat.FontColor;
import co.thecomet.common.utils.RandomUtil;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.core.utils.items.ItemBuilder;
import co.thecomet.data.ranks.Rank;
import co.thecomet.hub.modules.cosmetic.Unlockable;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.FireworkEffect;
import org.bukkit.Material;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.material.MaterialData;

import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class FireworkHurler extends Unlockable implements Listener {
    private static final ItemStack FIREWORK = new ItemBuilder(Material.FIREWORK).name("Firework Hurler").build();
    private static final int COOLDOWN_TIME = 5;
    private static final Cache<UUID, Long> COOLDOWN = CacheBuilder.newBuilder().expireAfterWrite(COOLDOWN_TIME, TimeUnit.SECONDS).build();
    
    public FireworkHurler() {
        super(FontColor.translateString("&c&lFirework Hurler"), new MaterialData(Material.FIREWORK));
        this.setRequiredRank(Rank.VIP);
        this.setMenuSlot(1, 5);
        this.setDescriptions(new ArrayList<String>(){{
            add("");
            add(FontColor.translateString("&7Aim for the stars with a bang!"));
            add("");
            add(FontColor.translateString("&6&lUnlocked at VIP rank"));
        }});
    }

    @Override
    protected void onActivate(Player player) {
        player.getInventory().setItem(5, FIREWORK);
    }

    @Override
    protected void onDeactivate(Player player) {
        player.getInventory().setItem(5, null);
    }
    
    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (event.getItem() == null || event.getItem().getItemMeta() == null || event.getItem().getItemMeta().getDisplayName() == null) {
            return;
        }
        
        if (event.getItem().getItemMeta().getDisplayName().equals(FIREWORK.getItemMeta().getDisplayName()) == false) {
            return;
        }

        event.setCancelled(true);
        if (COOLDOWN.asMap().containsKey(event.getPlayer().getUniqueId())) {
            MessageFormatter.sendInfoMessage(event.getPlayer(), "You can use the Firework Hurler in " + ((COOLDOWN.asMap().get(event.getPlayer().getUniqueId()) + (COOLDOWN_TIME * 1000) - System.currentTimeMillis()) / 1000) + " seconds!");
            return;
        }
        
        run(event.getPlayer());
        COOLDOWN.put(event.getPlayer().getUniqueId(), System.currentTimeMillis());
    }
    
    public void run(Player player) {
        final Firework fw = player.getWorld().spawn(player.getLocation(), Firework.class);
        FireworkMeta fwm = fw.getFireworkMeta();
        
        int nType = RandomUtil.RANDOM.nextInt(FireworkEffect.Type.values().length);
        FireworkEffect.Type type = FireworkEffect.Type.values()[nType];
        
        Color c1 = DyeColor.values()[RandomUtil.RANDOM.nextInt(DyeColor.values().length)].getFireworkColor();
        Color c2 = DyeColor.values()[RandomUtil.RANDOM.nextInt(DyeColor.values().length)].getFireworkColor();
        
        FireworkEffect.Builder builder = FireworkEffect.builder();
        FireworkEffect effect = builder.flicker(RandomUtil.RANDOM.nextBoolean())
                .with(type)
                .withColor(c1)
                .withFade(c2)
                .trail(true)
                .build();
        
        fwm.addEffect(effect);
        fwm.setPower(2);
        fw.setFireworkMeta(fwm);
        fw.setPassenger(player);
    }
}

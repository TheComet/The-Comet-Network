package co.thecomet.hub.modules.toolbar;

import co.thecomet.common.chat.FontColor;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.module.Module;
import co.thecomet.core.module.ModuleInfo;
import co.thecomet.core.module.ModuleManager;
import co.thecomet.core.ui.MenuItem;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.hub.modules.gamemenu.GameMenuModule;
import co.thecomet.hub.modules.hublist.HubListModule;
import co.thecomet.hub.modules.preferences.PreferencesModule;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.material.MaterialData;

import java.util.ArrayList;
import java.util.List;

@ModuleInfo(name = "toolbar")
public class ToolbarModule extends Module {
    public void onEnable() {
        ModuleManager.registerModule(new GameMenuModule());
        ModuleManager.registerModule(new HubListModule());
        ModuleManager.registerModule(new PreferencesModule());

        MenuItem store = new MenuItem(FontColor.translateString("&5&lS&dtore"), new MaterialData(Material.NETHER_STAR)) {
            @Override
            public void onClick(Player player) {
                MessageFormatter.sendInfoMessage(player, "Go to &astore.thecomet.co &6to check out our amazing deals.");
            }
        };
        store.setSlot(3);
        Toolbar.register(this, store);

        MenuItem stats = new MenuItem(FontColor.translateString("&9&lS&9tats &8- &7Coming Soon"), new MaterialData(Material.SKULL_ITEM, (byte) 3)) {
            @Override
            public void onClick(Player player) {
                MessageFormatter.sendInfoMessage(player, "Coming soon!");
            }
        };
        stats.setSlot(7);
        Toolbar.register(this, stats);
    }

    public void onDisable() {
        ModuleManager.unregisterModules(GameMenuModule.class, HubListModule.class);
    }

    @Override
    public List<Listener> registerListeners() {
        return new ArrayList<Listener>() {{
            add(new Toolbar());
        }};
    }
}

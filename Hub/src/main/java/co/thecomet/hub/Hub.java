package co.thecomet.hub;

import co.thecomet.core.CorePlugin;
import co.thecomet.core.module.ModuleManager;
import co.thecomet.data.DAOManager;
import co.thecomet.data.delivery.UserDeliveryStats;
import co.thecomet.hub.commands.DebugCommands;
import co.thecomet.data.hub.HubData;
import co.thecomet.data.user.UserHubData;
import co.thecomet.hub.status.ServerInfoListener;
import org.bson.types.ObjectId;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.mongodb.morphia.dao.BasicDAO;

public class Hub extends CorePlugin implements Listener {
    private static Hub instance;
    private ServerInfoListener serverInfoListener;

    @Override
    public void enable() {
        instance = this;
        serverInfoListener = new ServerInfoListener();
        new DebugCommands();

        Bukkit.getWorlds().get(0).setGameRuleValue("doDaylightCycle", String.valueOf(false));
        Bukkit.getWorlds().get(0).setTime(6000);

        ModuleManager.registerModule(new HubModule());
    }

    @Override
    public void disable() {
        //
    }

    @Override
    public String getType() {
        return "HUB";
    }

    public ServerInfoListener getServerInfoListener() {
        return serverInfoListener;
    }

    public static Hub getInstance() {
        return instance;
    }

    public static BasicDAO<HubData, ObjectId> getHubDataDAO() {
        return DAOManager.getDAO(HubData.class);
    }

    public static BasicDAO<UserHubData, ObjectId> getPlayerHubDataDAO() {
        return DAOManager.getDAO(UserHubData.class);
    }

    public static BasicDAO<UserDeliveryStats, ObjectId> getUserDeliveryStatsDAO() {
        return DAOManager.getDAO(UserDeliveryStats.class);
    }
}

package co.thecomet.hub.commands;

import co.thecomet.core.CoreAPI;
import co.thecomet.core.command.CommandRegistry;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.data.ranks.Rank;
import co.thecomet.hub.status.ServerInfo;
import co.thecomet.hub.status.ServerInfoListener;
import org.bukkit.command.CommandSender;

public class DebugCommands {
    public DebugCommands() {
        CommandRegistry.registerUniversalCommand(CoreAPI.getPlugin(), "debug", Rank.ADMIN, DebugCommands::debug);
        CommandRegistry.registerUniversalSubCommand("debug", "slist", Rank.ADMIN, DebugCommands::slist);
    }
    
    public static void debug(CommandSender sender, String[] args) {
        MessageFormatter.sendUsageMessage(sender, "/debug slist");
    }
    
    public static void slist(CommandSender sender, String[] args) {
        for (ServerInfo info : ServerInfoListener.getServerInfos()) {
            MessageFormatter.sendInfoMessage(sender, info.getId() + ":" + info.getType() + ":" + info.getStatus() + ":" + info.getPlayersOnline() + "/" + info.getMaxPlayers());
        }
    }
}

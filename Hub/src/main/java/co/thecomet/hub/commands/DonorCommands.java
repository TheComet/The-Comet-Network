package co.thecomet.hub.commands;

import co.thecomet.core.CoreAPI;
import co.thecomet.core.command.CommandRegistry;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.data.ranks.Rank;
import org.bukkit.entity.Player;

public class DonorCommands {
    public DonorCommands() {
        CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "fly", Rank.ULTRA, DonorCommands::fly);
    }

    public static void fly(Player player, String[] args) {
        if (player.getAllowFlight()) {
            player.setAllowFlight(false);
            MessageFormatter.sendSuccessMessage(player, "You are no longer flying.");
        } else {
            player.setAllowFlight(true);
            MessageFormatter.sendSuccessMessage(player, "You are now flying.");
        }
    }
}

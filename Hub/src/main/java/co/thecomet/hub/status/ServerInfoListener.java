package co.thecomet.hub.status;

import com.archeinteractive.rc.RedisConnect;
import com.archeinteractive.rc.redis.pubsub.NetTaskSubscribe;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class ServerInfoListener {
    private static final Cache<String, ServerInfo> cache = CacheBuilder.newBuilder()
            .expireAfterWrite(5, TimeUnit.SECONDS)
            .build();

    public ServerInfoListener() {
        RedisConnect.getRedis().registerTask(this);
    }

    @NetTaskSubscribe(name = "serverUpdate", args = {"id", "type", "status", "playersOnline", "maxPlayers", "vipOnly", "joinable", "spectatorJoinInProgress"})
    public void retrieve(HashMap<String, Object> data) {
        String id, type, status;
        int playersOnline, maxPlayers;
        boolean vipOnly, joinable, spectatorJoinInProgress;

        id = (String) data.get("id");
        type = (String) data.get("type");
        status = (String) data.get("status");
        playersOnline = ((Number) data.get("playersOnline")).intValue();
        maxPlayers = ((Number) data.get("maxPlayers")).intValue();
        vipOnly = ((Boolean) data.get("vipOnly")).booleanValue();
        joinable = ((Boolean) data.get("joinable")).booleanValue();
        spectatorJoinInProgress = ((Boolean) data.get("spectatorJoinInProgress")).booleanValue();

        handle(id, type, status, playersOnline, maxPlayers, vipOnly, joinable, spectatorJoinInProgress);
    }

    private void handle(String id, String type, String status, int playersOnline, int maxPlayers, boolean vipOnly, boolean joinable, boolean spectatorJoinInProgress) {
        ServerType t = ServerType.findMatching(type);

        if (t == null) {
            return;
        }

        cache.put(id.toUpperCase(), new ServerInfo(id, type, status, playersOnline, maxPlayers, vipOnly, joinable, spectatorJoinInProgress));
    }

    public static List<ServerInfo> get(ServerType type) {
        List<ServerInfo> infos = new ArrayList<>();
        Map<String, ServerInfo> copy = cache.asMap();
        for (Iterator<String> ids = copy.keySet().iterator(); ids.hasNext();) {
            String id = ids.next();
            ServerInfo info = copy.get(id);
            
            if (info.getType().equalsIgnoreCase(type.name())) {
                infos.add(info);
            }
        }

        return infos;
    }

    public static int getOnlinePlayers() {
        int players = 0;
        Map<String, ServerInfo> copy = cache.asMap();
        for (String id : new ArrayList<>(copy.keySet())) {
            ServerInfo info = copy.get(id);
            players += info.getPlayersOnline();
        }
        return players;
    }

    public static List<ServerInfo> getServerInfos() {
        return new ArrayList<>(cache.asMap().values());
    }
}

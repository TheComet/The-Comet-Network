package co.thecomet.core.listeners;

import co.thecomet.common.utils.Log;
import co.thecomet.data.SessionAPI;
import co.thecomet.common.chat.FontColor;
import co.thecomet.data.DataAPI;
import co.thecomet.data.ranks.Rank;
import co.thecomet.core.CoreAPI;
import co.thecomet.data.user.INetworkUser;
import co.thecomet.data.user.User;
import co.thecomet.core.events.LoginSuccessEvent;
import co.thecomet.core.permission.PermissionManager;
import co.thecomet.core.player.NetworkUser;
import com.archeinteractive.rc.bukkit.BukkitConnector;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ConnectionListener implements Listener {
    private static boolean announceJoin = true;

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerPreLogin(AsyncPlayerPreLoginEvent event) {
        Log.debug("Player with uuid " + event.getUniqueId().toString() + " is logging in.");
        if (CoreAPI.isLoaded() == false) {
            event.setKickMessage("This server is currently starting up!");
            event.setLoginResult(AsyncPlayerPreLoginEvent.Result.KICK_OTHER);
            return;
        }

        Log.debug("Fetching user data for " + event.getUniqueId().toString());
        User user = DataAPI.initUser(event.getUniqueId(), event.getName(), event.getAddress().getHostAddress());
        if (user == null) {
            event.setKickMessage("Sorry, something went wrong while initializing player data.");
            event.setLoginResult(AsyncPlayerPreLoginEvent.Result.KICK_OTHER);
            return;
        }

        Log.debug("Setting last server for " + event.getUniqueId().toString());
        user.setLastServer(BukkitConnector.getInstance().getName());
        DataAPI.setLastServer(event.getUniqueId(), user.getLastServer());

        Log.debug("Wrapping user data for " + event.getUniqueId().toString());
        INetworkUser u = SessionAPI.addUser(new NetworkUser(user));

        Log.debug("Login complete. Sending off login success.");
        Bukkit.getScheduler().scheduleSyncDelayedTask(CoreAPI.getPlugin(), () -> Bukkit.getPluginManager().callEvent(new LoginSuccessEvent(u)));
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent event) {
        final Player player = event.getPlayer();
        final INetworkUser user = SessionAPI.getUser(player.getUniqueId());

        if (user == null) {
            event.getPlayer().kickPlayer("There was a problem loading your player data. Please inform the developers.");
            return;
        }

        if (user.getRank().ordinal() >= Rank.VIP.ordinal() && user.getRank().ordinal() <= Rank.ULTRA.ordinal()) {
            if (!user.getRankCoinsReceived().contains(user.getRank())) {
                user.getRankCoinsReceived().add(user.getRank());
                DataAPI.updateRankCoinsReceived(user.getUuid(), user.getRank());
                DataAPI.addCoins(user.getRank().getRankPurchaseCoins(), user.getUuid(), true);
            }
        }

        PermissionManager.update(user.getUuid());
        if (event.getPlayer().getGameMode() != GameMode.SURVIVAL) {
            event.getPlayer().setGameMode(GameMode.SURVIVAL);
        }
        event.getPlayer().getInventory().setHeldItemSlot(0);

        if (announceJoin && user.getRank().ordinal() >= Rank.ULTRA.ordinal()) {
            event.setJoinMessage(FontColor.translateString("&8[" + user.getRank().getRankColor().toString() + user.getRank().getDisplayName() + "&8] " + user.getRank().getChatColor().toString() + user.getName() + " &7has joined the server!"));
        } else {
            event.setJoinMessage(null);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onLoginSuccess(LoginSuccessEvent event) {
        INetworkUser u = event.getProfile();

        if (u.getModerationHistory().getActiveMute() != null) {
            ChatListener.addMute(u);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerQuit(PlayerQuitEvent event) {
        event.setQuitMessage(null);
        
        INetworkUser user = null;
        if ((user = SessionAPI.removeUser(event.getPlayer().getUniqueId())) == null) {
            return;
        }
        
        if (ChatListener.getMutedPlayers().containsKey(user.getUuid())) {
            ChatListener.getMutedPlayers().remove(user.getUuid());
        }

        DataAPI.setTimeStats(user.getUuid(), System.currentTimeMillis() - user.getLastOnline(), System.currentTimeMillis());
        PermissionManager.remove(user.getUuid());
    }

    public static boolean isAnnounceJoin() {
        return announceJoin;
    }

    public static void setAnnounceJoin(boolean announceJoin) {
        ConnectionListener.announceJoin = announceJoin;
    }

    public static void kick(Player player, String message) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(CoreAPI.getPlugin(), () -> player.kickPlayer(message));
    }
}

package co.thecomet.core.listeners;

import co.thecomet.data.SessionAPI;
import co.thecomet.common.chat.FontColor;
import co.thecomet.data.ranks.Rank;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.data.user.INetworkUser;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import net.jodah.expiringmap.ExpiringMap;
import org.apache.commons.lang.CharUtils;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class ChatListener implements Listener {
    private static Cache<UUID, Long> cooldown = CacheBuilder.newBuilder().expireAfterWrite(1, TimeUnit.SECONDS).build();
    private static Cache<UUID, String> lastMessage = CacheBuilder.newBuilder().expireAfterWrite(5, TimeUnit.MINUTES).build();
    private static ExpiringMap<UUID, Long> mutedPlayers = ExpiringMap.builder().variableExpiration().build();

    @EventHandler(priority = EventPriority.LOWEST)
    public void onChatLowest(AsyncPlayerChatEvent event) {
        INetworkUser user = SessionAPI.getUser(event.getPlayer().getUniqueId());
        if (user == null) {
            event.setCancelled(true);
            return;
        }

        if (event.getMessage().startsWith("connected with an") && event.getMessage().endsWith("using MineChat")) {
            event.setCancelled(true);
            event.getPlayer().kickPlayer(FontColor.translateString("&cYou cannot use MineChat on this network."));
            return;
        }
        
        if (user.getRank().ordinal() < Rank.HELPER.ordinal()) {
            if (mutedPlayers.containsKey(event.getPlayer().getUniqueId())) {
                MessageFormatter.sendErrorMessage(event.getPlayer(), "You are muted.");
                event.setCancelled(true);
                return;
            }

            if (cooldown.asMap().containsKey(event.getPlayer().getUniqueId())) {
                MessageFormatter.sendErrorMessage(event.getPlayer(), "You are sending messages to quickly.");
                event.setCancelled(true);
                return;
            }

            Map<UUID, String> lastMessageMap = lastMessage.asMap();
            if (lastMessageMap.containsKey(event.getPlayer().getUniqueId()) || isCapSpam(event.getMessage())) {
                if (lastMessageMap.get(event.getPlayer().getUniqueId()) != null) {
                    if (lastMessageMap.get(event.getPlayer().getUniqueId()).equalsIgnoreCase(event.getMessage())) {
                        MessageFormatter.sendErrorMessage(event.getPlayer(), "You are spamming the chat.");
                        event.setCancelled(true);
                        return;
                    }
                }
            }
        }

        lastMessage.put(event.getPlayer().getUniqueId(), event.getMessage());
        cooldown.put(event.getPlayer().getUniqueId(), System.currentTimeMillis());
        
        event.setFormat(getFormat(user));
    }
    
    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onChatHighest(AsyncPlayerChatEvent event) {
        INetworkUser user = SessionAPI.getUser(event.getPlayer().getUniqueId());
        event.setFormat(FontColor.translateString(parseFormat(event)));

        if (user.getRank().ordinal() >= Rank.VIPPLUS.ordinal()) {
            event.setMessage(FontColor.translateString(event.getMessage(), true));
        }
    }
    
    public String getFormat(INetworkUser user) {
        if (user.getRank() == Rank.DEFAULT) {
            return "{CCOLOR}%s &8> {CCOLOR}%s";
        }
        
        if (Bukkit.getPlayer(user.getUuid()).getName().equalsIgnoreCase("bodil40")) {
            return "{RCOLOR}&l{RANK} {CCOLOR}&l%s &r&8> {CCOLOR}&l%s";
        }
        
        if (user.getRank().ordinal() >= Rank.ULTRA.ordinal()) {
            return "{RCOLOR}&l{RANK} {CCOLOR}&l%s &r&8> {CCOLOR}%s";
        }

        return "{RCOLOR}&l{RANK} &r{CCOLOR}%s &8> {CCOLOR}%s";
    }

    public String parseFormat(AsyncPlayerChatEvent event) {
        INetworkUser user = SessionAPI.getUser(event.getPlayer().getUniqueId());
        String format = event.getFormat();
        if (user.getRank() != Rank.DEFAULT) {
            format = format.replace("{RCOLOR}", user.getRank().getRankColor().toString());
            format = format.replace("{RANK}", user.getRank().getDisplayName());
        }

        format = format.replace("{CCOLOR}", user.getRank().getChatColor().toString());
        format = FontColor.translateString(format);

        return format;
    }

    public static ExpiringMap<UUID, Long> getMutedPlayers() {
        return mutedPlayers;
    }

    public static void addMute(INetworkUser user) {
        if (user == null) {
            return;
        }

        mutedPlayers.put(user.getUuid(), System.currentTimeMillis(), user.getModerationHistory().getActiveMute().getExpiration().getTime() - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
    }

    public boolean isCapSpam(String message) {
        if (StringUtils.isAllUpperCase(message) || capSpam(message)) {
            return true;
        }

        return false;
    }

    public boolean capSpam(String message) {
        if (message.length() > 10) {
            int count = 0;
            for (char c : message.toCharArray()) {
               if (CharUtils.isAsciiAlphaUpper(c)) {
                   count += 1;
               }
            }

            if (((float) count / (float) message.length()) > 30.0f) {
                return true;
            }
        }

        return false;
    }
}

package co.thecomet.core;

import co.thecomet.common.config.JsonConfig;
import co.thecomet.common.utils.Log;
import co.thecomet.core.command.CommandListener;
import co.thecomet.core.config.CoreConfig;
import co.thecomet.core.listeners.ChatListener;
import co.thecomet.core.listeners.ConnectionListener;
import co.thecomet.core.moderation.commands.*;
import co.thecomet.core.module.Module;
import co.thecomet.core.module.ModuleInfo;
import co.thecomet.core.module.ModuleManager;
import co.thecomet.core.player.commands.UserCommands;
import co.thecomet.core.utils.announcer.AnnouncementConfig;
import co.thecomet.core.utils.announcer.AnnouncerBuilder;
import co.thecomet.core.utils.announcer.AnnouncerCommands;
import co.thecomet.core.utils.conversion.ImporterModule;
import org.bukkit.event.Listener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@ModuleInfo(name = "core")
public class CoreModule extends Module {
    private static CoreConfig coreConfig;
    private AnnouncementConfig announcementConfig;
    private AnnouncerBuilder.Announcer announcer;
    private ServerInfoPublisher publisher;

    @Override
    public void onEnable() {
        Log.debug("Loading core config.");
        loadConfig();
        Log.debug("Loading core announcer.");
        loadAnnouncer();
        Log.debug("Loading core publisher.");
        publisher = new ServerInfoPublisher(CoreAPI.getPlugin());

        if (coreConfig.importConvertedData) {
            Log.debug("Prepping for data import.");
            ModuleManager.registerModule(new ImporterModule());
            coreConfig.importConvertedData = false;
            coreConfig.save(new File(CoreAPI.getPlugin().getDataFolder(), "core.json"));
        }

        Log.debug("Registering core commands.");
        registerCommands();
        Log.debug("Disabling commands");
        CommandListener.disableCommands("me", "tell", "pl", "plugins", "kill");
    }
    
    public void loadConfig() {
        coreConfig = JsonConfig.load(new File(CoreAPI.getPlugin().getDataFolder(), "core.json"), CoreConfig.class);
        coreConfig.save(new File(CoreAPI.getPlugin().getDataFolder(), "core.json"));
    }

    public void loadAnnouncer() {
        announcementConfig = JsonConfig.load(new File(CoreAPI.getPlugin().getDataFolder(), "announcements.json"), AnnouncementConfig.class);
        announcementConfig.save(new File(CoreAPI.getPlugin().getDataFolder(), "announcements.json"));

        if (announcer == null) {
            announcer = AnnouncerBuilder.newBuilder().fromConfig(announcementConfig).build(true);
        } else {
            announcer = AnnouncerBuilder.newBuilder().fromAnnouncer(announcer).build(true);
        }
    }

    @Override
    public List<Listener> registerListeners() {
        return new ArrayList<Listener>(){{
            add(new ConnectionListener());
            add(new ChatListener());
        }};
    }

    public void registerCommands() {
        new BanCommands();
        new KickCommands();
        new MuteCommands();
        new StatCommands();
        new UserCommands();
        new NetworkCommands();
        new AnnouncerCommands();
    }

    public static CoreConfig getCoreConfig() {
        return coreConfig;
    }

    public AnnouncerBuilder.Announcer getAnnouncer() {
        return announcer;
    }
}

package co.thecomet.core.player;

import co.thecomet.common.chat.MessageType;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.data.moderation.ModerationHistory;
import co.thecomet.data.ranks.Rank;
import co.thecomet.data.transaction.Transaction;
import co.thecomet.data.user.INetworkUser;
import co.thecomet.data.user.User;
import co.thecomet.data.DAOManager;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.*;

public class NetworkUser implements INetworkUser {
    @Getter
    private UUID uuid;
    @Getter @Setter
    private String name;
    @Getter @Setter
    private String nameLower;
    @Getter
    private List<String> nameHistory;
    @Getter @Setter
    private String ip;
    @Getter
    private List<String> ipHistory;
    @Getter @Setter
    private Rank rank;
    @Getter @Setter
    private long playTime;
    @Getter @Setter
    private long lastOnline;
    @Getter @Setter
    private String lastServer;
    @Getter
    private ModerationHistory moderationHistory;
    @Getter @Setter
    private int points;
    @Getter @Setter
    private int coins;
    @Getter @Setter
    private List<Transaction> transactions = new ArrayList<>();
    @Getter @Setter
    private List<Rank> rankCoinsReceived;
    @Getter
    private Map<Class<?>, Object> data = new HashMap<Class<?>, Object>();

    public NetworkUser(User user) {
        this.uuid = UUID.fromString(user.getUuid());
        this.name = user.getName();
        this.nameLower = user.getNameLower();
        this.nameHistory = user.getNameHistory();
        this.ip = user.getIp();
        this.ipHistory = user.getIpHistory();
        this.rank = user.getRank();
        this.playTime = user.getPlayTime();
        this.lastOnline = user.getLastOnline();
        this.lastServer = user.getLastServer();
        this.moderationHistory = user.getModerationHistory();
        this.points = user.getPoints();
        this.coins = user.getCoins();
        this.transactions = user.getTransactions();
        this.rankCoinsReceived = user.getRankCoinsReceived();
    }

    /**
     * Retrieves a player's profile from the database.
     * This method should be used sparingly and only when needed.
     *
     * @return
     */
    public User getProfile() {
        return DAOManager.getDAO(User.class).findOne("uuid", uuid.toString());
    }

    public <T> T getData(Class<T> clazz) {
        Object object = data.get(clazz);
        if (object == null || !clazz.isInstance(object)) {
            return null;
        }
        return clazz.cast(object);
    }

    public <T> void setData(Class<T> clazz, T data) {
        this.data.put(clazz, data);
    }

    @Override
    public void incrementPlayTime(long time) {
        this.playTime += time;
    }

    @Override
    public void incrementPoints(int points) {
        this.points += points;
    }

    @Override
    public void incrementCoins(int coins) {
        this.coins += coins;
    }

    @Override
    public void sendMessage(MessageType type, String message) {
        Player player = Bukkit.getPlayer(uuid);

        if (player != null) {
            switch (type) {
                case INFO:
                    MessageFormatter.sendInfoMessage(player, message);
                    break;
                case ERROR:
                    MessageFormatter.sendErrorMessage(player, message);
                    break;
                case USAGE:
                    MessageFormatter.sendUsageMessage(player, message);
                    break;
                case SUCCESS:
                    MessageFormatter.sendSuccessMessage(player, message);
                    break;
                case GENERAL:
                    MessageFormatter.sendGeneralMessage(player, message);
                    break;
                case ANNOUNCEMENT:
                    MessageFormatter.sendAnnouncement(player, message);
            }
        }
    }

    @Override
    public void sendMessages(MessageType type, String... messages) {
        for (String message : messages) {
            sendMessage(type, message);
        }
    }
}

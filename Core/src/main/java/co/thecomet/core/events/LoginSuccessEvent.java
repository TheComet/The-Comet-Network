package co.thecomet.core.events;

import co.thecomet.data.user.INetworkUser;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class LoginSuccessEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private INetworkUser profile;

    public LoginSuccessEvent(INetworkUser profile) {
        this.profile = profile;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public INetworkUser getProfile() {
        return profile;
    }
}

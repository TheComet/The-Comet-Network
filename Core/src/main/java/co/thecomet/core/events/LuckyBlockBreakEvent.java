package co.thecomet.core.events;

import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyBlocks;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.TriggerBlock;
import co.thecomet.core.module.ModuleManager;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class LuckyBlockBreakEvent extends Event implements Cancellable {
    private static HandlerList handlerList = new HandlerList();
    
    @Override
    public HandlerList getHandlers() {
        return getHandlerList();
    }
    
    private final TriggerBlock triggerBlock;
    private final LuckyBlock luckyBlock;
    private final Location location;
    private final Player whoBroke;
    private boolean cancelled;

    public LuckyBlockBreakEvent(TriggerBlock triggerBlock, LuckyBlock luckyBlock, Location location, Player whoBroke) {
        this.triggerBlock = triggerBlock;
        this.luckyBlock = luckyBlock;
        this.location = location;
        this.whoBroke = whoBroke;
    }
    
    public LuckyInfo getLuckyInfo() {
        LuckyBlocks luckyBlocks = ModuleManager.getModule(LuckyBlocks.class);
        return luckyBlocks == null ? null : luckyBlocks.getLuckyInfo(luckyBlock.getClass());
    }

    public static HandlerList getHandlerList() {
        return handlerList;
    }

    public TriggerBlock getTriggerBlock() {
        return triggerBlock;
    }

    public LuckyBlock getLuckyBlock() {
        return luckyBlock;
    }

    public Location getLocation() {
        return location;
    }

    public Player getWhoBroke() {
        return whoBroke;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}

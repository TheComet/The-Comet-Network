package co.thecomet.core.events;

import co.thecomet.data.user.User;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class SetPlayerRankEvent extends Event {

    private static final HandlerList handlers = new HandlerList();
    private User profile;

    public SetPlayerRankEvent(User profile) {
        this.profile = profile;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public User getProfile() {
        return profile;
    }
}

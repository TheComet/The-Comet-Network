package co.thecomet.core.chat;

import co.thecomet.core.utils.reflection.VersionHandler;

@SuppressWarnings("rawtypes")
public enum TitleAction {
    TITLE,
    SUBTITLE,
    TIMES,
    CLEAR,
    RESET;

    Enum enom;

    TitleAction() {
        Class<?> clazz = VersionHandler.getNMSClass("PacketPlayOutTitle$EnumTitleAction");
        Object[] constants = clazz.getEnumConstants();

        for (Object object : constants) {
            if (object instanceof Enum) {
                Enum enom = (Enum) object;
                if (enom.name().equalsIgnoreCase(this.name())) {
                    this.enom = enom;
                }
            }
        }
    }

    public Enum getEnom() {
        return enom;
    }

}

package co.thecomet.core.chat;

import co.thecomet.core.utils.reflection.EntityHandler;
import co.thecomet.core.utils.reflection.packets.WrapperPlayOutChat;
import co.thecomet.core.utils.reflection.packets.WrapperPlayOutTitle;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

@SuppressWarnings("deprecation")
public class MessageService {
    /**
     * Sends an action bar announcement to all players online.
     *
     * @param message
     */
    public static void actionAnnouncement(String message) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            actionAnnouncement(player, "{\"text\":\"" + message + "\"}");
        }
    }

    /**
     * Sends an action bar announcement to the specified player.
     *
     * @param player
     * @param message
     */
    public static void actionAnnouncement(Player player, String message) {
        EntityHandler.sendPacket(player, new WrapperPlayOutChat(message, (byte) 2).get());
    }

    public static void sendTitleAnnouncement(String title) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            sendTitleAnnouncement(player, title);
        }
    }

    public static void sendTitleAnnouncement(String title, int fadeIn, int wait, int fadeOut) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            sendTitleAnnouncement(player, title, fadeIn, wait, fadeOut);
        }
    }

    public static void sendTitleAnnouncement(Player player, String title) {
        EntityHandler.sendPacket(player, new WrapperPlayOutTitle(TitleAction.TITLE, title, 40, 100, 40).get());
    }

    public static void sendTitleAnnouncement(Player player, String title, int fadeIn, int wait, int fadeOut) {
        EntityHandler.sendPacket(player, new WrapperPlayOutTitle(TitleAction.TITLE, title, fadeIn, wait, fadeOut).get());
    }

    public static void sendSubtitleAnnouncement(String subtitle) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            sendSubtitleAnnouncement(player, subtitle);
        }
    }

    public static void sendSubtitleAnnouncement(String subtitle, int fadeIn, int wait, int fadeOut) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            sendSubtitleAnnouncement(player, subtitle, fadeIn, wait, fadeOut);
        }
    }

    public static void sendSubtitleAnnouncement(Player player, String subtitle) {
        EntityHandler.sendPacket(player, new WrapperPlayOutTitle(TitleAction.SUBTITLE, subtitle, 40, 100, 40).get());
    }

    public static void sendSubtitleAnnouncement(Player player, String subtitle, int fadeIn, int wait, int fadeOut) {
        EntityHandler.sendPacket(player, new WrapperPlayOutTitle(TitleAction.SUBTITLE, subtitle, fadeIn, wait, fadeOut).get());
    }

    public static void sendStandaloneSubtitleAnnouncement(String subtitle) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            sendSubtitleAnnouncement(player, subtitle);
        }
    }

    public static void sendStandaloneSubtitleAnnouncement(String subtitle, int fadeIn, int wait, int fadeOut) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            sendSubtitleAnnouncement(player, subtitle, fadeIn, wait, fadeOut);
        }
    }

    public static void sendStandaloneSubtitleAnnouncement(Player player, String subtitle) {
        sendTitleAnnouncement(player, "", 40, 100, 40);
        sendSubtitleAnnouncement(player, subtitle, 40, 100, 40);
    }

    public static void sendStandaloneSubtitleAnnouncement(Player player, String subtitle, int fadeIn, int wait, int fadeOut) {
        sendTitleAnnouncement(player, "", fadeIn, wait, fadeOut);
        sendSubtitleAnnouncement(player, subtitle, fadeIn, wait, fadeOut);
    }

    public static void sendFullTitleAnnouncement(String title, String subtitle) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            sendFullTitleAnnouncement(player, title, subtitle);
        }
    }

    public static void sendFullTitleAnnouncement(String title, String subtitle, int fadeIn, int wait, int fadeOut) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            sendFullTitleAnnouncement(player, title, subtitle, fadeIn, wait, fadeOut);
        }
    }

    public static void sendFullTitleAnnouncement(Player player, String title, String subtitle) {
        sendTitleAnnouncement(player, title, 40, 100, 40);
        sendSubtitleAnnouncement(player, subtitle, 40, 100, 40);
    }

    public static void sendFullTitleAnnouncement(Player player, String title, String subtitle, int fadeIn, int wait, int fadeOut) {
        sendTitleAnnouncement(player, title, fadeIn, wait, fadeOut);
        sendSubtitleAnnouncement(player, subtitle, fadeIn, wait, fadeOut);
    }

}

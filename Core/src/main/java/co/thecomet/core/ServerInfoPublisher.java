package co.thecomet.core;

import co.thecomet.common.utils.Log;
import com.archeinteractive.rc.RedisConnect;
import com.archeinteractive.rc.redis.pubsub.NetTask;
import org.bukkit.scheduler.BukkitRunnable;

public class ServerInfoPublisher extends BukkitRunnable {
    public ServerInfoPublisher(CorePlugin plugin) {
        Log.debug("Registering publisher channel \"serverUpdate\"");
        RedisConnect.getRedis().registerChannel("serverUpdate");
        Log.debug("Initializing publisher task");
        this.runTaskTimer(plugin, 20 * 2, 20 * 2);
    }

    @Override
    public void run() {
        send();
    }

    public void send() {
        NetTask.withName("serverUpdate")
                .withArg("id", CoreAPI.getPlugin().getId())
                .withArg("type", CoreAPI.getPlugin().getType())
                .withArg("status", CoreAPI.getPlugin().getStatus())
                .withArg("playersOnline", CoreAPI.getPlugin().getPlayersOnline())
                .withArg("maxPlayers", CoreAPI.getPlugin().getMaxPlayers())
                .withArg("vipOnly", CoreAPI.getPlugin().isVipOnly())
                .withArg("joinable", CoreAPI.getPlugin().isJoinable())
                .withArg("spectatorJoinInProgress", CoreAPI.getPlugin().isSpectatorJoinInProgress()).send("serverUpdate");
    }
}

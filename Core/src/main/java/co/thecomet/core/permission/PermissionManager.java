package co.thecomet.core.permission;

import co.thecomet.data.SessionAPI;
import co.thecomet.common.async.Async;
import co.thecomet.data.ranks.Rank;
import co.thecomet.core.CoreAPI;
import co.thecomet.data.ranks.RankData;
import co.thecomet.data.user.INetworkUser;
import org.bukkit.Bukkit;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PermissionManager {
    private static Map<Rank, RankData> ranks = new HashMap<>();
    private static Map<UUID, PermissionEntry> permissions = new HashMap<>();
    
    public static void updateAll() {
        SessionAPI.getUsers().forEach(user -> update(user.getUuid()));
    }

    public static void update(final UUID uuid) {
        Async.execute(() -> {
            INetworkUser u;
            
            while ((u = SessionAPI.getUser(uuid)) == null) {
                if (Bukkit.getPlayer(uuid) == null) {
                    return;
                }
            }
            
            final INetworkUser user = u;
            Bukkit.getScheduler().scheduleSyncDelayedTask(CoreAPI.getPlugin(), () -> update(user));
        });
    }
    
    private static void update(final INetworkUser user) {
        PermissionEntry entry = permissions.get(user.getUuid());
        if (entry == null) {
            permissions.put(user.getUuid(), (entry = new PermissionEntry(user)));
        }

        final PermissionEntry finalEntry = entry;
        entry.attachment.getPermissions().keySet().forEach(entry.attachment::unsetPermission);
        Arrays.asList(Rank.values()).forEach((rank) -> {
            if (user.getRank().ordinal() >= rank.ordinal()) {
                RankData data = ranks.get(rank);
                new HashMap<>(data.getPermissions()).entrySet().forEach((e) -> {
                    String perm = e.getKey().replace("_", ".");
                    if (e.getValue() == false && finalEntry.attachment.getPermissions().containsKey(perm)) {
                        finalEntry.attachment.unsetPermission(perm);
                        return;
                    }

                    finalEntry.attachment.setPermission(perm, e.getValue());
                });
            }
        });
    }

    public static void remove(UUID uuid) {
        PermissionEntry entry = permissions.get(uuid);
        
        if (entry != null) {
            entry.attachment.remove();
            permissions.remove(uuid);
        }
    }

    public static Map<Rank, RankData> getRanks() {
        return ranks;
    }

    public static Map<UUID, PermissionEntry> getPermissions() {
        return new HashMap<>(permissions);
    }
}

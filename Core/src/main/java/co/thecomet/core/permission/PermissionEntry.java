package co.thecomet.core.permission;

import co.thecomet.data.ranks.Rank;
import co.thecomet.core.CoreAPI;
import co.thecomet.data.user.INetworkUser;
import org.bukkit.Bukkit;
import org.bukkit.permissions.PermissionAttachment;

public class PermissionEntry {
    protected Rank rank;
    protected PermissionAttachment attachment;

    public PermissionEntry(INetworkUser player) {
        this.rank = player.getRank();
        this.attachment = Bukkit.getPlayer(player.getUuid()).addAttachment(CoreAPI.getPlugin());
    }

    public Rank getRank() {
        return rank;
    }

    public PermissionAttachment getAttachment() {
        return attachment;
    }
}

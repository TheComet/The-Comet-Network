package co.thecomet.core.permission.commands;

import co.thecomet.data.DAOManager;
import co.thecomet.data.ranks.Rank;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.command.CommandRegistry;
import co.thecomet.data.ranks.RankData;
import co.thecomet.core.permission.PermissionManager;
import co.thecomet.core.utils.MessageFormatter;
import org.bukkit.command.CommandSender;

public class PermissionCommands {
    public PermissionCommands() {
        CommandRegistry.registerUniversalCommand(CoreAPI.getPlugin(), "perm", Rank.ADMIN, PermissionCommands::perm);
        CommandRegistry.registerUniversalSubCommand("perm", "set", Rank.ADMIN, PermissionCommands::set);
        CommandRegistry.registerUniversalSubCommand("perm", "remove", Rank.ADMIN, PermissionCommands::remove);
    }

    public static void perm(CommandSender sender, String[] args) {
        MessageFormatter.sendUsageMessage(sender, "/perm set <perm> <true|false> [rank]", "/perm remove <perm> [rank]");
    }

    public static void set(CommandSender sender, String[] args) {
        if (args.length < 2 || args.length > 3) {
            MessageFormatter.sendUsageMessage(sender, "/perm set <perm> <true|false> [rank]");
            return;
        }

        String perm = args[0].replace(".", "_");
        Boolean bool;

        try {
            bool = Boolean.parseBoolean(args[1]);
        } catch (NumberFormatException e) {
            MessageFormatter.sendUsageMessage(sender, "/perm set <perm> <true|false> [rank]");
            return;
        }

        Rank rank = args.length == 2 ? Rank.DEFAULT : Rank.getRankFromString(args[2]);
        RankData data = PermissionManager.getRanks().get(rank);
        data.getPermissions().put(perm, bool);
        PermissionManager.getPermissions().entrySet().forEach((entry) -> {
            if (entry.getValue().getRank().ordinal() >= rank.ordinal()) {
                PermissionManager.update(entry.getKey());
            }
        });

        CoreAPI.async(() -> DAOManager.getDAO(RankData.class).save(data));
        PermissionManager.updateAll();
        MessageFormatter.sendSuccessMessage(sender, "You added " + perm + " to " + rank.name());
    }

    public static void remove(CommandSender sender, String[] args) {
        if (args.length < 1 || args.length > 2) {
            MessageFormatter.sendUsageMessage(sender, "/perm remove <perm> [rank]");
            return;
        }

        String perm = args[0].replace(".", "_");
        Rank rank = args.length != 2 ? Rank.DEFAULT : Rank.getRankFromString(args[1]);
        RankData data = PermissionManager.getRanks().get(rank);
        data.getPermissions().remove(perm);
        PermissionManager.getPermissions().entrySet().forEach((entry) -> {
            if (entry.getValue().getRank().ordinal() >= rank.ordinal()) {
                PermissionManager.update(entry.getKey());
            }
        });

        CoreAPI.async(() -> DAOManager.getDAO(RankData.class).save(data));
        PermissionManager.updateAll();
        MessageFormatter.sendSuccessMessage(sender, "You removed " + perm + " from " + rank.name());
    }
}

package co.thecomet.core.luckyblocks;

import co.thecomet.common.utils.RandomUtil;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.Random;

public abstract class LuckyBlock {
    protected static Random RANDOM = RandomUtil.RANDOM;

    public abstract void onBreak(Block block, Player player);

    protected final void explode(Location loc, float power, boolean setFire, boolean destroy) {
        loc.getWorld().createExplosion(loc.getX(), loc.getY(), loc.getZ(), power, setFire, destroy);
    }

    /**
     * This allows us to determine if the lucky block is able to spawn at the desired location
     */
    public boolean canSpawn(Location loc, Player whoBroke) {
        return Boolean.TRUE;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof LuckyBlock) {
            return ((LuckyBlock) o).getClass().getSimpleName().equals(getClass().getSimpleName());
        }

        return super.equals(o);
    }
}

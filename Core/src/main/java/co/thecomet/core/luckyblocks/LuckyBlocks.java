package co.thecomet.core.luckyblocks;

import co.thecomet.core.serialization.BukkitJsonLocation;
import co.thecomet.data.SessionAPI;
import co.thecomet.data.ranks.Rank;
import co.thecomet.common.utils.RandomUtil;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.command.CommandRegistry;
import co.thecomet.common.serialization.JsonLocation;
import co.thecomet.core.events.LuckyBlockBreakEvent;
import co.thecomet.core.luckyblocks.types.*;
import co.thecomet.core.module.Module;
import co.thecomet.core.module.ModuleInfo;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.data.user.INetworkUser;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Maps;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.*;
import java.util.concurrent.TimeUnit;

@ModuleInfo(name = "LuckyBlocks")
public class LuckyBlocks extends Module implements Listener {
    private static List<UUID> receivingAnnouncements = new ArrayList<>();
    
    private List<TriggerBlock> triggerBlocks;
    private EnumMap<LuckyType, List<Class<? extends LuckyBlock>>> luckyBlocks; // TODO: Make ASYNC
    private EnumMap<LuckyType, Double> max;
    private boolean canCancel = false;
    private String vipOnlyMessage = "Only VIPs can break this Lucky Block";
    private Cache<JsonLocation, LuckyBlock> checkExpiring = CacheBuilder.newBuilder().expireAfterWrite(50, TimeUnit.MILLISECONDS).build();
    private List<Class> registeredTypeListeners = new ArrayList<>();

    @Override
    public void onEnable() {
        this.triggerBlocks = new ArrayList<>();
        this.max = Maps.newEnumMap(LuckyType.class);
        this.luckyBlocks = Maps.newEnumMap(LuckyType.class);

        this.triggerBlocks.add(new TriggerBlock(Material.SPONGE, LuckyType.NORMAL));
        this.triggerBlocks.add(new TriggerBlock(Material.SPONGE, LuckyType.VIP).setData((byte) 1));

        CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "lb", Rank.MOD, LuckyBlocks::lb);
        CommandRegistry.registerPlayerSubCommand("lb", "togann", Rank.MOD, LuckyBlocks::toggleAnnouncements);
        
        for (LuckyType type : Arrays.asList(LuckyType.VIP, LuckyType.NORMAL)) {
            this.luckyBlocks.put(type, new ArrayList<>());
            this.max.put(type, 0.0);
        }
    }

    public void onDisable() {
        this.luckyBlocks.clear();
        this.luckyBlocks = null;
    }

    @Override
    public List<Listener> registerListeners() {
        List<Listener> listeners = new ArrayList<>();
        listeners.add(this);
        return listeners;
    }

    public LuckyInfo getLuckyInfo(Class<? extends LuckyBlock> lb) {
        if (lb.isAnnotationPresent(LuckyInfo.class)) {
            LuckyInfo type = lb.getAnnotation(LuckyInfo.class);

            return type;
        }

        return null;
    }

    public boolean isValidLuckyBlock(Class<? extends LuckyBlock> lb) {
        LuckyInfo info = getLuckyInfo(lb);

        if (info == null) return false;
        if (info.name() == null || info.name().isEmpty()) return false;
        if (info.type() == null) return false;

        return true;
    }

    public boolean hasLuckyBlocks(LuckyType type) {
        return !luckyBlocks.get(type).isEmpty();
    }

    public void removeLuckyBlock(Class<? extends LuckyBlock> lb) {
        if (isValidLuckyBlock(lb)) {
            LuckyInfo info = getLuckyInfo(lb);
            luckyBlocks.get(info.type()).remove(lb);
        }
    }

    public void registerLuckyBlock(Class<? extends LuckyBlock> lb) {
        if (!isValidLuckyBlock(lb)) {
            CoreAPI.getPlugin().getLogger().info("Unable to register LuckyBlock: " + lb.getSimpleName());
            return;
        }

        try {
            LuckyInfo info = getLuckyInfo(lb);
            if (info.type() == LuckyType.BOTH) {
                for (LuckyType t : Arrays.asList(LuckyType.NORMAL, LuckyType.VIP)) {
                    luckyBlocks.get(t).add(lb);
                    setMax(t, getMax(t) + info.dropChance());
                }
            } else {
                luckyBlocks.get(info.type()).add(lb);
                setMax(info.type(), getMax(info.type()) + info.dropChance());
            }

            CoreAPI.getPlugin().getLogger().info("Registered LuckyBlock: " + info.name());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void registerLuckyBlocks(Class<? extends LuckyBlock> ... luckyBlocks) {
        for (Class lb : luckyBlocks) {
            registerLuckyBlock(lb);
        }
    }

    public boolean isLucky(Location loc) {
        return getTrigger(loc.getBlock().getType(), loc.getBlock().getData()) != null;
    }

    public boolean addTrigger(TriggerBlock tb) {
        if (tb.getLuckyType() == LuckyType.BOTH) {
            throw new UnsupportedOperationException("TriggerBlock cannot support LuckyType.BOTH");
        }
        return this.triggerBlocks.add(tb);
    }

    public double getMax(LuckyType type) {
        return this.max.get(type);
    }

    public void setMax(LuckyType type, double max) {
        if (type == LuckyType.BOTH) {
            for (LuckyType t : Arrays.asList(LuckyType.NORMAL, LuckyType.VIP)) {
                this.max.put(t, max);
            }

            return;
        }

        this.max.put(type, max);
    }

    public TriggerBlock getTrigger(Material mat, byte data) {
        for (TriggerBlock tb : this.triggerBlocks) {
            if (tb.getMaterial() == mat && tb.getData() == data) {
                return tb;
            }
        }

        return null;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    void onBlockBreak(BlockBreakEvent event) {
        if (canCancel && event.isCancelled()) return;
        JsonLocation location = new BukkitJsonLocation(event.getBlock().getLocation()).getSerializedLocation();
        if (!checkExpiring.asMap().containsKey(location)) {
            TriggerBlock tb = getTrigger(event.getBlock().getType(), event.getBlock().getData());

            if (tb == null) {
                return;
            }

            event.setCancelled(true);
            LuckyBlock lb = getLuckyBlock(tb.getLuckyType(), event.getBlock().getLocation(), event.getPlayer());
            LuckyInfo info = getLuckyInfo(lb.getClass());

            INetworkUser user = SessionAPI.getUser(event.getPlayer().getUniqueId());
            if (user == null) {
                event.setCancelled(true);
                return;
            }
            
            if (tb.getLuckyType() == LuckyType.VIP && user.getRank().ordinal() < Rank.VIP.ordinal()) {
                event.getPlayer().sendMessage(ChatColor.RED + vipOnlyMessage);
                return;
            }

            LuckyBlockBreakEvent lbbe = new LuckyBlockBreakEvent(tb, lb, event.getBlock().getLocation(), event.getPlayer());
            Bukkit.getPluginManager().callEvent(lbbe);
            if (lbbe.isCancelled()) {
                return;
            }

            checkExpiring.put(location, lb);
            event.getBlock().setType(Material.AIR);

            try {
                lb.onBreak(event.getBlock(), event.getPlayer());
                for (INetworkUser u : SessionAPI.getUsers()) {
                    if (u.getRank().ordinal() >= Rank.MOD.ordinal()) {
                        Player p = Bukkit.getPlayer(u.getUuid());
                        if (p != null && receivingAnnouncements.contains(p.getUniqueId())) {
                            MessageFormatter.sendAnnouncement(p, "LB: " + ChatColor.GREEN + event.getPlayer().getName() + " opened " + info.name() + " (" + tb.getLuckyType() + ")");
                        }
                    }
                }
                
                if (lb instanceof Listener) {
                    if (info.uniqueRegister() == false || (info.uniqueRegister() && registeredTypeListeners.contains(lb.getClass()) == false)) {
                        Bukkit.getPluginManager().registerEvents((Listener) lb, CoreAPI.getPlugin());
                        
                        if (registeredTypeListeners.contains(lb.getClass()) == false) {
                            registeredTypeListeners.add(lb.getClass());
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                event.getBlock().setType(tb.getMaterial());
                event.getBlock().setData(tb.getData());
                removeLuckyBlock(lb.getClass());
                
                if (lb instanceof Listener) {
                    HandlerList.unregisterAll((Listener) lb);
                }
                
                if (info.type() == LuckyType.BOTH) {
                    for (LuckyType type : Arrays.asList(LuckyType.NORMAL, LuckyType.VIP)) {
                        setMax(type, getMax(type) - info.dropChance());
                    }
                } else {
                    setMax(info.type(), getMax(info.type()) - info.dropChance());
                }
            }
        }
    }

    public LuckyBlock getLuckyBlock(LuckyType type, Location loc, Player whoBroke) {
        double fnord = RandomUtil.RANDOM.nextDouble() * getMax(type);

        LuckyBlock lb = null;
        int times = 10;

        do {
            times--;
            List<Class<? extends LuckyBlock>> types = luckyBlocks.get(type);
            Collections.shuffle(types);

            for (Class<? extends LuckyBlock> clazz : types) {
                fnord -= getLuckyInfo(clazz).dropChance();

                if (fnord <= 0) {
                    LuckyBlock b = newInstance(clazz);

                    if (b == null) {
                        continue;
                    }
                    
                    if (b.canSpawn(loc, whoBroke)) {
                        lb = b;
                        break;
                    }
                }
            }
        } while (lb == null && times >= 0);

        return lb != null ? lb : new LolChest();
    }

    private LuckyBlock newInstance(Class<? extends LuckyBlock> clazz) {
        try {
            return clazz.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getVipOnlyMessage() {
        return vipOnlyMessage;
    }

    public void setVipOnlyMessage(String vipOnlyMessage) {
        this.vipOnlyMessage = vipOnlyMessage;
    }

    public boolean isCancellable() {
        return canCancel;
    }

    public void setCanCancel(boolean cancel) {
        this.canCancel = cancel;
    }
    
    public void clearLuckyBlocks() {
        luckyBlocks.clear();
    }
    
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        receivingAnnouncements.remove(event.getPlayer().getUniqueId());
    }
    
    public static void lb(Player player, String[] args) {
        MessageFormatter.sendUsageMessage(player, "/lb togann");
    }
    
    public static void toggleAnnouncements(Player player, String[] args) {
        if (receivingAnnouncements.contains(player.getUniqueId())) {
            receivingAnnouncements.remove(player.getUniqueId());
            MessageFormatter.sendSuccessMessage(player, "LB announcements have been enabled.");
        } else {
            receivingAnnouncements.add(player.getUniqueId());
            MessageFormatter.sendSuccessMessage(player, "LB announcements have been disabled.");
        }
    }
}

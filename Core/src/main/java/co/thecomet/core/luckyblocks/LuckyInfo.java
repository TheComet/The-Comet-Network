package co.thecomet.core.luckyblocks;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface LuckyInfo {
    String name();

    LuckyType type();

    double dropChance() default 0.05;
    
    boolean uniqueRegister() default false;
}

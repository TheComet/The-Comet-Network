package co.thecomet.core.luckyblocks;

import org.bukkit.Location;
import org.bukkit.Material;

public class TriggerBlock {
    private final Material material;
    private final LuckyType luckyType;
    private byte data = 0;

    public TriggerBlock(Material material, LuckyType luckyType) {
        this.material = material;
        this.luckyType = luckyType;
    }

    public TriggerBlock setData(byte b) {
        this.data = b;
        return this;
    }

    public void set(Location loc) {
        loc.getBlock().setType(material);
        loc.getBlock().setData(data);
    }

    public Material getMaterial() {
        return material;
    }

    public LuckyType getLuckyType() {
        return luckyType;
    }

    public byte getData() {
        return data;
    }
}

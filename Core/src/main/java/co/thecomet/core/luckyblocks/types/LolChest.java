package co.thecomet.core.luckyblocks.types;

import co.thecomet.common.chat.FontColor;
import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Wool;

@LuckyInfo(name = "Lol Chest", type = LuckyType.NORMAL, dropChance = 0.1)
public class LolChest extends LuckyBlock {
    @Override
    public boolean canSpawn(Location loc, Player whoBroke) {
        Block above = loc.add(0, 1, 0).getBlock();

        return above.getType() == Material.AIR || above.isLiquid();
    }

    @Override
    public void onBreak(Block block, Player player) {
        block.setType(Material.CHEST);
        Chest chest = ((Chest) block.getState());
        Inventory inv = chest.getBlockInventory();

        //L
        inv.setItem(0, randomWool());
        inv.setItem(9, randomWool());
        inv.setItem(18, randomWool());
        inv.setItem(19, randomWool());

        //O
        inv.setItem(3, randomWool());
        inv.setItem(4, randomWool());
        inv.setItem(5, randomWool());
        inv.setItem(12, randomWool());
        inv.setItem(14, randomWool());
        inv.setItem(21, randomWool());
        inv.setItem(22, randomWool());
        inv.setItem(23, randomWool());

        //L
        inv.setItem(7, randomWool());
        inv.setItem(16, randomWool());
        inv.setItem(25, randomWool());
        inv.setItem(26, randomWool());
    }

    private ItemStack randomWool() {
        Wool wool = new Wool(DyeColor.values()[(int) (Math.random() * DyeColor.values().length)]);
        ItemStack is = wool.toItemStack();
        is.setAmount(1); //To prevent the "0" next to the items
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(FontColor.style("LOL").toRainbow(false));
        is.setItemMeta(im);
        return is;
    }
}

package co.thecomet.core.config;

import co.thecomet.common.config.JsonConfig;

public class CoreConfig extends JsonConfig {
    public boolean importConvertedData = false;
    public boolean vipOnly = false;
}

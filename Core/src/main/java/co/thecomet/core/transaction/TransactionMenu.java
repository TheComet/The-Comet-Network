package co.thecomet.core.transaction;

import co.thecomet.data.SessionAPI;
import co.thecomet.common.chat.FontColor;
import co.thecomet.core.CoreAPI;
import co.thecomet.data.DAOManager;
import co.thecomet.data.transaction.CurrencyType;
import co.thecomet.data.transaction.Transaction;
import co.thecomet.data.user.INetworkUser;
import co.thecomet.data.user.User;
import co.thecomet.core.ui.Menu;
import co.thecomet.core.ui.MenuItem;
import co.thecomet.core.utils.MessageFormatter;
import org.bson.types.ObjectId;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.UpdateOperations;

public class TransactionMenu extends Menu {
    private Transaction transaction;
    private MenuItem approve;
    private MenuItem filler;
    private MenuItem cancel;
    
    public TransactionMenu(Player player, final Transaction transaction) {
        super("TRAN:" + TransactionManager.transactionSessionId++, 6);
        this.transaction = transaction;

        final INetworkUser u = SessionAPI.getUser(player.getUniqueId());
        u.getTransactions().add(transaction);

        this.setExitOnClickOutside(false);
        this.setMenuCloseBehavior(p -> {
            if (transaction.isApproved()) {
                if (transaction.getFeature().getType() == CurrencyType.POINTS) {
                    u.setPoints(u.getPoints() - transaction.getFeature().getPoints());
                } else {
                    u.setPoints(u.getCoins() - transaction.getFeature().getPoints());
                }

                MessageFormatter.sendSuccessMessage(player, "Your purchase has been approved.");

                CoreAPI.async(() -> {
                    BasicDAO<User, ObjectId> dao = DAOManager.getDAO(User.class);
                    DAOManager.getDAO(Transaction.class).save(transaction);
                    UpdateOperations<User> ops = dao.createUpdateOperations();
                    ops.add("transactions", transaction);
                    ops.inc(transaction.getFeature().getType() == CurrencyType.POINTS ? "points" : "coins", -transaction.getFeature().getPoints());
                    dao.update(dao.createQuery().field("uuid").equal(u.getUuid().toString()), ops);
                    MessageFormatter.sendSuccessMessage(player, "Your purchase has been completed!");
                });
            } else {
                u.getTransactions().remove(transaction);
                MessageFormatter.sendGeneralMessage(player, "You have canceled the purchase.");
            }
        });
        
        approve = new MenuItem(FontColor.translateString("&aApprove Transaction"), new MaterialData(Material.STAINED_GLASS_PANE, DyeColor.LIME.getData())) {
            @Override
            public void onClick(Player player) {
                transaction.setApproved(true);
                closeMenu(player);
            }
        };

        filler = new MenuItem(FontColor.translateString("&a<-- Approve &7| &cCancel -->"), new MaterialData(Material.STAINED_GLASS_PANE, DyeColor.BLACK.getData())) {
            @Override
            public void onClick(Player player) {}
        };

        cancel = new MenuItem(FontColor.translateString("&cCancel Transaction"), new MaterialData(Material.STAINED_GLASS_PANE, DyeColor.RED.getData())) {
            @Override
            public void onClick(Player player) {
                closeMenu(player);
            }
        };
        
        for (int x = 0; x < 6; x++) {
            for (int y = 0; y < 9; y++) {
                if (y < 4) {
                    addMenuItem(approve, y, x);
                } else if (y > 4) {
                    addMenuItem(cancel, y, x);
                } else {
                    addMenuItem(filler, y, x);
                }
            }
        }
    }
}

package co.thecomet.core.transaction;

import co.thecomet.data.SessionAPI;
import co.thecomet.core.CoreAPI;
import co.thecomet.data.DAOManager;
import co.thecomet.data.transaction.CurrencyType;
import co.thecomet.data.transaction.Feature;
import co.thecomet.data.transaction.Transaction;
import co.thecomet.data.user.INetworkUser;
import co.thecomet.data.user.User;
import co.thecomet.core.utils.MessageFormatter;
import org.bson.types.ObjectId;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.mongodb.morphia.dao.BasicDAO;

import java.util.ArrayList;
import java.util.List;

public class TransactionManager {
    protected static int transactionSessionId = 1;
    
    public static void submit(final Player player, final Feature feature) {
        final INetworkUser bp = SessionAPI.getUser(player.getUniqueId());
        
        if (bp == null) {
            return;
        }

        CoreAPI.async(() -> {
            BasicDAO<User, ObjectId> dao = DAOManager.getDAO(User.class);
            User user = dao.findOne(dao.createQuery().field("uuid").equal(bp.getUuid().toString()));
            
            if (user == null) {
                CoreAPI.getPlugin().getLogger().info("User is null...");
                return;
            }
            
            final int points = user.getPoints();
            final List<Transaction> transactions = user.getTransactions();
            Bukkit.getScheduler().scheduleSyncDelayedTask(CoreAPI.getPlugin(), () -> {
                if (bp.getPoints() != points) {
                    bp.setPoints(points);
                }
                
                if (bp.getTransactions().containsAll(transactions) == false) {
                    bp.setTransactions(transactions);
                }
                
                if (feature.isUnique()) {
                    new ArrayList<>(transactions).forEach(transaction -> {
                        if (transaction.getFeature().equals(feature)) {
                            MessageFormatter.sendGeneralMessage(player, "You have already purchased this feature!");
                            return;
                        }
                    });
                }

                if (feature.getType() == CurrencyType.POINTS) {
                    if (bp.getPoints() < feature.getPoints()) {
                        MessageFormatter.sendErrorMessage(player, "Insufficient points. You need " + feature.getPoints() + " to buy " + feature.getDisplay());
                        return;
                    }
                } else {
                    if (bp.getCoins() < feature.getPoints()) {
                        MessageFormatter.sendErrorMessage(player, "Insufficient coins. You need " + feature.getPoints() + " to buy " + feature.getDisplay());
                        return;
                    }
                }
                
                final Transaction transaction = new Transaction(feature, player.getUniqueId());
                TransactionMenu menu = new TransactionMenu(player, transaction);
                menu.openMenu(player);
            });
        });
    }
}

package co.thecomet.core.utils.items;

import co.thecomet.core.utils.ColorUtil;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.ArrayList;
import java.util.List;

public class ItemHelper {
    public static final List<Material> ARMOR = new ArrayList<Material>() {{
        add(Material.LEATHER_HELMET);
        add(Material.LEATHER_CHESTPLATE);
        add(Material.LEATHER_LEGGINGS);
        add(Material.LEATHER_BOOTS);
        add(Material.IRON_HELMET);
        add(Material.IRON_CHESTPLATE);
        add(Material.IRON_LEGGINGS);
        add(Material.IRON_BOOTS);
        add(Material.GOLD_HELMET);
        add(Material.GOLD_CHESTPLATE);
        add(Material.GOLD_LEGGINGS);
        add(Material.GOLD_BOOTS);
        add(Material.DIAMOND_HELMET);
        add(Material.DIAMOND_CHESTPLATE);
        add(Material.DIAMOND_LEGGINGS);
        add(Material.DIAMOND_BOOTS);
        add(Material.CHAINMAIL_HELMET);
        add(Material.CHAINMAIL_CHESTPLATE);
        add(Material.CHAINMAIL_LEGGINGS);
        add(Material.CHAINMAIL_BOOTS);
    }};
    
    public static boolean isArmor(ItemStack is) {
        return isArmor(is.getType());
    }
    
    public static boolean isArmor(Material mat) {
        if (ARMOR.contains(mat)) {
            return true;
        }
        
        return false;
    }
    
    public static void equipArmor(Player player, ItemStack is) {
        if (isArmor(is)) {
            String type = is.getType().name().split("_")[1];
            switch (type) {
                case "HELMET":
                    player.getEquipment().setHelmet(is);
                    return;
                case "CHESTPLATE":
                    player.getEquipment().setChestplate(is);
                    return;
                case "LEGGINGS":
                    player.getEquipment().setLeggings(is);
                    return;
                case "BOOTS":
                    player.getEquipment().setBoots(is);
                    return;
                default:
                    break;
            }
        }
        
        player.getInventory().addItem(is);
    }
    
    public static boolean setColor(ItemStack is, int red, int green, int blue) {
        return setColor(is, Color.fromRGB(red, green, blue));
    }
    
    public static boolean setColor(ItemStack is, Color color) {
        ItemMeta meta = is.getItemMeta();
        
        if (meta instanceof LeatherArmorMeta) {
            LeatherArmorMeta laMeta = (LeatherArmorMeta) meta;
            laMeta.setColor(color);
            is.setItemMeta(laMeta);
            return true;
        }
        
        return false;
    }
    
    public static boolean setColor(ItemStack is, float hue, float saturation, float brightness) {
        return setColor(is, ColorUtil.getColorFromHSB(hue, saturation, brightness));
    }
}

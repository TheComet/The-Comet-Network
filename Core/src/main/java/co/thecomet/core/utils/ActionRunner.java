package co.thecomet.core.utils;

import org.bukkit.entity.Player;

public abstract class ActionRunner {
    public boolean willDestroy() {
        return false;
    }
    
    public abstract void action(Player player);
}

package co.thecomet.core.utils.conversion;

import co.thecomet.common.config.JsonConfig;

import java.util.HashMap;
import java.util.Map;

public class ResultsConfig extends JsonConfig {
    public Map<String, String> results = new HashMap<>();
}

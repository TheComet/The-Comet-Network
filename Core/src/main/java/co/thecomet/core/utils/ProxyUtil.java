package co.thecomet.core.utils;

import com.archeinteractive.rc.redis.pubsub.NetTask;
import org.bukkit.entity.Player;

public class ProxyUtil {
    public static void send(Player player, String server) {
        NetTask.withName("send")
                .withArg("player", player.getName())
                .withArg("server", server)
                .send("send");
    }
}

package co.thecomet.core.utils.announcer;

import co.thecomet.data.ranks.Rank;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.CoreModule;
import co.thecomet.core.command.CommandRegistry;
import co.thecomet.core.module.ModuleManager;
import co.thecomet.core.utils.MessageFormatter;
import org.bukkit.command.CommandSender;

public class AnnouncerCommands {
    public AnnouncerCommands() {
        CommandRegistry.registerUniversalCommand(CoreAPI.getPlugin(), "announcer", Rank.ADMIN, AnnouncerCommands::announcer);
        CommandRegistry.registerUniversalSubCommand("announcer", "reload", Rank.ADMIN, AnnouncerCommands::reload);
        CommandRegistry.registerUniversalSubCommand("announcer", "start", Rank.ADMIN, AnnouncerCommands::start);
        CommandRegistry.registerUniversalSubCommand("announcer", "stop", Rank.ADMIN, AnnouncerCommands::stop);
    }

    public static void announcer(CommandSender sender, String[] args) {
        MessageFormatter.sendInfoMessage(sender, "/announcer reload");
        MessageFormatter.sendInfoMessage(sender, "/announcer start");
        MessageFormatter.sendInfoMessage(sender, "/announcer stop");
    }

    public static void reload(CommandSender sender, String[] args) {
        CoreModule module = ModuleManager.getModule(CoreModule.class);
        if (module != null) {
            module.loadAnnouncer();
            MessageFormatter.sendSuccessMessage(sender, "Announcer has been reloaded!");
            return;
        }

        MessageFormatter.sendErrorMessage(sender, "Announcer could not be reloaded!");
    }

    public static void start(CommandSender sender, String[] args) {
        CoreModule module = ModuleManager.getModule(CoreModule.class);
        if (module != null) {
            module.getAnnouncer().start();
            MessageFormatter.sendSuccessMessage(sender, "Announcer has been started!");
            return;
        }

        MessageFormatter.sendErrorMessage(sender, "Announcer could not be started!");
    }

    public static void stop(CommandSender sender, String[] args) {
        CoreModule module = ModuleManager.getModule(CoreModule.class);
        if (module != null) {
            module.getAnnouncer().stop();
            MessageFormatter.sendSuccessMessage(sender, "Announcer has been stopped!");
            return;
        }

        MessageFormatter.sendErrorMessage(sender, "Announcer could not be stopped!");
    }
}

package co.thecomet.core.utils.reflection;

import co.thecomet.common.reflection.CommonReflection;
import org.bukkit.entity.Player;

public class PlayerHandler {
    public static int getPing(Player player) {
        Object handle = HandleReflection.getHandle(player);
        return CommonReflection.getFieldValue(handle, "ping", int.class);
    }
}

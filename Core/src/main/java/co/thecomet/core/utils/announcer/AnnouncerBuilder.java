package co.thecomet.core.utils.announcer;

import co.thecomet.data.SessionAPI;
import co.thecomet.common.chat.FontColor;
import co.thecomet.core.CoreAPI;
import co.thecomet.data.user.INetworkUser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

public class AnnouncerBuilder {
    private Announcer announcer = new Announcer();

    public static AnnouncerBuilder newBuilder() {
        return new AnnouncerBuilder();
    }

    public AnnouncerBuilder withHeader(String ... header) {
        announcer.header = colorize(header);
        return this;
    }

    public AnnouncerBuilder withFooter(String ... footer) {
        announcer.footer = colorize(footer);
        return this;
    }

    public AnnouncerBuilder withWelcomeMessage(String ... message) {
        announcer.welcomeMessage = colorize(message);
        return this;
    }

    public AnnouncerBuilder withAnnouncement(String ... message) {
        announcer.announcements.add(colorize(message.clone()));
        return this;
    }

    public AnnouncerBuilder withAnnouncements(List<String[]> messages) {
        for (String[] lines : messages) {
            withAnnouncement(lines);
        }
        return this;
    }

    public AnnouncerBuilder withInterval(int interval) {
        announcer.interval = interval;
        return this;
    }

    public AnnouncerBuilder fromConfig(AnnouncementConfig config) {
        AnnouncerBuilder builder = this;
        if (config.header != null && config.header.length > 0) {
            builder = builder.withHeader(config.header);
        }

        if (config.footer != null && config.footer.length > 0) {
            builder = builder.withFooter(config.footer);
        }

        if (config.welcomeMessage != null && config.welcomeMessage.length > 0) {
            builder = builder.withWelcomeMessage(config.welcomeMessage);
        }

        if (config.announcements != null && config.announcements.size() > 0) {
            builder = builder.withAnnouncements(config.announcements);
        }
        return builder;
    }

    public AnnouncerBuilder fromAnnouncer(Announcer announcer) {
        announcer = new Announcer(announcer);
        return this;
    }

    public Announcer build(boolean start) {
        announcer.start();
        return announcer;
    }

    private String[] colorize(String[] lines) {
        for (int i = 0; i < lines.length; i++) {
            lines[i] = FontColor.translateString(lines[i]);
        }

        return lines;
    }

    public class Announcer extends BukkitRunnable implements Listener {
        private boolean started = false;
        private String[] header = null;
        private String[] footer = null;
        private String[] welcomeMessage = null;
        private Deque<String[]> announcements = new ArrayDeque<>();
        private int interval = 20 * 60;

        public Announcer() {}

        public Announcer(Announcer announcer) {
            if (announcer.isStarted()) {
                announcer.stop();
            }

            this.header = announcer.header;
            this.footer = announcer.footer;
            this.welcomeMessage = announcer.welcomeMessage;
            this.announcements = announcer.announcements;
            this.interval = announcer.interval;
        }

        @Override
        public void run() {
            if (announcements.size() > 0) {
                for (Player player : Bukkit.getOnlinePlayers()) {
                    send(player, announcements.peek());
                }

                announcements.offer(announcements.poll());
            }
        }

        @EventHandler
        public void onPlayerJoin(PlayerJoinEvent event) {
            if (welcomeMessage != null && welcomeMessage.length > 0) {
                send(event.getPlayer(), welcomeMessage);
            }
        }

        public void send(Player player, final String[] message) {
            CoreAPI.async(() -> {
                if (player == null) {
                    return;
                }

                while (!SessionAPI.containsUser(player.getUniqueId())) {
                    if (!player.isOnline()) {
                        return;
                    }

                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                processMessage(player, message);
            });
        }

        private void processMessage(Player player, String[] message) {
            if (header != null && header.length > 0) {
                for (String line : header.clone()) {
                    processLine(player, line);
                }
            }

            for (String line : message.clone()) {
                processLine(player, line);
            }

            if (footer != null && footer.length > 0) {
                for (String line : footer.clone()) {
                    processLine(player, line);
                }
            }
        }

        private void processLine(Player player, String line) {
            INetworkUser u = SessionAPI.getUser(player.getUniqueId());
            String out = line.replace("%player%", u.getRank().getRankColor() + player.getName() + FontColor.RESET);
            player.sendMessage(out);
        }

        public void start() {
            if (!started) {
                this.runTaskTimer(CoreAPI.getPlugin(), interval, interval);
                Bukkit.getPluginManager().registerEvents(this, CoreAPI.getPlugin());
            }
        }

        public void stop() {
            cancel();
            HandlerList.unregisterAll(this);
        }

        public boolean isStarted() {
            return started;
        }
    }
}

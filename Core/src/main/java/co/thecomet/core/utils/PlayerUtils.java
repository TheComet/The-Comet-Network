package co.thecomet.core.utils;

import co.thecomet.common.reflection.CommonReflection;
import co.thecomet.common.reflection.MethodBuilder;
import co.thecomet.core.utils.reflection.EntityHandler;
import co.thecomet.core.utils.reflection.HandleReflection;
import co.thecomet.core.utils.reflection.VersionHandler;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class PlayerUtils {
    private static final Class<?> classEntityPlayer = VersionHandler.getNMSClass("EntityPlayer");
    private static final Field EntityPlayer_playerConnection = CommonReflection.getField(classEntityPlayer, "playerConnection");

    private static final Class<?> classPacket = VersionHandler.getNMSClass("PacketPlayInClientCommand");
    private static final Class<?> enumClientCommand = VersionHandler.getNMSClass("PacketPlayInClientCommand$EnumClientCommand");
    private static final Field PERFORM_RESPAWN = CommonReflection.getField(enumClientCommand, "PERFORM_RESPAWN");
    private static final Method packetMethod = CommonReflection.getMethod(EntityPlayer_playerConnection.getType(), "a", new Class<?>[]{classPacket});

    public static void clearArrowsFromPlayer(Player player) {
        Object watcher = EntityHandler.getWatcher(player);

        if (watcher == null) {
            return;
        }

        new MethodBuilder(watcher.getClass(), "watch", watcher, new Class<?>[] { int.class, Object.class }).invoke(9, (Byte) (byte) 0);
    }

    public static void respawn(Player player, Location spawn) {
        Location previous = player.getBedSpawnLocation();

        player.setBedSpawnLocation(spawn, true);
        respawn(player);
        player.teleport(spawn);

        // Reset their bed spawn
        player.setBedSpawnLocation(previous, true);
    }

    public static void respawn(Player player) {
        try {
            Object packet = classPacket.getConstructor(enumClientCommand).newInstance(PERFORM_RESPAWN.get(null));
            Object nmsplayer = HandleReflection.getHandle(player);
            packetMethod.invoke(EntityPlayer_playerConnection.get(nmsplayer), packet);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

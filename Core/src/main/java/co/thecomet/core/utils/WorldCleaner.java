package co.thecomet.core.utils;

import co.thecomet.core.CoreAPI;
import co.thecomet.core.utils.reflection.HandleReflection;
import co.thecomet.core.utils.reflection.VersionHandler;
import com.google.common.collect.Lists;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permissible;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.SimplePluginManager;

import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class WorldCleaner {
    // Field reflection
    private static Field modifiers;
    
    // WorldServer reflection
    private static Class worldServerClass = VersionHandler.getNMSClass("WorldServer");
    private static Field chunkProviderServer;

    // ChunkProviderServer reflection
    private static Class chunkProviderServerClass = VersionHandler.getNMSClass("ChunkProviderServer");
    private static Field unloadQueue;
    private static Field emptyChunk;
    private static Field chunks;
    private static Field chunkProviderServerWorld;
    
    // Chunk reflection
    private static Class chunkClass = VersionHandler.getNMSClass("Chunk");
    private static Field chunkWorld;
    private static Field tileEntities;
    private static Field bukkitChunk;

    // TileEntity reflection
    private static Class tileEntityClass = VersionHandler.getNMSClass("TileEntity");
    private static Field tileEntityWorld;
    
    // CraftChunk reflection
    private static Class craftChunkClass = VersionHandler.getOBCClass("CraftChunk");
    private static Field craftChunkWeakChunk;
    private static Field craftChunkWorldServer;

    // BlockRedstoneTorch reflection
    private static Class blockRestoneTorchClass = VersionHandler.getNMSClass("BlockRedstoneTorch");
    private static Field bWorldMap;

    // CraftEntity reflection
    private static Class craftEntityClass = VersionHandler.getOBCClass("entity.CraftEntity");
    private static Field lastDamageEvent;
    
    // CraftingManager reflection
    private static Class craftingManagerClass = VersionHandler.getNMSClass("CraftingManager");
    private static Method getCraftingManagerInstance;
    private static Field lastCraftView;

    // EnchantmentManager reflection
    private static Class enchantmentManagerClass = VersionHandler.getNMSClass("EnchantmentManager");
    private static Class enchantmentModifierProtectionClass = VersionHandler.getNMSClass("EnchantmentManager$EnchantmentModifierProtection");
    private static Constructor enchantmentModifierProtectionConstructor;
    private static Field protectionModifier;
    private static Class enchantmentModifierDamageClass = VersionHandler.getNMSClass("EnchantmentManager$EnchantmentModifierDamage");
    private static Constructor enchantmentModifierDamageConstructor;
    private static Field damageModifier;
    private static Class enchantmentModifierThornsClass = VersionHandler.getNMSClass("EnchantmentManager$EnchantmentModifierThorns");
    private static Constructor enchantmentModifierThornsConstructor;
    private static Field thornsModifier;
    private static Class enchantmentModifierArthropodsClass = VersionHandler.getNMSClass("EnchantmentManager$EnchantmentModifierArthropods");
    private static Constructor enchantmentModifierArthropodsConstructor;
    private static Field arthropodsModifier;
    
    // BiomeBase reflection
    private static Class biomeBaseClass = VersionHandler.getNMSClass("BiomeBase");
    private static List<Field> biomes = new ArrayList<>();
    private static Field worldGenBigTree;
    private static Class worldGenBigTreeClass = VersionHandler.getNMSClass("WorldGenBigTree");
    private static Field worldGenBigTreeWorld;
            
    static {
        try {
            modifiers = Field.class.getDeclaredField("modifiers");
            modifiers.setAccessible(true);

            chunkProviderServer = worldServerClass.getDeclaredField("chunkProviderServer");

            unloadQueue = chunkProviderServerClass.getField("unloadQueue");
            emptyChunk = chunkProviderServerClass.getField("emptyChunk");
            chunks = chunkProviderServerClass.getField("chunks");
            chunkProviderServerWorld = chunkProviderServerClass.getField("world");

            chunkWorld = chunkClass.getField("world");
            chunkWorld.setAccessible(true);
            tileEntities = chunkClass.getField("tileEntities");
            tileEntities.setAccessible(true);
            bukkitChunk = chunkClass.getField("bukkitChunk");

            tileEntityWorld = tileEntityClass.getDeclaredField("world");
            tileEntityWorld.setAccessible(true);
            
            craftChunkWeakChunk = craftChunkClass.getDeclaredField("weakChunk");
            craftChunkWeakChunk.setAccessible(true);
            craftChunkWorldServer = craftChunkClass.getDeclaredField("worldServer");
            craftChunkWorldServer.setAccessible(true);
            
            bWorldMap = blockRestoneTorchClass.getDeclaredField("b");
            bWorldMap.setAccessible(true);
            
            lastDamageEvent = craftEntityClass.getDeclaredField("lastDamageEvent");
            lastDamageEvent.setAccessible(true);
            
            getCraftingManagerInstance = craftingManagerClass.getDeclaredMethod("getInstance");
            lastCraftView = craftingManagerClass.getField("lastCraftView");
            
            enchantmentModifierProtectionConstructor = enchantmentModifierProtectionClass.getDeclaredConstructor();
            enchantmentModifierProtectionConstructor.setAccessible(true);
            protectionModifier = enchantmentManagerClass.getDeclaredField("b");
            protectionModifier.setAccessible(true);
            modifiers.setInt(protectionModifier, protectionModifier.getModifiers() & ~Modifier.FINAL);
            enchantmentModifierDamageConstructor = enchantmentModifierDamageClass.getDeclaredConstructor();
            enchantmentModifierDamageConstructor.setAccessible(true);
            damageModifier = enchantmentManagerClass.getDeclaredField("c");
            damageModifier.setAccessible(true);
            modifiers.setInt(damageModifier, damageModifier.getModifiers() & ~Modifier.FINAL);
            enchantmentModifierThornsConstructor = enchantmentModifierThornsClass.getDeclaredConstructor();
            enchantmentModifierThornsConstructor.setAccessible(true);
            thornsModifier = enchantmentManagerClass.getDeclaredField("d");
            thornsModifier.setAccessible(true);
            modifiers.setInt(thornsModifier, thornsModifier.getModifiers() & ~Modifier.FINAL);
            enchantmentModifierArthropodsConstructor = enchantmentModifierArthropodsClass.getDeclaredConstructor();
            enchantmentModifierArthropodsConstructor.setAccessible(true);
            arthropodsModifier = enchantmentManagerClass.getDeclaredField("e");
            arthropodsModifier.setAccessible(true);
            modifiers.setInt(arthropodsModifier, arthropodsModifier.getModifiers() & ~Modifier.FINAL);
            
            for (Field field : biomeBaseClass.getDeclaredFields()) {
                if (field.getType().isAssignableFrom(biomeBaseClass)) {
                    biomes.add(field);
                }
            }
            worldGenBigTree = biomeBaseClass.getDeclaredField("aB");
            worldGenBigTree.setAccessible(true);
            worldGenBigTreeWorld = worldGenBigTreeClass.getDeclaredField("l");
            worldGenBigTreeWorld.setAccessible(true);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static void unload(World world, boolean save) {
        world.getEntitiesByClass(Player.class).forEach(p -> p.kickPlayer("Sorry, the world you were in was unloaded!"));
        world.getEntities().forEach(e -> {
            if (e instanceof Player == false) {
                e.remove();
            }
        });

        Object chunkProviderServer = getChunkProviderServer(world);
        Bukkit.unloadWorld(world, save);
        clearBlockCommandSenderPermissibles(world);
        clearWorldChunkProviderServer(chunkProviderServer);
        
        Bukkit.getOnlinePlayers().forEach(p -> clearPlayer(p));
        
        try {
            clearCraftManager();
            clearEnchantmentManager();
            cleanBiomes(world);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }

    private static Object getChunkProviderServer(World world) {
        Object handle = HandleReflection.getHandle(world);

        try {
            if (chunkProviderServer != null) {
                return chunkProviderServer.get(handle);
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static void clearWorldChunkProviderServer(Object chunkProviderServer) {
        if (chunkProviderServer != null) {
            try {
                unloadQueue.set(chunkProviderServer, null);
                emptyChunk.set(chunkProviderServer, null);

                Object chunkMap = chunks.get(chunkProviderServer);
                Method entrySet = chunkMap.getClass().getMethod("entrySet");
                Method remove = chunkMap.getClass().getMethod("remove", long.class);
                Set<Map.Entry> chunkEntrySet = (Set<Map.Entry>) entrySet.invoke(chunkMap);
                for (Map.Entry entry : chunkEntrySet) {
                    Object chunk = entry.getValue();
                    remove.invoke(chunkMap, (Long) entry.getKey());

                    chunkWorld.set(chunk, null);

                    Set<Map.Entry> tileEntityMap = ((Map) tileEntities.get(chunk)).entrySet();
                    for (Map.Entry entry2 : new ArrayList<>(tileEntityMap)) {
                        tileEntityWorld.set(entry2.getValue(), null);
                    }
                    tileEntities.set(chunk, null);

                    Object craftChunk = bukkitChunk.get(chunk);
                    clearCraftChunk(craftChunk);
                    bukkitChunk.set(chunk, null);
                }

                chunks.set(chunkProviderServer, null);
                chunkProviderServerWorld.set(chunkProviderServer, null);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    private static void clearCraftChunk(Object chunk) {
        try {
            craftChunkWeakChunk.set(chunk, null);
            craftChunkWorldServer.set(chunk, null);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private static void clearBlockCommandSenderPermissibles(World world) {
        SimplePluginManager spm = (SimplePluginManager) Bukkit.getPluginManager();
        Field permSubsField = null;
        Field defSubsField = null;
        Map<String, Map<Permissible, Boolean>> permSubs;
        Map<Boolean, Map<Permissible, Boolean>> defSubs;
        
        try {
            permSubsField = spm.getClass().getDeclaredField("permSubs");
            defSubsField = spm.getClass().getDeclaredField("defSubs");

            if (permSubsField != null) {
                permSubsField.setAccessible(true);
                permSubs = (Map<String, Map<Permissible, Boolean>>) permSubsField.get(spm);

                for (Map.Entry<String, Map<Permissible, Boolean>> entry : new ArrayList<>(permSubs.entrySet())) {
                    for (Permissible permissible : new ArrayList<>(entry.getValue().keySet())) {
                        if (permissible instanceof BlockCommandSender) {
                            BlockCommandSender blockCommandSender = (BlockCommandSender) permissible;
                            if (blockCommandSender.getBlock().getWorld() == world) {
                                permSubs.get(entry.getKey()).remove(permissible);
                            }
                        }
                    }
                }
                
                permSubsField.set(spm, permSubs);
            }

            if (defSubsField != null) {
                defSubsField.setAccessible(true);
                defSubs = (Map<Boolean, Map<Permissible, Boolean>>) defSubsField.get(spm);

                for (Map.Entry<Boolean, Map<Permissible, Boolean>> entry : new ArrayList<>(defSubs.entrySet())) {
                    for (Permissible permissible : new ArrayList<>(entry.getValue().keySet())) {
                        if (permissible instanceof BlockCommandSender) {
                            BlockCommandSender blockCommandSender = (BlockCommandSender) permissible;
                            if (blockCommandSender.getBlock().getWorld() == world) {
                                defSubs.get(entry.getKey()).remove(permissible);
                            }
                        }
                    }
                }
                
                defSubsField.set(spm, defSubs);
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    
    private static void clearCraftManager() throws InvocationTargetException, IllegalAccessException {
        Object craftingManager = getCraftingManagerInstance.invoke(null);
        lastCraftView.set(craftingManager, null);
    }
    
    private static void clearEnchantmentManager() throws IllegalAccessException, InstantiationException, InvocationTargetException {
        protectionModifier.set(null, enchantmentModifierProtectionConstructor.newInstance());
        damageModifier.set(null, enchantmentModifierDamageConstructor.newInstance());
        thornsModifier.set(null, enchantmentModifierThornsConstructor.newInstance());
        arthropodsModifier.set(null, enchantmentModifierArthropodsConstructor.newInstance());
    }
    
    private static void cleanBiomes(World world) throws IllegalAccessException {
        Object handle = HandleReflection.getHandle(world);
        for (Field field : biomes) {
            Object biome = field.get(null);
            Object aB = worldGenBigTree.get(biome);
            
            if (aB != null) {
                Object w = worldGenBigTreeWorld.get(aB);
                if (w == handle) {
                    worldGenBigTreeWorld.set(aB, null);
                }
            }
        }
    }

    public static void clearPlayer(Player player) {
        try {
            clearLastDamageEvent(player);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    
    private static void clearLastDamageEvent(Player player) throws IllegalAccessException {
        lastDamageEvent.set(player, null);
    }

    private static List<Field> getObjectFields(Object obj) {
        return Lists.newArrayList(obj.getClass().getDeclaredFields());
    }
    
    private static void log(String message) {
        CoreAPI.getPlugin().getLogger().info(message);
    }
}

package co.thecomet.core.utils.conversion;

import co.thecomet.data.SessionAPI;
import co.thecomet.common.config.JsonConfig;
import co.thecomet.data.DAOManager;
import co.thecomet.data.DataAPI;
import co.thecomet.data.ranks.Rank;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.module.Module;
import co.thecomet.core.module.ModuleInfo;
import co.thecomet.core.module.ModuleManager;
import co.thecomet.common.utils.NameFetcher;
import co.thecomet.data.user.INetworkUser;
import co.thecomet.data.user.User;
import com.google.common.collect.Lists;
import org.bukkit.Bukkit;

import java.io.File;
import java.util.UUID;

@ModuleInfo(name = "importer")
public class ImporterModule extends Module {
    private ResultsConfig config = JsonConfig.load(new File(CoreAPI.getPlugin().getDataFolder(), "imports.json"), ResultsConfig.class);

    public void onEnable() {
        port();
        Bukkit.getScheduler().runTaskLater(CoreAPI.getPlugin(), () -> ModuleManager.unregisterModule(this), 20 * 60 * 5);
    }

    private void port() {
        for (String uuid : config.results.keySet()) {
            UUID u;
            try {
                u = UUID.fromString(uuid);
            } catch (Exception e) {
                continue;
            }
            CoreAPI.async(() -> process(u, Rank.getRankFromString(config.results.get(uuid))));
        }
    }

    private void process(final UUID uuid, final Rank rank) {
        INetworkUser u = SessionAPI.getUser(uuid);

        if (u != null) {
            return;
        }

        User user = DataAPI.retrieveUser(uuid);
        if (user != null) {
            return;
        }

        user = new User(uuid.toString());
        String name;
        try {
            name = new NameFetcher(Lists.newArrayList(uuid)).call().get(uuid);
        } catch (Exception e) {
            return;
        }

        user.setName(name);
        user.setNameLower(name);
        user.setRank(rank);

        DAOManager.getDAO(User.class).save(user);
    }
}

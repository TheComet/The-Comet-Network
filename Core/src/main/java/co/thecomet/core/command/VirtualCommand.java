package co.thecomet.core.command;

import co.thecomet.data.SessionAPI;
import co.thecomet.data.ranks.Rank;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.data.user.INetworkUser;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * A Class for representing a VirtualCommand, which is registered from an
 * implementation of the functional interfaces
 */
class VirtualCommand {

    final Class<? extends JavaPlugin> plugincls;
    final Map<String, VirtualSubCommand> subcommands;
    CommandAction<? super Player> player;
    CommandAction<? super ConsoleCommandSender> console;
    Rank rank;

    VirtualCommand(Class<? extends JavaPlugin> plugincls) {
        this.plugincls = plugincls;
        this.subcommands = new HashMap<>();
        this.player = null;
        this.console = null;
        this.rank = Rank.DEFAULT;
    }

    VirtualCommand(Class<? extends JavaPlugin> plugincls, Rank rank) {
        this(plugincls);
        this.rank = rank;
    }

    boolean invokeConsole(ConsoleCommandSender sender, String[] args) {
        VirtualSubCommand s;

        if (args.length < 1 || (s = getSubCommand(args[0])) == null) {
            if (hasConsoleExecution()) {
                console.invoke(sender, args);
                return true;
            }

            return false;
        }

        if (s.hasConsoleExecution()) {
            s.console.invoke(sender, Arrays.copyOfRange(args, 1, args.length));
            return true;
        }

        return false;
    }

    boolean invokePlayer(Player sender, String[] args) {
        VirtualSubCommand s;

        INetworkUser user = SessionAPI.getUser(sender.getUniqueId());

        if (user == null) {
            return false;
        }

        if (args.length < 1 || (s = getSubCommand(args[0])) == null) {
            if (hasPlayerExecution()) {
                if (user.getRank().ordinal() >= rank.ordinal()) {
                    player.invoke(sender, args);
                } else {
                    MessageFormatter.sendErrorMessage(sender, "You do not have permission to use this command.");
                }
                return true;
            }

            return false;
        }

        if (s.hasPlayerExecution()) {
            if (user.getRank().ordinal() >= s.rank.ordinal()) {
                s.player.invoke(sender, Arrays.copyOfRange(args, 1, args.length));
            } else {
                MessageFormatter.sendErrorMessage(sender, "You do not have permission to use this command.");
            }
            return true;
        }

        return false;
    }

    public String toString() {
        return String.format("VirtualCommand{plugin=%s,p exec=%s, c exec=%s}",
                plugincls.getSimpleName(), hasPlayerExecution(), hasConsoleExecution());
    }

    boolean hasPlayerExecution() {
        return player != null;
    }

    boolean hasConsoleExecution() {
        return console != null;
    }

    VirtualSubCommand getSubCommand(String sub_label) {
        for (Map.Entry<String, VirtualSubCommand> entry : subcommands.entrySet()) {
            if (sub_label.equalsIgnoreCase(entry.getKey())) {
                return entry.getValue();
            }
        }

        return null;
    }

    class VirtualSubCommand {

        CommandAction<? super Player> player;
        CommandAction<? super ConsoleCommandSender> console;
        Rank rank;

        public VirtualSubCommand() {
            this.rank = Rank.DEFAULT;
        }

        public VirtualSubCommand(Rank rank) {
            this.rank = rank;
        }

        boolean hasPlayerExecution() {
            return player != null;
        }

        boolean hasConsoleExecution() {
            return console != null;
        }
    }
}

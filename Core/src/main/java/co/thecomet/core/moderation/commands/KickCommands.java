package co.thecomet.core.moderation.commands;

import co.thecomet.data.DataAPI;
import co.thecomet.data.moderation.ModerationAction;
import co.thecomet.data.moderation.ModerationEntry;
import co.thecomet.data.ranks.Rank;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.command.CommandRegistry;
import co.thecomet.core.utils.MessageFormatter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class KickCommands {
    public KickCommands() {
        CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "kick", Rank.FRIEND, KickCommands::kick);
    }

    public static void kick(Player sender, String[] args) {
        if (args.length < 2) {
            MessageFormatter.sendUsageMessage(sender, "/kick <player> <reason>");
            return;
        }

        String player = args[0];
        String reason = args[1];

        if (args.length > 2) {
            for (int i = 2; i < args.length; i++) {
                reason += " " + args[i];
            }
        }

        Player target = Bukkit.getPlayer(player);
        if (target.isOnline()) {
            target.kickPlayer(ChatColor.translateAlternateColorCodes('&', "&cYou have been kicked for: &6" + reason));
        } else {
            return;
        }

        DataAPI.addModerationEntry(new ModerationEntry(target.getUniqueId().toString(), sender.getUniqueId().toString(), ModerationAction.KICK, reason));
        for (Player p : Bukkit.getOnlinePlayers()) {
            MessageFormatter.sendInfoMessage(p, player + " &cwas kicked by &e&l" + sender.getName(), "&cfor &6&l" + reason);
        }
    }
}

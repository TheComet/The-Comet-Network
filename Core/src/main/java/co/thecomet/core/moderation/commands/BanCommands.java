package co.thecomet.core.moderation.commands;

import co.thecomet.data.SessionAPI;
import co.thecomet.data.DAOManager;
import co.thecomet.data.DataAPI;
import co.thecomet.data.moderation.ModerationAction;
import co.thecomet.data.moderation.ModerationEntry;
import co.thecomet.data.ranks.Rank;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.command.CommandRegistry;
import co.thecomet.data.user.INetworkUser;
import co.thecomet.data.user.User;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.core.utils.TimeFormatUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class BanCommands {
    public BanCommands() {
        CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "ban", Rank.MOD, BanCommands::ban);
        CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "tempban", Rank.FRIEND, BanCommands::tempban);
        CommandRegistry.registerUniversalCommand(CoreAPI.getPlugin(), "unban", Rank.ADMIN, BanCommands::unban);
        CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "ipban", Rank.FRIEND, BanCommands::ipban);
        CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "ipunban", Rank.ADMIN, BanCommands::ipunban);
    }

    public static void ban(Player sender, String[] args) {
        if (args.length < 2) {
            MessageFormatter.sendUsageMessage(sender, "/ban <player> <reason>");
            return;
        }

        String player = args[0];
        String reason = args[1];

        if (args.length > 2) {
            for (int i = 2; i < args.length; i++) {
                reason += " " + args[i];
            }
        }

        Player target = Bukkit.getPlayer(player);
        User user = target != null ? DataAPI.retrieveUser(target.getUniqueId()) : DataAPI.retrieveUserByName(player);

        if (user == null) {
            MessageFormatter.sendErrorMessage(sender, "Unable to find user in the database. Is the name correct?");
            return;
        }

        if (user.getRank().ordinal() >= Rank.HELPER.ordinal()) {
            MessageFormatter.sendErrorMessage(sender, "You cannot ban staff members!");
            return;
        }

        if (target != null) {
            target.kickPlayer(ChatColor.translateAlternateColorCodes('&', "&cYou have been banned for: &6" + reason));
        }

        DataAPI.addModerationEntry(new ModerationEntry(user.getUuid(), sender.getUniqueId().toString(), ModerationAction.BAN, reason));
        for (Player p : Bukkit.getOnlinePlayers()) {
            MessageFormatter.sendInfoMessage(p, player + " &cwas banned by &e&l" + sender.getName(), "&cfor &6&l" + reason);
        }
    }

    public static void tempban(Player sender, String[] args) {
        if (args.length < 3) {
            MessageFormatter.sendUsageMessage(sender, "/tempban <player> <#m|#h|#d> <reason>");
            return;
        }

        String player = args[0];
        String time = args[1];

        String reason = args[2];

        if (args.length > 3) {
            for (int i = 3; i < args.length; i++) {
                reason += " " + args[i];
            }
        }

        Player target = Bukkit.getPlayer(player);
        User user = target != null ? DataAPI.retrieveUser(target.getUniqueId()) : DataAPI.retrieveUserByName(player);

        if (user == null) {
            MessageFormatter.sendErrorMessage(sender, "Unable to find user in the database. Is the name correct?");
            return;
        }

        if (user.getRank().ordinal() >= Rank.HELPER.ordinal()) {
            MessageFormatter.sendErrorMessage(sender, "You cannot ban staff members!");
            return;
        }

        if (target != null) {
            target.kickPlayer(ChatColor.translateAlternateColorCodes('&', "&cYou have been tempbanned for: &6" + reason));
        }

        try {
            DataAPI.addModerationEntry(new ModerationEntry(user.getUuid(), sender.getUniqueId().toString(), ModerationAction.BAN, reason, TimeFormatUtils.getTime(time).longValue()));
        } catch (NumberFormatException e) {
            MessageFormatter.sendUsageMessage(sender, "/ban <player> <#m|#h|#d> <reason>");
            return;
        }

        for (Player p : Bukkit.getOnlinePlayers()) {
            MessageFormatter.sendInfoMessage(p, player + " &cwas tempbanned by &e&l" + sender.getName(), "&cfor &6&l" + reason + " &7(" + time + ")");
        }
    }

    public static void unban(CommandSender sender, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(sender, "/unban <player>");
            return;
        }

        String player = args[0];

        Player target = Bukkit.getPlayer(player);
        User user = target != null ? DataAPI.retrieveUser(target.getUniqueId()) : DataAPI.retrieveUserByName(player);

        if (user == null) {
            MessageFormatter.sendErrorMessage(sender, "Unable to find user in the database. Is the name correct?");
            return;
        }

        user.getModerationHistory().setActiveBan(null);
        DAOManager.getDAO(User.class).save(user);
        MessageFormatter.sendSuccessMessage(sender, ChatColor.GOLD + player + " &4has been unbanned.");
    }

    public static void ipban(Player sender, String[] args) {
        if (args.length < 2) {
            MessageFormatter.sendUsageMessage(sender, "/ipban <player>");
            return;
        }

        String player = args[0];
        String reason = args[1];

        if (args.length > 2) {
            for (int i = 2; i < args.length; i++) {
                reason += " " + args[i];
            }
        }

        Player target = Bukkit.getPlayer(player);
        User user = target != null ? DataAPI.retrieveUser(target.getUniqueId()) : DataAPI.retrieveUserByName(player);

        if (user == null) {
            MessageFormatter.sendErrorMessage(sender, "Unable to find user in the database. Is the name correct?");
            return;
        }

        if (user.getRank().ordinal() >= Rank.HELPER.ordinal()) {
            MessageFormatter.sendErrorMessage(sender, "You cannot ban staff members!");
            return;
        }

        boolean ipBanned = false;
        List<String> bannedIps = new ArrayList<>();
        if (CoreAPI.getGlobalModerationData().ipBans.contains(user.getIp())) {
            ipBanned = true;
            bannedIps.add(user.getIp());
        }

        for (String ip : user.getIpHistory()) {
            if (CoreAPI.getGlobalModerationData().ipBans.contains(ip)) {
                ipBanned = true;
                bannedIps.add(ip);
            }
        }

        if (ipBanned) {
            MessageFormatter.sendErrorMessage(sender, "That user is already ip banned.");
            return;
        }

        CoreAPI.getGlobalModerationData().ipBans.addAll(bannedIps);
        if (target != null) {
            target.kickPlayer(ChatColor.translateAlternateColorCodes('&', "&cYou have been banned for: &6" + reason));
        }
        
        for (INetworkUser u : SessionAPI.getUsers()) {
            if (u.getUuid() != UUID.fromString(user.getUuid())) {
                ipBanned = false;
                if (CoreAPI.getGlobalModerationData().ipBans.contains(user.getIp())) {
                    ipBanned = true;
                }
                
                for (String ip : u.getIpHistory()) {
                    if (CoreAPI.getGlobalModerationData().ipBans.contains(ip)) {
                        ipBanned = true;
                    }
                }
                
                if (ipBanned) {
                    if (target != null) {
                        target.kickPlayer(ChatColor.translateAlternateColorCodes('&', "&cYou have been banned for: &6" + reason));
                    }
                }
            }
        }

        DataAPI.addModerationEntry(new ModerationEntry(user.getUuid(), sender.getUniqueId().toString(), ModerationAction.IPBAN, reason));
        DataAPI.banIps(bannedIps);
        for (Player p : Bukkit.getOnlinePlayers()) {
            MessageFormatter.sendInfoMessage(p, player + " &cwas banned by &e&l" + sender.getName(), "&cfor &6&l" + reason);
        }
    }

    public static void ipunban(CommandSender sender, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(sender, "/unban <player>");
            return;
        }

        String player = args[0];

        Player target = Bukkit.getPlayer(player);
        User user = target != null ? DataAPI.retrieveUser(target.getUniqueId()) : DataAPI.retrieveUserByName(player);

        if (user == null) {
            MessageFormatter.sendErrorMessage(sender, "Unable to find user in the database. Is the name correct?");
            return;
        }

        boolean ipBanned = false;
        List<String> bannedIps = new ArrayList<>();
        if (CoreAPI.getGlobalModerationData().ipBans.contains(user.getIp())) {
            ipBanned = true;
            bannedIps.add(user.getIp());
        }

        for (String ip : user.getIpHistory()) {
            if (CoreAPI.getGlobalModerationData().ipBans.contains(ip)) {
                ipBanned = true;
                bannedIps.add(ip);
            }
        }
        
        if (ipBanned == false) {
            MessageFormatter.sendErrorMessage(sender, "That user is not ip banned.");
            return;
        }

        DataAPI.unbanIps(bannedIps);
        MessageFormatter.sendSuccessMessage(sender, ChatColor.GOLD + player + " &4is no longer ip banned.");
    }
}

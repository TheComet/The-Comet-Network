package co.thecomet.core.moderation.commands;

import co.thecomet.data.SessionAPI;
import co.thecomet.data.DataAPI;
import co.thecomet.data.ranks.Rank;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.command.CommandRegistry;
import co.thecomet.data.user.INetworkUser;
import co.thecomet.data.user.User;
import co.thecomet.core.utils.MessageFormatter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class StatCommands {
    public StatCommands() {
        CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "seen", Rank.ADMIN, StatCommands::seen);
        CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "ontime", Rank.MOD, StatCommands::onTime);
    }

    public static void seen(Player sender, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(sender, "/seen <name>");
            return;
        }

        final String name = args[0];
        Player player = Bukkit.getPlayer(name);
        if (player != null) {
            if (player != sender) {
                MessageFormatter.sendErrorMessage(sender, "That player is on the same server as you.");
            } else {
                MessageFormatter.sendErrorMessage(sender, "Go take a look in the mirror.");
            }
            return;
        }

        Bukkit.getScheduler().runTaskAsynchronously(CoreAPI.getPlugin(), () -> {
            User user = DataAPI.retrieveUserByName(name);
            if (user == null) {
                MessageFormatter.sendErrorMessage(sender, "Stats could not be retrieved for " + name);
                return;
            }

            if (user.getPlayTime() == 0) {
                MessageFormatter.sendErrorMessage(sender, ChatColor.GOLD + name + " &chas yet to play on the network.");
                return;
            }

            MessageFormatter.sendInfoMessage(sender, name + " was last online on " + formatDate(user.getLastOnline()), "They were last seen on " + user.getLastServer() + " with ip " + user.getIp());
        });
    }

    public static void onTime(Player sender, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(sender, "/ontime <name>");
            return;
        }

        final String name = args[0];
        Player player = Bukkit.getPlayer(name);
        if (player != null) {
            INetworkUser u = SessionAPI.getUser(player.getUniqueId());
            
            if (u != null) {
                long playTime = u.getPlayTime() + (System.currentTimeMillis() - u.getLastOnline());
                MessageFormatter.sendInfoMessage(sender, name + "'s total time on the network: " + formatTime(playTime));
            }
            
            return;
        }

        Bukkit.getScheduler().runTaskAsynchronously(CoreAPI.getPlugin(), () -> {
            User user = DataAPI.retrieveUserByName(name);
            if (user == null) {
                MessageFormatter.sendErrorMessage(sender, "Stats could not be retrieved for " + name);
                return;
            }
            MessageFormatter.sendInfoMessage(sender, name + "'s total time on the network: " + formatTime(user.getPlayTime()));
        });
    }

    public static String formatDate(long time) {
        final Date date = new Date(time);
        final String LEGACY_FORMAT = "EEE MMM dd hh:mm:ss zzz yyyy";
        final SimpleDateFormat sdf = new SimpleDateFormat(LEGACY_FORMAT);
        final TimeZone utc = TimeZone.getTimeZone("EST");
        sdf.setTimeZone(utc);
        return sdf.format(date);
    }

    public static String formatTime(long playTime) {
        return String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes(playTime), TimeUnit.MILLISECONDS.toSeconds(playTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(playTime)));
    }
}

package co.thecomet.core.moderation.commands;

import co.thecomet.data.ranks.Rank;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.command.CommandRegistry;
import co.thecomet.core.utils.MessageFormatter;
import net.jodah.expiringmap.ExpiringMap;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class TeleportCommands implements Listener {
    private static Map<UUID, TeleportEntries> map = new HashMap<>();
    
    public TeleportCommands(boolean tp, Rank tpRank, boolean tphere, Rank tphereRank, boolean tpa, Rank tpaRank, boolean tpahere, Rank tpahereRank, boolean tpall, Rank tpallRank) {
        boolean requestingEnabled = false;
        if (tp) {
            CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "tp", tpRank, TeleportCommands::tp);
        }

        if (tphere) {
            CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "tphere", tphereRank, TeleportCommands::tphere);
        }

        if (tpa) {
            CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "tpa", tpaRank, TeleportCommands::tpa);
            requestingEnabled = true;
        }

        if (tpahere) {
            CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "tpahere", tpahereRank, TeleportCommands::tpahere);
            requestingEnabled = true;
        }
        
        if (tpall) {
            CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "tpall", tpallRank, TeleportCommands::tpall);
        }
        
        if (requestingEnabled) {
            CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "tpaccept", tpaRank.ordinal() > tpahereRank.ordinal() ? tpaRank : tpahereRank, TeleportCommands::tpaccept);
            CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "tpdeny", tpaRank.ordinal() > tpahereRank.ordinal() ? tpaRank : tpahereRank, TeleportCommands::tpdeny);
        }
    }
    
    public static void tp(Player player, String[] args) {
        Player target;
        Player teleporting;
        
        if (args.length >= 2) {
            target = Bukkit.getPlayer(args[1]);
            teleporting = Bukkit.getPlayer(args[0]);
        } else if (args.length == 1) {
            target = Bukkit.getPlayer(args[0]);
            teleporting = player;
        } else {
            MessageFormatter.sendUsageMessage(player, "/tp <player> [player]");
            return;
        }
        
        if (target == null || teleporting == null) {
            MessageFormatter.sendErrorMessage(player, "The player/s specified must be online!");
            return;
        }

        teleporting.teleport(target);
        MessageFormatter.sendInfoMessage(teleporting, "You have been teleported to " + target.getName() + ".");
        MessageFormatter.sendInfoMessage(target, teleporting.getName() + " has been teleported to you.");
    }
    
    public static void tphere(Player player, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(player, "/tphere <player>");
            return;
        }
        
        Player teleporting = Bukkit.getPlayer(args[0]);
        if (teleporting == null) {
            MessageFormatter.sendErrorMessage(player, "The player specified must be online!");
            return;
        }

        teleporting.teleport(player);
        MessageFormatter.sendInfoMessage(teleporting, "You have been teleported to " + player.getName() + ".");
        MessageFormatter.sendSuccessMessage(player, teleporting.getName() + " has been teleported to you.");
    }
    
    public static void tpa(Player player, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(player, "/tpa <player>");
            return;
        }
        
        Player target = Bukkit.getPlayer(args[0]);
        if (target == null) {
            MessageFormatter.sendErrorMessage(player, "The player specified must be online!");
            return;
        }
        
        if (map.containsKey(player.getUniqueId()) == false) {
            map.put(player.getUniqueId(), new TeleportEntries(player.getUniqueId()));
        }
        
        if (map.get(player.getUniqueId()).putSent(target.getUniqueId(), RequestType.TPTO)) {
            MessageFormatter.sendSuccessMessage(player, "Your teleport request has been sent to " + target.getName());
            MessageFormatter.sendInfoMessage(target, "You have received a request from " + player.getName() + " to teleport to your position.");
            MessageFormatter.sendInfoMessage(target, "Type /tpaccept to accept or /tpdeny to decline!");
        } else {
            MessageFormatter.sendErrorMessage(player, "You have already sent a teleport request to " + target.getName() + ".");
        }
    }
    
    public static void tpahere(Player player, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(player, "/tpahere <player>");
            return;
        }

        Player target = Bukkit.getPlayer(args[0]);
        if (target == null) {
            MessageFormatter.sendErrorMessage(player, "The player specified must be online!");
            return;
        }

        if (map.containsKey(player.getUniqueId()) == false) {
            map.put(player.getUniqueId(), new TeleportEntries(player.getUniqueId()));
        }
        
        if (map.get(player.getUniqueId()).putSent(target.getUniqueId(), RequestType.TPHERE)) {
            MessageFormatter.sendSuccessMessage(player, "Your teleport request has been sent to " + target.getName());
            MessageFormatter.sendInfoMessage(target, "You have received a request from " + player.getName() + " to teleport to their position.");
            MessageFormatter.sendInfoMessage(target, "Type /tpaccept to accept or /tpdeny to decline!");
        } else {
            MessageFormatter.sendErrorMessage(player, "You have already sent a teleport request to " + target.getName() + ".");
        }
    }

    public static void tpall(Player player, String[] args) {
        Player target = args.length >= 1 ? (Bukkit.getPlayer(args[0]) != null ? Bukkit.getPlayer(args[0]) : player) : player;
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (p != target) {
                MessageFormatter.sendInfoMessage(p, "You have been teleported to " + target.getName() + ".");
                p.teleport(target);
            }
        }

        MessageFormatter.sendSuccessMessage(target, "All players were teleported to your position.");
    }
    
    public static void tpaccept(Player player, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(player, "/tpaccept <player>");
            return;
        }
        
        Player p = Bukkit.getPlayer(args[0]);
        if (p == null) {
            MessageFormatter.sendErrorMessage(player, "The player specified must be online!");
            return;
        }
        
        if (map.get(player.getUniqueId()) != null && map.get(player.getUniqueId()).containsReceived(p.getUniqueId())) {
            RequestType type = map.get(player.getUniqueId()).received.get(p.getUniqueId());
            switch (type) {
                case TPTO:
                    p.teleport(player);
                    break;
                case TPHERE:
                    player.teleport(p);
                    break;
            }
            
            MessageFormatter.sendInfoMessage(p, player.getName() + " has accepted your teleport request.");
            MessageFormatter.sendSuccessMessage(player, "You have accepted " + p.getName() + "'s teleport request.");
        } else {
            MessageFormatter.sendInfoMessage(player, "You have no pending request from " + p.getName());
        }
    }
    
    public static void tpdeny(Player player, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(player, "/tpdeny <player>");
            return;
        }

        Player p = Bukkit.getPlayer(args[0]);
        if (p == null) {
            MessageFormatter.sendErrorMessage(player, "The player specified must be online!");
            return;
        }

        if (map.get(player.getUniqueId()) == null && map.get(player.getUniqueId()).containsReceived(p.getUniqueId())) {
            map.get(player.getUniqueId()).removeReceived(p.getUniqueId());
            MessageFormatter.sendInfoMessage(p, player.getName() + " has declined your teleport request.");
            MessageFormatter.sendSuccessMessage(player, "You have declined " + p.getName() + "'s teleport request.");
        } else {
            MessageFormatter.sendInfoMessage(player, "You have no pending request from " + p.getName());
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        map.remove(event.getPlayer().getUniqueId());
    }
    
    public static class TeleportEntries {
        private UUID user;
        private ExpiringMap<UUID, RequestType> sent = ExpiringMap.builder().expiration(60, TimeUnit.SECONDS).build();
        private ExpiringMap<UUID, RequestType> received = ExpiringMap.builder().expiration(60, TimeUnit.SECONDS).build();
        
        public TeleportEntries(UUID user) {
            this.user = user;
        }
        
        public boolean putSent(UUID uuid, RequestType type) {
            if (containsSent(uuid)) {
                return false;
            }
            
            if (map.containsKey(uuid) == false) {
                map.put(uuid, new TeleportEntries(uuid));
            }
            
            map.get(uuid).putReceived(user, type);
            sent.put(uuid, type);
            return true;
        }
        
        public boolean containsSent(UUID uuid) {
            return sent.containsKey(uuid);
        }

        public boolean putReceived(UUID uuid, RequestType type) {
            if (containsSent(uuid)) {
                return false;
            }

            received.put(uuid, type);
            return true;
        }

        public boolean containsReceived(UUID uuid) {
            return received.containsKey(uuid);
        }
        
        public void removeReceived(UUID uuid) {
            received.remove(uuid);
            map.get(uuid).sent.remove(uuid);
        }
    }
    
    public enum RequestType {
        TPTO,
        TPHERE;
    }
}

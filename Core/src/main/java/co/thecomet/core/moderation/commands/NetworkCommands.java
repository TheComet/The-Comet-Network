package co.thecomet.core.moderation.commands;

import co.thecomet.data.ranks.Rank;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.command.CommandRegistry;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.core.utils.ProxyUtil;
import com.archeinteractive.rc.redis.pubsub.NetTask;
import org.apache.commons.lang.StringUtils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class NetworkCommands {
    public NetworkCommands() {
        CommandRegistry.registerUniversalCommand(CoreAPI.getPlugin(), "maintenance", Rank.ADMIN, NetworkCommands::maintenance);
        CommandRegistry.registerUniversalCommand(CoreAPI.getPlugin(), "galert", Rank.ADMIN, NetworkCommands::galert);
        CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "hub", NetworkCommands::hub);
    }
    
    public static void maintenance(CommandSender sender, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(sender, "/maintenance <true|false>");
            return;
        }
        
        boolean enabled;
        try {
            enabled = Boolean.parseBoolean(args[0]);
        } catch (NumberFormatException e) {
            enabled = false;
        }

        NetTask.withName("maintenance").withArg("maintenance", enabled).send("maintenance");
        MessageFormatter.sendSuccessMessage(sender, "Maintenance mode will " + (enabled ? "enabled" : "disabled") + " shortly.");
    }
    
    public static void hub(Player sender, String[] args) {
        if (args.length > 0) {
            MessageFormatter.sendUsageMessage(sender, "/hub");
            return;
        }

        ProxyUtil.send(sender, "HUB");
    }
    
    public static void galert(CommandSender sender, String[] args) {
        if (args.length == 0) {
            MessageFormatter.sendUsageMessage(sender, "/galert <message>");
            return;
        }
        
        String message = StringUtils.join(args, " ");
        NetTask.withName("galert").withArg("message", message).send("galert");
    }
}

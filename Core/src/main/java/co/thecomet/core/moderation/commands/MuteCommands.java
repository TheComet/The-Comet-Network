package co.thecomet.core.moderation.commands;

import co.thecomet.data.SessionAPI;
import co.thecomet.data.DAOManager;
import co.thecomet.data.DataAPI;
import co.thecomet.data.moderation.ModerationAction;
import co.thecomet.data.moderation.ModerationEntry;
import co.thecomet.data.ranks.Rank;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.command.CommandRegistry;
import co.thecomet.data.user.INetworkUser;
import co.thecomet.data.user.User;
import co.thecomet.core.listeners.ChatListener;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.core.utils.TimeFormatUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MuteCommands {
    public MuteCommands() {
        CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "mute", Rank.FRIEND, MuteCommands::tempmute);
        CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "tempmute", Rank.FRIEND, MuteCommands::tempmute);
        CommandRegistry.registerUniversalCommand(CoreAPI.getPlugin(), "unmute", Rank.FRIEND, MuteCommands::unmute);
    }

    public static void tempmute(Player sender, String[] args) {
        if (args.length < 3) {
            MessageFormatter.sendUsageMessage(sender, "/tempmute <player> <#m|#h|#d> <reason>");
            return;
        }

        String player = args[0];
        String time = args[1];
        String reason = args[2];

        if (args.length > 3) {
            for (int i = 3; i < args.length; i++) {
                reason += " " + args[i];
            }
        }

        Player target = Bukkit.getPlayer(player);
        User user = target != null ? DataAPI.retrieveUser(target.getUniqueId()) : DataAPI.retrieveUserByName(player);

        if (user == null) {
            MessageFormatter.sendErrorMessage(sender, "Unable to find user in the database. Is the name correct?");
            return;
        }

        long duration;
        try {
            duration = TimeFormatUtils.getTime(time).longValue();
            if (duration == -1) {
                MessageFormatter.sendUsageMessage(sender, "/tempmute <player> <#m|#h|#d> <reason>");
                return;
            }
        } catch (NumberFormatException e) {
            MessageFormatter.sendUsageMessage(sender, "/tempmute <player> <#m|#h|#d> <reason>");
            return;
        }

        ModerationEntry entry = new ModerationEntry(user.getUuid(), sender.getUniqueId().toString(), ModerationAction.MUTE, reason, duration);
        DataAPI.addModerationEntry(entry);

        if (target != null) {
            INetworkUser u = SessionAPI.getUser(target.getUniqueId());
            
            if (u != null) {
                u.getModerationHistory().setActiveMute(entry);
                ChatListener.addMute(u);
            }
        }

        for (Player p : Bukkit.getOnlinePlayers()) {
            MessageFormatter.sendInfoMessage(p, player + " &cwas tempmuted by &e&l" + sender.getName(), "&cfor &6&l" + reason + " &7(" + time + ")");
        }
    }

    public static void unmute(CommandSender sender, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(sender, "/unmute <player>");
            return;
        }

        String player = args[0];

        Player target = Bukkit.getPlayer(player);
        User user = target != null ? DataAPI.retrieveUser(target.getUniqueId()) : DataAPI.retrieveUserByName(player);

        if (user == null) {
            MessageFormatter.sendErrorMessage(sender, "Unable to find user in the database. Is the name correct?");
            return;
        }

        if (target != null) {
            ChatListener.getMutedPlayers().remove(target.getUniqueId());
        }

        user.getModerationHistory().setActiveMute(null);
        DAOManager.getDAO(User.class).save(user);
        MessageFormatter.sendSuccessMessage(sender, ChatColor.GOLD + player + " &4has been unmuted.");
    }
}

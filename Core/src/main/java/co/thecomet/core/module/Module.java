package co.thecomet.core.module;

import co.thecomet.core.CoreAPI;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.List;

public abstract class Module {
    protected Module() {
        super();
    }

    public ModuleInfo getModuleInfo() {
        return ModuleManager.getModuleInfo(this);
    }

    public void onEnable() {}

    public void onDisable() {}

    public List<Listener> registerListeners() {
        return new ArrayList<>();
    }
    
    public void registerListener(Listener listener) {
        Bukkit.getPluginManager().registerEvents(listener, CoreAPI.getPlugin());
        ModuleManager.listeners.put(this.getClass(), listener);
    }
    
    public void unregisterListener(Listener listener) {
        HandlerList.unregisterAll(listener);
        ModuleManager.listeners.remove(this.getClass(), listener);
    }
}

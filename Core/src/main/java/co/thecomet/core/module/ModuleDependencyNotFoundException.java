package co.thecomet.core.module;

public class ModuleDependencyNotFoundException extends Exception {
    public ModuleDependencyNotFoundException(Class<? extends Module> module, Class<? extends Module> dependency) {
        super("Failed to load " + module.getSimpleName() + " as a result of missing dependency: " + dependency.getSimpleName());
    }
}

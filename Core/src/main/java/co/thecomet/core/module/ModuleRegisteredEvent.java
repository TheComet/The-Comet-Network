package co.thecomet.core.module;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class ModuleRegisteredEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private Module module;

    public ModuleRegisteredEvent(Module module) {
        this.module = module;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public <T> T getModule(Class<T> clazz) {
        return clazz.cast(module);
    }

    public Module getModule() {
        return module;
    }
}

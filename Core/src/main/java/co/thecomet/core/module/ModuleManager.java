package co.thecomet.core.module;

import co.thecomet.core.CoreAPI;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

import java.util.*;

public class ModuleManager {
    private static final Map<Class<? extends Module>, ModuleInfo> moduleInfo = new HashMap<>();
    private static final Map<Class<? extends Module>, Module> modules = new HashMap<>();
    private static final List<Class<? extends Module>> enabled = new ArrayList<>();
    protected static final Multimap<Class<? extends Module>, Listener> listeners = HashMultimap.create();

    public static boolean registerModule(Module module) {
        if (module == null) {
            return false;
        }

        boolean initialized;
        try {
            initialized = init(module);
            if (initialized) {
                Bukkit.getPluginManager().callEvent(new ModuleRegisteredEvent(module));
            }
        } catch (ModuleDependencyNotFoundException e) {
            CoreAPI.getPlugin().getLogger().info(ChatColor.RED + e.getMessage());
            return false;
        } catch (Exception e) {
            return false;
        }

        return initialized;
    }

    protected static boolean init(Module module) throws InstantiationException, IllegalAccessException, ModuleDependencyNotFoundException {
        Class<? extends Module> clazz = module.getClass();
        ModuleInfo info;
        if (clazz.isAnnotationPresent(ModuleInfo.class)) {
            info = clazz.getAnnotation(ModuleInfo.class);
            if (isEnabled(clazz)) {
                CoreAPI.getPlugin().getLogger().info("Module \"" + info.name() + "\" is already registered!");
                return true;
            }

            for (Class<? extends Module> c : info.depends()) {
                if (isEnabled(c) == false) {
                    throw new ModuleDependencyNotFoundException(clazz, c);
                }
            }

            if (moduleInfo.containsKey(clazz) == false) {
                moduleInfo.put(clazz, info);
            }

            if (modules.containsKey(clazz) == false) {
                modules.put(clazz, module);
            }

            if (isEnabled(module.getClass()) == false) {
                CoreAPI.getPlugin().getLogger().info("Enabling Module: " + info.name());
                enabled.add(clazz);
                module.onEnable();
                registerListeners(module);
                CoreAPI.getPlugin().getLogger().info("Module Enabled: " + info.name());
            } else {
                CoreAPI.getPlugin().getLogger().info("Module \"" + info.name() + "\" is already registered!");
            }
        } else {
            return false;
        }

        return true;
    }

    public static boolean isEnabled(Class<? extends Module> module) {
        return (moduleInfo.containsKey(module) && modules.containsKey(module) && enabled.contains(module));
    }

    protected static void registerListeners(Module module) {
        listeners.putAll(module.getClass(), module.registerListeners());
        listeners.get(module.getClass()).forEach((listener) -> Bukkit.getPluginManager().registerEvents(listener, CoreAPI.getPlugin()));
    }

    public static void unregisterModule(Module module) {
        clean(module.getClass());
        Bukkit.getPluginManager().callEvent(new ModuleUnregisteredEvent(module));
    }

    public static void unregisterModules(Class<? extends Module> ... module) {
        List<Class<? extends Module>> list = Arrays.asList(module);
        modules.forEach((clazz, m) -> {
            if (list.contains(clazz)) {
                unregisterModule(m);
            }
        });
    }

    public static void unregisterAllModules() {
        modules.values().forEach(ModuleManager::unregisterModule);
    }

    public static void unregisterAllModulesExcept(Class<? extends Module> ... module) {
        List<Class<? extends Module>> list = Arrays.asList(module);
        modules.forEach((clazz, m) -> {
            if (list.contains(clazz) == false) {
                unregisterModule(m);
            }
        });
    }

    protected static void clean(Class<? extends Module> clazz) {
        moduleInfo.remove(clazz);

        if (modules.containsKey(clazz)) {
            modules.get(clazz).onDisable();
        }

        if (enabled.contains(clazz)) {
            enabled.remove(clazz);
        }

        unregisterListeners(clazz);
        modules.remove(clazz);
    }

    protected static void unregisterListeners(Class<? extends Module> clazz) {
        listeners.get(clazz).forEach(HandlerList::unregisterAll);
        listeners.removeAll(clazz);
    }

    public static ModuleInfo getModuleInfo(Class<? extends Module> clazz) {
        return moduleInfo.get(clazz);
    }

    public static ModuleInfo getModuleInfo(Module module) {
        return getModuleInfo(module.getClass());
    }

    public static <T extends Module> T getModule(Class<T> clazz) {
        Module module = modules.get(clazz);
        return module == null ? null : clazz.cast(module);
    }
}

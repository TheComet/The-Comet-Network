package co.thecomet.core;

import co.thecomet.common.async.Async;
import co.thecomet.core.permission.PermissionManager;
import co.thecomet.data.DataAPI;
import co.thecomet.data.moderation.NetworkModerationData;
import co.thecomet.core.luckyblocks.LuckyBlocks;
import co.thecomet.core.module.ModuleManager;
import co.thecomet.core.permission.commands.PermissionCommands;
import co.thecomet.data.ranks.RankData;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

public class CoreAPI {
    private static CoreAPI instance;
    private static CorePlugin plugin;
    private static NetworkModerationData globalModerationData;
    private static CoreModule core;
    private static LuckyBlocks luckyBlocks;
    private static boolean loaded = false;

    public CoreAPI(CorePlugin p) {
        if (instance == null) {
            instance = this;
            plugin = p;

            // Setup Permissions
            globalModerationData = DataAPI.initGlobalModerationData();
            List<RankData> datas = DataAPI.initRankData();
            datas.forEach(data -> PermissionManager.getRanks().put(data.getRank(), data));
            new PermissionCommands();

            // Initialize Core Module
            core = new CoreModule();
            ModuleManager.registerModule(core);

            // Initialize LuckyBlocks Module
            luckyBlocks = new LuckyBlocks();
            ModuleManager.registerModule(luckyBlocks);
            loaded = true;
        }
    }

    public static CorePlugin getPlugin() {
        return plugin;
    }

    public static void async(Runnable runnable) {
        Async.execute(runnable);
    }

    public static <V> Future<V> async(Callable<V> callable) {
        return Async.submit(callable);
    }

    public static boolean isLoaded() {
        return loaded;
    }

    public static void setLoaded(boolean loaded) {
        CoreAPI.loaded = loaded;
    }

    public static NetworkModerationData getGlobalModerationData() {
        return globalModerationData;
    }

    public static LuckyBlocks getLuckyBlocks() {
        return luckyBlocks;
    }

    public static void debug(String ... messages) {
        for (String message : messages) {
            plugin.getLogger().info("[Debug] " + message);
        }
    }
}

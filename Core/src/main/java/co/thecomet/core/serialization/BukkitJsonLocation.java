package co.thecomet.core.serialization;

import co.thecomet.common.serialization.JsonLocation;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

@ToString(callSuper = true)
@NoArgsConstructor
public class BukkitJsonLocation {
    @Getter
    private JsonLocation serializedLocation;

    public BukkitJsonLocation(JsonLocation location)  {
        this.serializedLocation = location;
    }

    public BukkitJsonLocation(Player player) {
        this(player.getLocation());
    }

    public BukkitJsonLocation(Block block) {
        this(block.getLocation());
    }

    public BukkitJsonLocation(Location location) {
        this(location.getWorld(), location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());
    }

    public BukkitJsonLocation(World world, double x, double y, double z, float yaw, float pitch) {
        serializedLocation = new JsonLocation(world.getName(), x, y, z, yaw, pitch);
    }

    public Location getLocation() {
        World world = Bukkit.getWorld(serializedLocation.getWorld()) == null ? Bukkit.getWorlds().get(0) : Bukkit.getWorld(serializedLocation.getWorld());
        return new Location(world, serializedLocation.getX(), serializedLocation.getY(), serializedLocation.getZ(), (float) serializedLocation.getYaw(), (float) serializedLocation.getPitch());
    }

    public Block getBlock() {
        return getLocation().getBlock();
    }
}

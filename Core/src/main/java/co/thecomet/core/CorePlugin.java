package co.thecomet.core;

import co.thecomet.common.utils.Log;
import co.thecomet.core.command.CommandListener;
import co.thecomet.core.ui.MenuAPI;
import com.archeinteractive.rc.RedisConnect;
import com.archeinteractive.rc.bukkit.BukkitConnector;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * A base class that handles core feature initialization
 * and general plugin preparation.
 */
public abstract class CorePlugin extends JavaPlugin {
    private static MenuAPI menuAPI;
    protected boolean vipOnly = false;
    protected boolean joinable = true;
    protected boolean spectatorJoinInProgress = true;

    public void onEnable() {
        Log.setLogger(getLogger());
        // Init Command Listener
        CommandListener.setup(this);

        // Init Menu API
        if (menuAPI == null) {
            menuAPI = new MenuAPI(this);
        }

        // Init API
        new CoreAPI(this);

        // Enable
        enable();
        
        // VIP Only
        vipOnly = CoreModule.getCoreConfig().vipOnly;
        
        // Loaded
        CoreAPI.setLoaded(true);
    }

    public void onDisable() {
        // Disable
        disable();
    }

    /**
     * Enables a plugin
     */
    public abstract void enable();

    /**
     * Disables a plugin
     */
    public void disable() {}

    /**
     * Returns the server name specified in redis config.
     *
     * @return
     */
    public String getId() {
        return RedisConnect.getConnectorSettings().getName();
    }

    /**
     * Returns the server type.
     * e.g. HUB, SG, FACTIONS, SW, etc...
     *
     * @return
     */
    public abstract String getType();

    /**
     * Returns the current status/phase of the server.
     * e.g. ONLINE, INPROGRESS, VOTING, etc...
     *
     * @return
     */
    public String getStatus() {
        return "ONLINE";
    }

    /**
     * Returns the number of players online on this server.
     *
     * @return
     */
    public long getPlayersOnline() {
        return Bukkit.getOnlinePlayers().size();
    }

    /**
     * Returns the max number of players allowed online.
     *
     * @return
     */
    public int getMaxPlayers() {
        return Bukkit.getMaxPlayers();
    }

    /**
     * Returns if the server is in vip mode.
     *
     * @return
     */
    public boolean isVipOnly() {
        return vipOnly;
    }

    /**
     * Sets the vipOnly state.
     *
     * @param vipOnly
     */
    public void setVipOnly(boolean vipOnly) {
        this.vipOnly = vipOnly;
    }

    public boolean isJoinable() {
        return joinable;
    }

    public void setJoinable(boolean joinable) {
        this.joinable = joinable;
    }

    public boolean isSpectatorJoinInProgress() {
        return spectatorJoinInProgress;
    }

    public void setSpectatorJoinInProgress(boolean spectatorJoinInProgress) {
        this.spectatorJoinInProgress = spectatorJoinInProgress;
    }
}

package co.thecomet.common.chat;

public enum MessageType {
    GENERAL,
    INFO,
    USAGE,
    ERROR,
    SUCCESS,
    ANNOUNCEMENT
}

package co.thecomet.common.impl;

import co.thecomet.common.Common;
import co.thecomet.common.ICommon;
import co.thecomet.common.utils.world.generator.VoidGenerator;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.java.JavaPlugin;

public class BukkitIMPL extends JavaPlugin implements ICommon {
    public void onEnable() {
        Common.setPlugin(this);
    }

    @Override
    public ChunkGenerator getDefaultWorldGenerator(String worldName, String id) {
        switch (id.toLowerCase()) {
            case "void":
                return new VoidGenerator();
            default:
                return super.getDefaultWorldGenerator(worldName, id);
        }
    }
}

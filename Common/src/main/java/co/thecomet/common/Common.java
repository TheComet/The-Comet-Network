package co.thecomet.common;

public class Common {
    private static ICommon plugin;
    
    public static ICommon getPlugin() {
        return plugin;
    }
    
    public static void setPlugin(ICommon p) {
        plugin = p;
    }
}

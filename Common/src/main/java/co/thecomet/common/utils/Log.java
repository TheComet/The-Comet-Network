package co.thecomet.common.utils;

import java.util.logging.Logger;

public class Log {
    private static Logger logger = null;

    public static void setLogger(Logger logger) {
        Log.logger = logger;
    }

    public static void debug(String ... messages) {
        if (logger == null) {
            return;
        }

        for (String message : messages) {
            logger.info("[Debug] " + message);
        }
    }
}

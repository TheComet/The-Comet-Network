package co.thecomet.database.bukkit;

import co.thecomet.database.DB;
import co.thecomet.database.DBPlugin;
import co.thecomet.database.config.Settings;
import co.thecomet.database.mongodb.MongoController;
import co.thecomet.common.config.JsonConfig;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class BukkitDBImpl extends JavaPlugin implements DBPlugin {

    private Settings settings = null;
    private MongoController mongoController;

    public void onEnable() {
        DB.setPlugin(this);

        if (!getDataFolder().exists()) {
            getLogger().info("Config folder not found! Creating...");
            getDataFolder().mkdir();
        }

        settings = JsonConfig.load(new File(getDataFolder(), "settings.json"), Settings.class);
        mongoController = new MongoController(this);
    }

    public Settings getSettings() {
        return settings;
    }

    public MongoController getMongoController() {
        return mongoController;
    }
}

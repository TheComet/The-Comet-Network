package co.thecomet.database;

import co.thecomet.database.config.Settings;
import co.thecomet.database.mongodb.MongoController;

import java.util.logging.Logger;

public interface DBPlugin {
    public Settings getSettings();

    public Logger getLogger();

    public MongoController getMongoController();
}

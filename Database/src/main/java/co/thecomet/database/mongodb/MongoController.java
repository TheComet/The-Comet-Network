package co.thecomet.database.mongodb;

import co.thecomet.database.DBPlugin;
import org.mongodb.morphia.dao.BasicDAO;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("rawtypes")
public class MongoController {
    private ResourceManager resourceManager;
    private Map<Class, BasicDAO> daoMap = new HashMap<>();

    public MongoController(DBPlugin plugin) {
        resourceManager = new ResourceManager(plugin);
        init();
    }

    public void register(BasicDAO dao, Class<?> entity) {
        daoMap.put(entity, dao);
    }

    public void init() {
        // register(new ExampleDAO(datastore), ExampleClass.class);
    }

    public ResourceManager getResourceManager() {
        return resourceManager;
    }
}

package co.thecomet.database.config;

import co.thecomet.database.config.components.Database;
import co.thecomet.common.config.JsonConfig;

public class Settings extends JsonConfig {

    private Database database = new Database();

    public Database getDatabase() {
        return database;
    }
}

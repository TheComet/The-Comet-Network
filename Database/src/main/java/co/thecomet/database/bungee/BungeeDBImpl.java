package co.thecomet.database.bungee;

import co.thecomet.database.DB;
import co.thecomet.database.DBPlugin;
import co.thecomet.database.config.Settings;
import co.thecomet.database.mongodb.MongoController;
import co.thecomet.common.config.JsonConfig;
import net.md_5.bungee.api.plugin.Plugin;

import java.io.File;

public class BungeeDBImpl extends Plugin implements DBPlugin {
    private static BungeeDBImpl instance;
    private Settings settings = null;
    private MongoController mongoController;

    public void onEnable() {
        instance = this;
        DB.setPlugin(this);

        if (!getDataFolder().exists()) {
            getLogger().info("Config folder not found! Creating...");
            getDataFolder().mkdir();
        }

        settings = JsonConfig.load(new File(getDataFolder(), "settings.json"), Settings.class);
        mongoController = new MongoController(this);
    }

    public static BungeeDBImpl getInstance() {
        return instance;
    }

    public Settings getSettings() {
        return settings;
    }

    public MongoController getMongoController() {
        return mongoController;
    }
}
